
var GSSS = {
    calendar: {
        maya: {}
    },
    modal: function(selector, options) {
        if (typeof options == 'undefined') options = {};
        var options2 = $.extend(options, {
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            items: {
                src: selector, // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });
        $.magnificPopup.open(options2);
    },


    /**
     * Парсит объект (object) в строку JSON
     *
     * @param object
     *
     * @returns {*}
     */
    jsonEncode: function (object) {
        return JSON.stringify(object);
    },

    /**
     * Парсит строку JSON (string) в объект
     *
     * @param string
     *
     * @returns {*}
     */
    jsonDecode: function (string) {
        return $.parseJSON(string);
    }
};


function ajaxJson(options) {
    return $.ajax({
        url: options.url,
        type: (typeof options.type != 'undefined') ? options.type : 'post',
        data: options.data,
        dataType: 'json',
        beforeSend: options.beforeSend,
        success: function (ret) {
            var status;
            for (i in ret) {
                if (i == 'error') status = 'error';
                if (i == 'success') status = 'success';
            }
            if (status == 'success') {
                if ((typeof options.success != 'undefined')) {
                    options.success(ret.success);
                }
                return;
            }
            if (status == 'error') {
                if (typeof options.errorScript != 'undefined') {
                    options.errorScript(ret.error);
                } else {
                    alert(ret.error);
                }
            }
        },
        error: function (ret) {
            $.ajax({
                url: '/errorAjaxReport',
                data: {
                    status: ret.status,
                    url: options.url,
                    type: (typeof options.type != 'undefined') ? options.type : 'post',
                    data: options.data
                },
                success: function(ret) {

                }
            });
        },
        complete: function(xhr, textStatus) {
            if (xhr.status == 500) {
                $.ajax({
                    url: '/errorAjaxReport',
                    data: {
                        status: ret.status,
                        url: options.url,
                        type: (typeof options.type != 'undefined') ? options.type : 'post',
                        data: options.data
                    },
                    success: function(ret) {

                    }
                });
            }
        }
    });

}

/**
 * Выводит информационное модальное окно
 * Если надо повесить событие на закрытие окна то надо добавить событие так
 *
 * @param text - string -  выводимое сообщение
 * @param closeFunction - function - функция выполняемая по закрытию окна
 */
function infoWindow(text, closeFunction) {

    $('#infoModal').html('').append($('<p>').html(text));
    $('#infoModal').magnificPopup({

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: false,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',

        items: {
            src: '#infoModal', // CSS selector of an element on page that should be used as a popup
            type: 'inline'
        },
        callbacks:{
            afterClose: closeFunction
        }
    }).magnificPopup('open');

}


$(document).ready(function() {
    $("[data-toggle='tooltip']").tooltip();
});