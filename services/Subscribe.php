<?php


namespace app\services;

use app\models\SiteContentInterface;
use app\models\SubscribeMailItem;
use app\models\User;
use cs\services\VarDumper;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Subscribe
{
    const TYPE_NEWS        = 1;
    const TYPE_SITE_UPDATE = 2;
    const TYPE_MANUAL      = 3;

    /** @var array Список полей для хранения информации о типах подписки */
    public static $userFieldList = [
        'subscribe_is_site_update',
        'subscribe_is_news',
        'subscribe_is_manual',
    ];

    /**
     * Добавляет записи для рассылки в таблицу рассылки
     *
     * @param SiteContentInterface | \app\models\SubscribeItem $item тема письма
     */
    public static function add($item)
    {
        if ($item instanceof SiteContentInterface) {
            $subscribeItem = $item->getMailContent();
        } else {
            $subscribeItem = $item;
        }
        switch ($subscribeItem->type) {
            case self::TYPE_NEWS:
                $where = ['subscribe_is_news' => 1];
                break;
            case self::TYPE_SITE_UPDATE:
                $where = ['subscribe_is_site_update' => 1];
                break;
            case self::TYPE_MANUAL:
                $where = ['subscribe_is_manual' => 1];
//                $where = ['subscribe_is_test' => 1];
                break;
        }

        $emailList = User::query($where)
        ->select('email')
        ->andWhere(['not', ['email' => null]])
        ->andWhere([
            'is_active'  => 1,
            'is_confirm' => 1,
        ])
        ->column();

        foreach ($emailList as $email) {
            $urlUnSubscribe = Url::to(['subscribe/unsubscribe', 'mail' => $email, 'type' => $subscribeItem->type, 'hash' => self::hashGenerate($email, $subscribeItem->type)], true);
            $fields = [
                'html'        => str_replace('{linkUnsubscribe}', $urlUnSubscribe, $subscribeItem->html),
                'subject'     => $subscribeItem->subject,
                'mail'        => $email,
                'date_insert' => time(),
            ];
            if ($subscribeItem->text) {
                $fields['text'] = str_replace('{linkUnsubscribe}', $urlUnSubscribe, $subscribeItem->text);
            }
            SubscribeMailItem::insert($fields);
        }
    }

    /**
     * Добавляет записи для рассылки в таблицу рассылки
     *
     * @param \app\models\SubscribeItem $subscribeItem
     */
    public static function add2($subscribeItem)
    {
        if (is_array($subscribeItem->html)) {
            /** @var \Closure $getHtml */
            $getHtml = $subscribeItem->html[0];
            $options = $subscribeItem->html[1];
        } else {
            $getHtml = function($options) {
                return $options;
            };
            $options = $subscribeItem->html;
        }

        $where = ['subscribe_is_site_update' => 1];
        switch ($subscribeItem->type) {
            case self::TYPE_NEWS:
                $where = ['subscribe_is_news' => 1];
                break;
            case self::TYPE_SITE_UPDATE:
                $where = ['subscribe_is_site_update' => 1];
                break;
            case self::TYPE_MANUAL:
                $where = ['subscribe_is_manual' => 1];
                break;
        }

        $emailList = User::query($where)
            ->select('email, time_zone')
            ->andWhere(['not', ['email' => null]])
            ->andWhere([
                'is_active'  => 1,
                'is_confirm' => 1,
            ])
            ->andWhere(['>', 'id', 41])
            ->orderBy('id')
            ->all();

        $c = 0;
        $timeZone = \Yii::$app->timeZone;
        foreach ($emailList as $row) {
            $urlUnSubscribe = Url::to(['subscribe/unsubscribe', 'mail' => $row['email'], 'type' => $subscribeItem->type, 'hash' => self::hashGenerate($row['email'], $subscribeItem->type)], true);
            if (is_null($tz = ArrayHelper::getValue($row, 'time_zone', null))) {
                \Yii::$app->setTimeZone($timeZone);
            } else {
                \Yii::$app->timeZone = $tz;
            }
            $html = $getHtml($options);
            $fields = [
                'html'        => str_replace('{linkUnsubscribe}', $urlUnSubscribe, $html),
                'subject'     => $subscribeItem->subject,
                'mail'        => $row['email'],
                'date_insert' => time(),
                'text'        => '',
            ];
            SubscribeMailItem::insert($fields);
            $c++;
        }
        \Yii::$app->setTimeZone($timeZone);
    }

    /**
     * Генерирует hash для ссылки отписки
     *
     * @param string  $email почта клиента
     * @param integer $type  имп рассылки \app\services\Subscribe::TYPE_*
     *
     * @return string
     *
     */
    public static function hashGenerate($email, $type)
    {
        return md5($email . '_' . $type);
    }

    /**
     * Проверяет hash для ссылки отписки
     *
     * @param string  $email почта клиента
     * @param integer $type  имп рассылки \app\services\Subscribe::TYPE_*
     * @param integer $hash
     *
     * @return boolean
     * true - верный
     * false - не верный
     */
    public static function hashValidate($email, $type, $hash)
    {
        return md5($email . '_' . $type) == $hash;
    }
}