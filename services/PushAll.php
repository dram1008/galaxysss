<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 11.04.2016
 * Time: 10:57
 */

namespace app\services;


use yii\base\Object;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * Рассылка уведомлений
 * 
 * Class PushAll
 * @package app\services
 *
 *
 Параметры для сервиса https://pushall.ru/api.php:
type - тип запроса. Может быть:self - самому себе, broadcast - вещание по каналу, unicast - отправка одному пользователю
id - номер вашей подписки в случае с self - ваш айди
key - ключ вашей подписки, или ключ безопасности для отправки Push себе
title - заголовок Push-уведомления
text - основной текст Push-уведомления
icon - иконка уведомления (необязательно)
url - адрес по которому будет осуществлен переход по клику (не обязательно)
hidden - 1 - сразу скрыть уведомление из истории пользователей, 2 - скрыть только из ленты, 0 - не скрывать (по-умолчанию 0)
encode - ваша кодировка. (не обязательно) например cp1251
priority - приоритет. 0 - по по-умолчанию. -1 - не важные, менее заметны, не будят устройство 1 - более заметные
ttl - время жизни в секундах. По-умолчанию 25 дней - 2160000 секунд.
 */

class PushAll extends Object
{
    public $channelId;
    public $apiKey;
    public $url = 'https://pushall.ru/api.php';

    /**
     * Отправляет заявку на рассылку
     * 
     * @param array $options
     * + title - string - Заголовок
     * + text - string - текст сообщения
     * - icon - string - иконка уведомления, ели будет указан локальный путь то он будет преобразован в полный
     * - url - string - адрес по которому будет осуществлен переход по клику, ели будет указан локальный путь то он будет преобразован в полный
     * - hidden - int - сразу скрыть уведомление из истории пользователей, 2 - скрыть только из ленты, 0 - не скрывать (по-умолчанию 0)
     * - encode - string - ваша кодировка
     * - priority - int - приоритет. 0 - по по-умолчанию. -1 - не важные, менее заметны, не будят устройство 1 - более заметные
     * - ttl - int - время жизни в секундах. По-умолчанию 25 дней - 2160000 секунд.
     * 
     * @return bool
     * Если удачно отправлено - true
     * Если не удачно и не отправлено - false
     */
    public function send($options)
    {
        if (isset($options['url'])) {
            if (StringHelper::startsWith($options['url'],'http') == false) {
                $options['url'] = Url::to($options['url'], true);
            }
        }
        if (isset($options['icon'])) {
            if (StringHelper::startsWith($options['icon'],'http') == false) {
                $options['icon'] = Url::to($options['icon'], true);
            }
        }
        $ch = curl_init();
        $options = array_merge($options, [
            "type"  => "broadcast",
            "id"    => $this->channelId,
            "key"   => $this->apiKey,
        ]);
        curl_setopt_array($ch, [
            CURLOPT_URL            => $this->url,
            CURLOPT_POSTFIELDS     => $options,
            CURLOPT_SAFE_UPLOAD    => true,
            CURLOPT_RETURNTRANSFER => true
        ]);
        $return = curl_exec($ch); //получить данные о рассылке
        curl_close($ch);
        
        return $return;
    }
}