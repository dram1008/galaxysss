<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Заказ</title>
</head>
<body>
<table border="0" cellpadding="0" cellpadding="0" width="600" align="center" style="
    border: 1px solid #cccccc;
">
    <tr>
        <td>
            <img src="http://www.bog-dan.com/images/mail/header.jpg" width="600">
        </td>
    <tr>
    <tr>
        <td style="padding: 20px;">
            <p>Мир Вашему дому!</p>
            <?= $content ?>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px;">
            <p>С Любовью и Светом</p>
            <p>Авиалинии БогДан</p>
        </td>
    </tr>
</table>
</body>
</html>
