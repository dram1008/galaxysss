<?php

/** Письмо от магазина */

/**
 * @var $request \app\models\Shop\Request
 * @var $union \app\models\Union
 * @var $shop \app\models\Shop
 * @var $client \app\models\User
 * @var $dostavkaItem \app\models\Shop\DostavkaItem
 */

?>

<h2>Информация о магазине:</h2>
<p>Название: <a href="<?= $union->getLink(true) ?>"><?= $shop->getName() ?></a></p>


<h2>Информация о клиенте:</h2>
<p>Имя: <?= $client->getName2() ?></p>
<p>Email: <?= $client->getEmail() ?></p>
<p>Ссылка: <a href="<?= \yii\helpers\Url::to(['site/user', 'id' => $client->getId()], true) ?>">профиль</a></p>

<h2>Информация о <a href="<?= \yii\helpers\Url::to(['cabinet_shop_client/order_item', 'id' => $request->getId()], true)?>">заказе</a>:</h2>
<p>id: <?= $request->getId() ?></p>
<p>Время: <?= Yii::$app->formatter->asDatetime($request->getField('date_create')) ?></p>
<p>Комментарий: <?= $request->getField('comment') ?></p>
<p>Телефон: <?= $request->getField('phone') ?></p>
<p>Доставка: <?= $dostavkaItem->getField('name') ?></p>
<?php if ($dostavkaItem->getField('is_need_address', 0) == 1) { ?>
    <p>Адрес: <?= $request->getField('address') ?></p>
<?php } ?>

<p>Сумма: <?= Yii::$app->formatter->asCurrency($request->getField('price')) ?></p>
<p>Товары:</p>

<table cellspacing="5" cellpadding="5" border="1">
    <tr>
        <td>
            #
        </td>
        <td>
            Наименование
        </td>
        <td>
            Кол-во
        </td>
        <td>
            Итого
        </td>
    </tr>
    <?php foreach ($request->getProductList()->all() as $product) {
        $p = new \app\models\Shop\Product($product);
        ?>
        <tr>
            <td>
                <?= $product['id'] ?>
            </td>
            <td>
                <a href="<?= $p->getLink(true) ?>"><?= $product['name'] ?></a>
            </td>
            <td>
                <?= $product['count'] ?>
            </td>
            <td>
                <?= $product['count'] * $product['price'] ?>
            </td>
        </tr>
    <?php } ?>
</table>
