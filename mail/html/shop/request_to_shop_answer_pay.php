<?php
/**
 * @var $request \app\models\Shop\Request
 * @var $text string с переносами строки "\n"
 */

$union = $request->getUnion();
$shop = $union->getShop();
$client = $request->getClient();
$dostavkaItem = $request->getDostavkaItem();
?>

<tr>
    <td>
        <img src="<?= $shop->getMailImage() ?>" width="600">
    </td>
</tr>
<tr>
    <td style="padding: 20px;">
        <p>Любви и Света!</p>

        <p>Клиент оплатил заказ</p>
        <p>Сообщение: <?= nl2br($text) ?></p>

        <?= $this->render('_from_client', [
            'request'      => $request,
            'union'        => $union,
            'shop'         => $shop,
            'client'       => $client,
            'dostavkaItem' => $dostavkaItem,
        ]) ?>

    </td>
</tr>
<tr>
    <td style="padding: 20px;">
        <p><?= $shop->getMailSignature() ?></p>
    </td>
</tr>


