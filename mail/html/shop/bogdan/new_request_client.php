<?php

/** @var $request \app\models\Shop\BogDan\Request */

$tickets = $request->getTickets();

?>

<p style="text-align: center;"><img src="http://www.bog-dan.com/images/controller/site/ticket/hologram.gif" style="width: 200px;"></p>

<p>Поздравляем вы сделали первый шаг к своему полю коллективного счастья</p>

<p>Ваши билеты:</p>

<?php foreach($tickets as $t) { ?>
    <p><a href="http://www.bog-dan.com/ticket/<?= $t['id'] ?>">Билет #<?= $t['id'] ?> (<?= $t['code'] ?>)</p>
<?php } ?>

<p>Билеты вы можете распечатать в личном кабинете.</p>
