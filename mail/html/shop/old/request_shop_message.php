<?php
/**
 * @var $request \app\models\Shop\Request
 * @var $text string с переносами строки "\n"
 */

$union = $request->getUnion();
$shop = $union->getShop();
$client = $request->getClient();
$dostavkaItem = $request->getDostavkaItem();


?>
<p>По вашему заказу от магазина поступило сообщение:</p>
<p><?= nl2br($text) ?></p>

<h2>Информация о магазине:</h2>
<p>Название: <?= $shop->getName() ?></p>

<h2>Информация о клиенте:</h2>
<p>Имя: <?= $client->getName2() ?></p>
<p>Email: <?= $client->getEmail() ?></p>

<h2>Информация о заказе:</h2>
<p>id: <?= $request->getId() ?></p>
<p>Время: <?=  Yii::$app->formatter->asDatetime($request->getField('date_create')) ?></p>
<p>Комментарий: <?=  $request->getField('comment') ?></p>
<p>Телефон: <?=  $request->getField('phone') ?></p>
<p>Доставка: <?=  $dostavkaItem->getField('name') ?></p>
<?php if ($dostavkaItem->getField('is_need_address',0) == 1) { ?>
    <p>Адрес: <?=  $request->getField('address') ?></p>
<?php } ?>

<p>Сумма: <?=  $request->getField('price') ?></p>
<p>Товары:</p>

<table>
    <tr>
        <td>
            #
        </td>
        <td>
            Наименование
        </td>
        <td>
            Кол-во
        </td>
        <td>
            Итого
        </td>
    </tr>
    <?php foreach($request->getProductList()->all() as $product) { ?>
        <tr>
            <td>
                <?= $product['id'] ?>
            </td>
            <td>
                <?= $product['name'] ?>
            </td>
            <td>
                <?= $product['count'] ?>
            </td>
            <td>
                <?= $product['count'] * $product['price'] ?>
            </td>
        </tr>
    <?php } ?>
</table>
