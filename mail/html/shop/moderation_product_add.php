<?php
/**
 * @var $shop \app\models\Shop
 * @var $product \app\models\Shop\Product
 */

?>

<p>Добавлен товар</p>
<p>Название: <?= $product->get('name') ?></p>
<p><img src="<?= $product->getImage(true) ?>" width="300"></p>
<p><a href="<?= $product->getLink(true) ?>">Ссылка</a></p>

<p>Магазин</p>
<p><?= $shop->get('name') ?></p>
