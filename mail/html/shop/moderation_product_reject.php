<?php
/**
 * @var $shop \app\models\Shop
 * @var $product \app\models\Shop\Product
 * @var $text string причина отклонения
 */

?>

<tr>
    <td>
        <img src="<?= $shop->getMailImage() ?>" width="600">
    </td>
</tr>
<tr>
    <td style="padding: 20px;">
        <p>Любви и Света!</p>

        <p>Ваш продукт не прошел модерацию.</p>

        <p>Причина: <?= $text ?></p>

        <p>Продукт: <?= $product->getLink(true) ?></p>

    </td>
</tr>
<tr>
    <td style="padding: 20px;">
        <p><?= $shop->getMailSignature() ?></p>
    </td>
</tr>


