<?php
/**
 * @var $items array
 * [
 *      'name'  =>
 *      'index' =>
 *      'rows'  => [
 *                      gs_site_update.*
 *                 ]
 * ]
 */

use yii\helpers\Html;
use app\services\GsssHtml;

?>

<p>На Инструмент Вознесения были сделаны сдедующие добавления:</p>

<table width="560" cellpadding="0" cellspacing="0">
    <?php foreach($items as $item) {  ?>
        <tr>
            <td colspan="2" style="height: 70px; vertical-align: bottom;">
                <b><?= $item['name'] ?></b>
                <hr>
            </td>
        </tr>
        <?php $d = (int)count($item['rows'])/2; ?>
        <?php for($c = 0; $c < $d; $c++) {?>
            <tr>
                <?php $cells = ((count($item['rows']) - $c*2) < 2)? 1 : 2; ?>
                <?php for($z = 0; $z < $cells; $z++) { ?>
                    <?php $i = $item['rows'][ ($c*2) + $z ]?>
                    <td width="280" style="padding-<?= ($z == 0)? 'right' : 'left' ?>:20px;">
                        <p style="height: 60px;"><?= $i['name'] ?></p>
                        <p style="color: #888; font-size: 7pt; font-family: courier, monospace;"><?= Yii::$app->formatter->asDatetime($i['date_insert']) ?></p>
                        <p><a href="<?= \yii\helpers\Url::to($i['link'], true) ?>"><img style="border-radius: 20px; border: 1px solid #888;" src="<?= \yii\helpers\Url::to($i['image'], true) ?>" width="260"/></a></p>
                        <?php
//                        switch($item['index']) {
//                            case \app\models\SiteUpdateItem::TYPE_CHANNELING:
//                                $object = \app\models\Chenneling::find($i);
//                        }

                        ?>

                    </td>
                <?php } ?>
                <?php if ($cells == 1) { ?>
                    <td width="280" style="padding-left: 20px;"></td>
                <?php } ?>
            </tr>
        <?php } ?>
    <?php } ?>
</table>


