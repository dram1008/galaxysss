<?php
/**
 * @var $item \app\models\Chenneling
 * @var $reason string
 */
?>

<p>Ваше послание отклонено</p>
<p>Причина: <?= nl2br($reason) ?></p>
<p><img src="<?= $item->getImage(true) ?>" width="300"></p>
<p><b><?= $item->getName() ?></b></p>
<p><?= $item->get('description') ?></p>
<p>Ссылка: <a href="cabinet/channeling/<?= $item->getId() ?>/view">Послание</a></p>