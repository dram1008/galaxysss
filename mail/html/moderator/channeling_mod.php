<?php
/**
 * @var $item \app\models\Chenneling
 * @var $user \app\models\User
 */
?>

<p>Отправлено послание на модерацию</p>
<p><img src="<?= $item->getImage(true) ?>" width="300"></p>
<p><b><?= $item->getName() ?></b></p>
<p><?= $item->get('description') ?></p>
<p>Ссылка: <a href="/moderator/channeling/<?= $item->getId() ?>/view">Послание</a></p>

<p></p>
<p>Пользователь</p>
<p>Имя: <a href="<?= $user->getLink(true) ?>"><?= $user->getName2() ?></a></p>