<?php


namespace cs\assets\JqueryForm;

use yii\web\AssetBundle;
use Yii;

/**
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@csRoot/assets/JqueryForm/source';
    public $css      = [
    ];
    public $js       = [
        'jquery.form.js'
    ];
    public $depends  = [
        'yii\web\JqueryAsset',
    ];
}
