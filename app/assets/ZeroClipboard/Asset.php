<?php
namespace cs\assets\ZeroClipboard;

use yii\web\AssetBundle;


/**
 * Class Asset
 * @package cs\assets\ZClip
 *
 */
/**
var client = new ZeroClipboard($("#copy-button"), {
    moviePath: "ZeroClipboard.swf"
});
// После того как происходит загрузка флеш файла
client.on("load", function(client) {
    // и завершено копирование в буфер
    client.on("complete", function(client, args) {
        // выводим результат
        alert(args.text);
    });
});
 * */
class Asset extends AssetBundle
{
    public $sourcePath = '@csRoot/assets/ZeroClipboard/source';
    public $js = [
        //'main.js',
        'ZeroClipboard.js',
    ];
    public $css = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}