<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace cs\Widget\FileUploadMany\Google\reCaptcha;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
	public $css = [
    ];
    public $js = [
        'https://www.google.com/recaptcha/api.js',
    ];
    public $depends = [
    ];
}
