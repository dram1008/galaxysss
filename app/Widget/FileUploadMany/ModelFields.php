<?php

/**
 * table
 * widget_uploader_many_fields
 * id
 * table_name
 * field_name
 */

namespace cs\Widget\FileUploadMany;

class ModelFields extends \cs\base\DbRecord
{
    const TABLE = 'widget_uploader_many_fields';
}