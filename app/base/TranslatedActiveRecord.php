<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2016
 * Time: 11:54
 */

namespace cs\base;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class TranslatedActiveRecord extends ActiveRecord
{
    public $_translationFields;

    /**
     * @return array
     * Два варианта возврата:
     *
     * Такие поля используются для перевода как строки:
     * [
     *     '<Название поля для перевода>'
     * ]
     *
     * Такие поля используются для перевода как строки и картини:
     * [
     *     'text' => [
     *          '<Название поля для перевода>',
     *          ...
     *          ],
     *     'image' => [
     *          '<Название поля для перевода>',
     *          ...
     *          ],
     * ]
     *
     */
    public function translationFields()
    {
        return [];
    }

    /**
     * Возвращает ответ на вопрос: Поле $name является полем для перевода?
     *
     * @return bool
     * true - поле является полем для перевода
     * false - поле не является полем для перевода
     */
    private function isTranslationField($name)
    {
        $fields = $this->translationFields();
        if (ArrayHelper::isAssociative($fields)) {
            $text = ArrayHelper::getValue($fields, 'text', []);
            $image = ArrayHelper::getValue($fields, 'image', []);
            return in_array($name, $text) || in_array($name, $image);
        } else {
            return in_array($name, $fields);
        }
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes and related objects can be accessed like properties.
     *
     * @param string $name property name
     * @throws \yii\base\InvalidParamException if relation name is wrong
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {
        if ($this->translationFields()) {
            if ($this->isTranslationField($name)) {
                return $this->__getTranslated($name);
            }
        }
        return parent::__get($name);
    }

    /**
     * Возвращает поле в языке который установлен в приложении
     * Если оно Текущий язык в системе является языком в который принят по умолчанияю то возвращается значение поля $name
     * иначе оно выбирается из поля `<$name>_translation`
     * 
     * @param $name
     * @return mixed
     */
    public function __getTranslated($name)
    {
        if (Language::isDefault()) {
            return parent::__get($name);
        } else {
            $fieldNameTranslation = $name . '_translation';
            $value = parent::__get($fieldNameTranslation);
            if (is_null($value)) {
                return parent::__get($name);
            } else {
                if ($value == '') {
                    return parent::__get($name);
                } else {
                    $data = json_decode($value);
                    foreach ($data as $row) {
                        if (Language::getLanguageId() == $row[0]) {
                            return $row[1];
                        }
                    }
                    return parent::__get($name);
                }
            }
        }
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes and related objects can be accessed like properties.
     *
     * @param string $name property name
     * @throws \yii\base\InvalidParamException if relation name is wrong
     * @return mixed property value
     * @see getAttribute()
     */
    public function __set($name, $value)
    {
        if ($this->translationFields()) {
            if ($this->isTranslationField($name)) {
                return $this->__setTranslated($name, $value);
            }
        }
        return parent::__set($name, $value);
    }

    public function __setTranslated($name, $value)
    {
        if (Language::isDefault()) {
            $this->$name = $value;
        } else {
            $fieldNameTranslation = $name . '_translation';
            $value = $this->$fieldNameTranslation;
            if (is_null($value)) {
                $value = [[Language::getLanguageId(), $value]];
            } else {
                if ($value == '') {
                    $value = [[Language::getLanguageId(), $value]];
                } else {
                    $data = json_decode($value);
                    $isSet = false;
                    for ($i = 0; $i < count($data); $i++) {
                        $row = &$data[$i];
                        if (Language::getLanguageId() == $row[0]) {
                            $row[1] = $value;
                            $isSet = true;
                            break;
                        }
                    }
                    if (!$isSet) {
                        $data[] = [Language::getLanguageId(), $value];
                    }
                    $this->$fieldNameTranslation = json_encode($data);
                }
            }
        }
    }
}