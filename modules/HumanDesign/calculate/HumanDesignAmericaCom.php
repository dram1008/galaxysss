<?php

namespace app\modules\HumanDesign\calculate;

use cs\services\Str;
use cs\services\VarDumper;

/**
 * Получает данные для Дизайна Человека
 *
 * Class HumanDesign
 *
 * @package app\services
 *
 */
class HumanDesignAmericaCom
{
    public $url = 'http://yourhumandesign.ru/modules/mod_mapgen/tmpl/result.php';

    public static $cross = [
        [ "44/24", "13/7", "Джаксто-позиции крест Бдительности" ],
        [ "54/53", "32/42", "Джаксто-позиции крест Амбиций" ],
        [ "12/11", "25/46", "Джаксто-позиции крест Артикуляции" ],
        [ "23/43", "30/29", "Джаксто-позиции крест Ассимиляции" ],
        [ "37/40", "5/35", "Джаксто-позиции крест Сделок" ],
        [ "53/54", "42/32", "Джаксто-позиции крест Начинаний" ],
        [ "10/15", "18/17", "Джаксто-позиции крест Поведения" ],
        [ "27/28", "19/33", "Джаксто-позиции крест Заботы" ],
        [ "29/30", "20/34", "Джаксто-позиции крест Соглашения" ],
        [ "42/32", "60/56", "Джаксто-позиции крест Завершения" ],
        [ "6/36", "15/10", "Джаксто-позиции крест Конфликта" ],
        [ "64/63", "45/26", "Джаксто-позиции крест Замешательства" ],
        [ "32/42", "56/60", "Джаксто-позиции крест Сохранения" ],
        [ "8/14", "55/59", "Джаксто-позиции крест Вклада" ],
        [ "21/48", "54/53", "Джаксто-позиции крест Контроля" ],
        [ "18/17", "39/38", "Джаксто-позиции крест Коррекции" ],
        [ "36/6", "10/15", "Джаксто-позиции крест Кризиса" ],
        [ "40/37", "35/5", "Джаксто-позиции крест Отрицания" ],
        [ "48/21", "53/54", "Джаксто-позиции крест Глубины" ],
        [ "62/61", "3/50", "Джаксто-позиции крест Деталей" ],
        [ "63/64", "26/45", "Джаксто-позиции крест Сомнений" ],
        [ "2/1", "49/4", "Джаксто-позиции крест Водителя" ],
        [ "14/8", "9/55", "Джаксто-позиции крест Усиления" ],
        [ "35/5", "22/47", "Джаксто-позиции крест Опыта" ],
        [ "16/9", "63/64", "Джаксто-позиции крест Экспериментирования" ],
        [ "15/10", "17/18", "Джаксто-позиции крест Крайностей" ],
        [ "41/31", "44/24", "Джаксто-позиции крест Фантазии" ],
        [ "30/29", "34/20", "Джаксто-позиции крест Судеб" ],
        [ "9/16", "64/63", "Джаксто-позиции крест Фокуса" ],
        [ "4/49", "8/14", "Джаксто-позиции крест Формулирования" ],
        [ "22/47", "11/12", "Джаксто-позиции крест Грации" ],
        [ "5/35", "47/22", "Джаксто-позиции крест Привычек" ],
        [ "11/12", "46/25", "Джаксто-позиции крест Идей" ],
        [ "31/41", "24/44", "Джаксто-позиции крест Влияния" ],
        [ "25/46", "58/52", "Джаксто-позиции крест Невинности" ],
        [ "43/23", "29/30", "Джаксто-позиции крест Озарения" ],
        [ "7/13", "23/43", "Джаксто-позиции крест Взаимодействия" ],
        [ "57/51", "62/61", "Джаксто-позиции крест Интуиции" ],
        [ "60/56", "28/27", "Джаксто-позиции крест Ограничения" ],
        [ "13/7", "43/23", "Джаксто-позиции крест Слушания" ],
        [ "55/59", "9/16", "Джаксто-позиции крест Настроений" ],
        [ "3/50", "41/31", "Джаксто-позиции крест Мутации" ],
        [ "19/33", "1/2", "Джаксто-позиции крест Потребности" ],
        [ "20/34", "37/40", "Джаксто-позиции крест Настоящего" ],
        [ "17/18", "38/39", "Джаксто-позиции крест Мнений" ],
        [ "38/39", "57/51", "Джаксто-позиции крест Оппозиции" ],
        [ "47/22", "12/11", "Джаксто-позиции крест Угнетения" ],
        [ "45/26", "36/6", "Джаксто-позиции крест Обладания" ],
        [ "34/20", "40/37", "Джаксто-позиции крест Силы" ],
        [ "49/4", "14/8", "Джаксто-позиции крест Принципов" ],
        [ "39/38", "51/57", "Джаксто-позиции крест Провокации" ],
        [ "24/44", "13/7", "Джаксто-позиции крест Рационализации" ],
        [ "33/19", "2/1", "Джаксто-позиции крест Отступления" ],
        [ "28/27", "33/19", "Джаксто-позиции крест Рисков" ],
        [ "1/2", "4/49", "Джаксто-позиции крест Самовыражения" ],
        [ "46/25", "52/58", "Джаксто-позиции крест Интуитивной Прозорливости" ],
        [ "51/57", "61/62", "Джаксто-позиции крест Шока" ],
        [ "52/58", "21/48", "Джаксто-позиции крест Неподвижности" ],
        [ "56/60", "27/28", "Джаксто-позиции крест Стимуляции" ],
        [ "59/55", "16/9", "Джаксто-позиции крест Стратегии" ],
        [ "61/62", "50/3", "Джаксто-позиции крест Размышления" ],
        [ "26/45", "6/36", "Джаксто-позиции крест Обманщика" ],
        [ "50/3", "31/41", "Джаксто-позиции крест Ценностей" ],
        [ "58/52", "48/21", "Джаксто-позиции крест Жизненности" ],

        [ "27/28", "19/33", "Левоугольный крест Выравнивания" ],
        [ "28/27", "33/19", "Левоугольный крест Выравнивания" ],
        [ "31/41", "24/44", "Левоугольный крест Альфы" ],
        [ "41/31", "44/24", "Левоугольный крест Альфы" ],
        [ "51/57", "61/62", "Левоугольный крест Горна" ],
        [ "57/51", "62/61", "Левоугольный крест Горна" ],
        [ "45/26", "36/6", "Левоугольный крест Конфронтации" ],
        [ "26/45", "6/36", "Левоугольный крест Конфронтации" ],
        [ "53/54", "42/32", "Левоугольный крест Циклов" ],
        [ "54/53", "32/42", "Левоугольный крест Циклов" ],
        [ "23/43", "30/29", "Левоугольный крест Посвящения" ],
        [ "43/23", "29/30", "Левоугольный крест Посвящения" ],
        [ "2/1", "49/4", "Левоугольный крест Неповиновения" ],
        [ "1/2", "4/49", "Левоугольный крест Неповиновения" ],
        [ "52/58", "21/48", "Левоугольный крест Требований" ],
        [ "58/52", "48/21", "Левоугольный крест Требований" ],
        [ "56/60", "27/28", "Левоугольный крест Рассеянности" ],
        [ "60/56", "28/27", "Левоугольный крест Рассеянности" ],
        [ "63/64", "26/45", "Левоугольный крест Господства" ],
        [ "64/63", "45/26", "Левоугольный крест Господства" ],
        [ "20/34", "37/40", "Левоугольный крест Дуальности" ],
        [ "34/20", "40/37", "Левоугольный крест Дуальности" ],
        [ "12/11", "25/46", "Левоугольный крест Образования" ],
        [ "11/12", "46/25", "Левоугольный крест Образования" ],
        [ "21/48", "54/53", "Левоугольный крест Стремления" ],
        [ "48/21", "53/54", "Левоугольный крест Стремления" ],
        [ "25/46", "58/52", "Левоугольный крест Целительства" ],
        [ "46/25", "52/58", "Левоугольный крест Целительства" ],
        [ "16/9", "63/64", "Левоугольный крест Идентификации" ],
        [ "9/16", "63/64", "Левоугольный крест Идентификации" ],
        [ "24/44", "13/7", "Левоугольный крест Инкарнации" ],
        [ "44/24", "7/13", "Левоугольный крест Инкарнации" ],
        [ "39/38", "51/57", "Левоугольный крест Индивидуализма" ],
        [ "38/39", "57/51", "Левоугольный крест Индивидуализма" ],
        [ "30/29", "34/20", "Левоугольный крест Индустрии" ],
        [ "29/30", "20/34", "Левоугольный крест Индустрии" ],
        [ "22/47", "11/12", "Левоугольный крест Информирования" ],
        [ "47/22", "11/12", "Левоугольный крест Информирования" ],
        [ "42/32", "60/56", "Левоугольный крест Ограничения" ],
        [ "32/42", "56/60", "Левоугольный крест Ограничения" ],
        [ "13/7", "43/23", "Левоугольный крест Масок" ],
        [ "7/13", "23/43", "Левоугольный крест Масок" ],
        [ "37/40", "5/35", "Левоугольный крест Миграции" ],
        [ "40/37", "35/5", "Левоугольный крест Миграции" ],
        [ "62/61", "3/50", "Левоугольный крест Затмения" ],
        [ "61/62", "50/3", "Левоугольный крест Затмения" ],
        [ "36/6", "10/15", "Левоугольный крест Плана" ],
        [ "6/36", "15/10", "Левоугольный крест Плана" ],
        [ "15/10", "17/18", "Левоугольный крест Предупреждения" ],
        [ "10/15", "18/17", "Левоугольный крест Предупреждения" ],
        [ "33/19", "2/1", "Левоугольный крест Совершенствования" ],
        [ "19/33", "1/2", "Левоугольный крест Совершенствования" ],
        [ "49/4", "14/8", "Левоугольный крест Революции" ],
        [ "4/49", "8/14", "Левоугольный крест Революции" ],
        [ "35/5", "22/47", "Левоугольный крест Разделения" ],
        [ "5/35", "47/22", "Левоугольный крест Разделения" ],
        [ "55/59", "9/16", "Левоугольный крест Духа" ],
        [ "59/55", "16/9", "Левоугольный крест Духа" ],
        [ "8/14", "55/59", "Левоугольный крест Неуверенности" ],
        [ "14/8", "59/55", "Левоугольный крест Неуверенности" ],
        [ "17/18", "38/39", "Левоугольный крест Бунта" ],
        [ "18/17", "39/39", "Левоугольный крест Бунта" ],
        [ "3/50", "41/31", "Левоугольный крест Желаний" ],
        [ "50/3", "31/41", "Левоугольный крест Желаний" ],

        [ "63/64", "5/35", "Правоугольныйкрест Сознания" ],
        [ "35/5", "63/64", "Правоугольныйкрест Сознания" ],
        [ "64/63", "35/5", "Правоугольныйкрест Сознания" ],
        [ "5/35", "64/63", "Правоугольныйкрест Сознания" ],
        [ "30/29", "14/8", "Правоугольныйкрест Инфицирования" ],
        [ "8/14", "30/29", "Правоугольныйкрест Инфицирования" ],
        [ "29/30", "8/14", "Правоугольныйкрест Инфицирования" ],
        [ "14/8", "29/30", "Правоугольныйкрест Инфицирования" ],
        [ "36/6", "11/12", "Правоугольныйкрест Эдема" ],
        [ "12/11", "36/6", "Правоугольныйкрест Эдема" ],
        [ "6/36", "12/11", "Правоугольныйкрест Эдема" ],
        [ "11/12", "36/6", "Правоугольныйкрест Эдема" ],
        [ "49/4", "43/23", "Правоугольныйкрест Объяснения" ],
        [ "23/43", "49/", "Правоугольныйкрест Объяснения" ],
        [ "4/49", "23/43", "Правоугольныйкрест Объяснения" ],
        [ "43/23", "4/49", "Правоугольныйкрест Объяснения" ],
        [ "24/44", "19/33", "Правоугольныйкрест Четырех Путей" ],
        [ "33/19", "24/44", "Правоугольныйкрест Четырех Путей" ],
        [ "44/24", "33/19", "Правоугольныйкрест Четырех Путей" ],
        [ "19/33", "44/24", "Правоугольныйкрест Четырех Путей" ],
        [ "3/50", "60/56", "Правоугольныйкрест Законов" ],
        [ "56/60", "3/50", "Правоугольныйкрест Законов" ],
        [ "50/3", "56/60", "Правоугольныйкрест Законов" ],
        [ "60/56", "50/3", "Правоугольныйкрест Законов" ],
        [ "42/32", "61/62", "Правоугольныйкрест Майи" ],
        [ "62/61", "42/32", "Правоугольныйкрест Майи" ],
        [ "32/42", "62/61", "Правоугольныйкрест Майи" ],
        [ "61/62", "32/42", "Правоугольныйкрест Майи" ],
        [ "51/57", "54/53", "Правоугольныйкрест Проникновения" ],
        [ "53/54", "51/57", "Правоугольныйкрест Проникновения" ],
        [ "57/51", "53/54", "Правоугольныйкрест Проникновения" ],
        [ "54/53", "57/51", "Правоугольныйкрест Проникновения" ],
        [ "37/40", "9/16", "Правоугольныйкрест Планирования" ],
        [ "16/9", "37/40", "Правоугольныйкрест Планирования" ],
        [ "40/37", "16/9", "Правоугольныйкрест Планирования" ],
        [ "9/16", "40/37", "Правоугольныйкрест Планирования" ],
        [ "22/47", "26/45", "Правоугольныйкрест Управления" ],
        [ "45/26", "22/47", "Правоугольныйкрест Управления" ],
        [ "47/22", "45/26", "Правоугольныйкрест Управления" ],
        [ "26/45", "47/22", "Правоугольныйкрест Управления" ],
        [ "17/18", "58/52", "Правоугольныйкрест Служения" ],
        [ "52/58", "17/18", "Правоугольныйкрест Служения" ],
        [ "18/17", "52/58", "Правоугольныйкрест Служения" ],
        [ "58/52", "18/17", "Правоугольныйкрест Служения" ],
        [ "55/59", "34/20", "Правоугольныйкрест Спящего Феникса" ],
        [ "20/34", "55/59", "Правоугольныйкрест Спящего Феникса" ],
        [ "59/55", "30/34", "Правоугольныйкрест Спящего Феникса" ],
        [ "34/20", "59/55", "Правоугольныйкрест Спящего Феникса" ],
        [ "13/7", "1/2", "Правоугольныйкрест Сфинкса" ],
        [ "2/1", "13/7", "Правоугольныйкрест Сфинкса" ],
        [ "7/13", "2/1", "Правоугольныйкрест Сфинкса" ],
        [ "1/ 2", "13/7", "Правоугольныйкрест Сфинкса" ],
        [ "21/48", "38/39", "Правоугольныйкрест Напряжения" ],
        [ "39/38", "21/48", "Правоугольныйкрест Напряжения" ],
        [ "48/21", "39/38", "Правоугольныйкрест Напряжения" ],
        [ "38/39", "48/21", "Правоугольныйкрест Напряжения" ],
        [ "27/28", "41/31", "Правоугольныйкрест Неожиданного" ],
        [ "31/41", "27/28", "Правоугольныйкрест Неожиданного" ],
        [ "28/27", "31/41", "Правоугольныйкрест Неожиданного" ],
        [ "41/31", "28/27", "Правоугольныйкрест Неожиданного" ],
        [ "25/46", "10/15", "Правоугольныйкрест Сосуда Любви" ],
        [ "15/10", "25/46", "Правоугольныйкрест Сосуда Любви" ],
        [ "46/25", "15/10", "Правоугольныйкрест Сосуда Любви" ],
        [ "10/15", "46/25", "Правоугольныйкрест Сосуда Любви" ],
    ];

    public static $links = [
        'type'    => [
            '/types/reflector'             => '/category/dizayn_cheloveka/article/2015/09/18/reflektor',
            '/types/manifesting-generator' => '/category/dizayn_cheloveka/article/2015/09/18/manifestiruyuschiy_generator',
            '/types/manifestor'            => '/category/dizayn_cheloveka/article/2015/09/18/manifestor',
            '/types/generator'             => '/category/dizayn_cheloveka/article/2015/09/18/generator',
            '/types/projector'             => '/category/dizayn_cheloveka/article/2015/09/18/proektor',
        ],
        'profile' => [
            '/profiles/profile-1-3' => '/category/dizayn_cheloveka/article/2015/10/11/profil_13_issledovatelmuchenik',
            '/profiles/profile-1-4' => '/category/dizayn_cheloveka/article/2015/10/11/profil_14_issledovatel__opport',
            '/profiles/profile-2-4' => '/category/dizayn_cheloveka/article/2015/10/11/profil_24_otshelnik__opportuni',
            '/profiles/profile-2-5' => '/category/dizayn_cheloveka/article/2015/10/11/profil_25_otshelnik__eretik',
            '/profiles/profile-3-5' => '/category/dizayn_cheloveka/article/2015/10/11/profil_35_muchenik__eretik',
            '/profiles/profile-3-6' => '/category/dizayn_cheloveka/article/2015/10/11/profil_36_muchenik__rolevaya_m',
            '/profiles/profile-4-1' => '/category/dizayn_cheloveka/article/2015/10/11/profil_41_opportunist__issledo',
            '/profiles/profile-4-6' => '/category/dizayn_cheloveka/article/2015/10/11/profil_46_opportunist__rolevay',
            '/profiles/profile-5-1' => '/category/dizayn_cheloveka/article/2015/10/11/profil_51_eretik__issledovatel',
            '/profiles/profile-5-2' => '/category/dizayn_cheloveka/article/2015/10/11/profil_52_eretik__otshelnik',
            '/profiles/profile-6-2' => '/category/dizayn_cheloveka/article/2015/10/11/profil_62_rolevaya_model__otsh',
            '/profiles/profile-6-3' => '/category/dizayn_cheloveka/article/2015/10/11/profil_63_rolevaya_model__much',

        ],
    ];

    /**
     * Вычисляет дизайн на часовой пояс Лондона
     *
     * @param string $reportTicks 346692600000
     * @param string $reportDate 12/26/1980
     * @param string $reportTime 15:30
     *
     * @return \app\models\HumanDesign
     */
    public function calc($reportTicks, $reportDate, $reportTime)
    {
        // изображение
        {
            $url = new \cs\services\Url('https://humandesignamerica.com/index.php');
            $options = [
                'option' => 'com_hdreports',
                'task'   => 'calculate',
                'name'   => 'Ra',
                'ticks'  => 'com_hdreports',
                'place'  => 'United Kingdom - London',
            ];
            foreach($options as $k => $v) {
                $url->addParam($k, $v);
            }
            $options = [
                'reportTicks'   => $reportTicks,
                'reportCountry' => 'United Kingdom',
                'reportCity'    => 'London',
                'reportDate'    => $reportDate,
                'reportTime'    => $reportTime,
                'reportName'    => 'Ra',
                'view'          => 'HDreports',
            ];
            $curl = curl_init($url->__toString());
            curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36');
            curl_setopt($curl, CURLOPT_POST, 1);
            $query = http_build_query($options);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            $body = curl_exec($curl);
            $result = new \StdClass();
            $result->info = curl_getinfo($curl);
            $result->status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $result->body = $body;
            curl_close($curl);
            $data = json_decode($body);
            $BirthDateTicks = $data->d->BirthDateTicks;

            $curl = curl_init('http://cdn.jovianarchive.com/RaveChartGenerator.php?Time=' . $BirthDateTicks);
            curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            $body = curl_exec($curl);
            $result = new \StdClass();
            $result->info = curl_getinfo($curl);
            $result->status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $result->body = $body;
            curl_close($curl);
        }

        $class = new \app\models\HumanDesign($this->getData($reportTicks, $reportDate, $reportTime));
        $class->image = $body;

        return $class;
    }

    /**
     * Получает info дизайна
     *
     * @param string $reportTicks 346692600000
     * @param string $reportDate 12/26/1980
     * @param string $reportTime 15:30
     *
     * @return array
     * [
     *    'image' => string
     *    'type' =>
     *        [
     *             'text'
     *             'href'
     *        ]
     *    'profile' =>
     *        [
     *             'text'
     *             'href'
     *        ]
     *    'definition' =>
     *        [
     *             'text'
     *        ]
     *    'inner' =>
     *        [
     *             'text'
     *        ]
     *    'strategy' =>
     *        [
     *             'text'
     *        ]
     *    'theme' =>
     *        [
     *             'text'
     *        ]
     *    'cross' =>
     *        [
     *             'text'
     *        ]
     * ]
     */
    private function getData($reportTicks, $reportDate, $reportTime)
    {
        $d = explode('/', $reportDate);
        $url = new \cs\services\Url('https://www.mybodygraph.com/free-bodygraph');
        $options = [
            'place' => 'London, United Kingdom',
            'date'   => $d[2].'-'.$d[1].'-'.$d[0],
            'time'   => $reportTime,
        ];
        foreach($options as $k => $v) {
            $url->addParam($k, $v);
        }
        $curl = curl_init($url->__toString());
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $body = curl_exec($curl);
        $result = new \StdClass();
        $result->info = curl_getinfo($curl);
        $result->status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $result->body = $body;

        return $this->getData2($body);
    }

    /**
     * Получает info дизайна
     *
     * @param $html
     *
     * @return array
     * [
     *    'image' => string
     *    'type' =>
     *        [
     *             'text'
     *             'href'
     *        ]
     *    'profile' =>
     *        [
     *             'text'
     *             'href'
     *        ]
     *    'definition' =>
     *        [
     *             'text'
     *        ]
     *    'inner' =>
     *        [
     *             'text'
     *        ]
     *    'strategy' =>
     *        [
     *             'text'
     *        ]
     *    'theme' =>
     *        [
     *             'text'
     *        ]
     *    'cross' =>
     *        [
     *             'text'
     *        ]
     * ]
     */
    private function getData2($html)
    {
        $rows = explode('container-chart-properties', $html);
        $rows = explode('</table>', $rows[1]);
        $trArray = explode('</tr>', $rows[0]);
        // $Type Тип
        $href = '';
        {
            $str = $this->getTag($trArray[2], 'td');
            switch($str) {
                case 'Manifesting Generator':
                    $str = 'Манифестирующий генератор';
                    $href = 'manifesting-generator';
                    break;
                case 'Reflector':
                    $str = 'Рефлектор';
                    $href = 'reflector';
                    break;
                case 'Manifestor':
                    $str = 'Манифестор';
                    $href = 'manifestor';
                    break;
                case 'Generator':
                    $str = 'Генератор';
                    $href = 'generator';
                    break;
                case 'Projector':
                    $str = 'Проектор';
                    $href = 'projector';
                    break;
            }
            $Type = [
                'text' => $str,
                'href' => '/types/'.$href,
            ];
        }
        // $Profile Профиль
        {
            $str =  $str = $this->getTag($trArray[8], 'td');
            $code =  $str = $this->getBetween($str, '(',')');
            $Profile = [
                'text' => $code,
                'href' => '/profiles/profile-' . str_replace('/', '-', $code),
            ];
        }
        // $Definition Определение
        {
            $str = $this->getTag($trArray[6], 'td');
            $Definition = [
                'text' => $this->translate($str, [
                    [
                        'key' => 'Split',
                        'translate' => 'Двойное',
                    ],
                    [
                        'key' => 'Single',
                        'translate' => 'Одинарное',
                    ],
                    [
                        'key' => 'Triple-Split',
                        'translate' => 'Тройное',
                    ],
                ]),
            ];
        }
        // $Inner Внутренний Авторитет
        {
            $str = $this->getTag($trArray[7], 'td');
            $Inner = [
                'text' => $this->translate($str, [
                    [
                        'key'       => 'Ego Projected',
                        'translate' => 'Ego Projected',
                    ],
                    [
                        'key'       => 'Splenic',
                        'translate' => 'Селезеночный',
                    ],
                    [
                        'key'       => 'Solar Plexus - Emotional',
                        'translate' => 'Солнечное Сплетение - Эмоциональный',
                    ],
                    [
                        'key'       => 'Sacral',
                        'translate' => 'Сакрал',
                    ],
                ]),
            ];
        }
        // $Strategy Стратегия
        {
            $str = $this->getTag($trArray[3], 'td');
            $Strategy = [
                'text' => $this->translate($str, [
                    [
                        'key'       => 'To Respond',
                        'translate' => 'Откликаться',
                    ],
                    [
                        'key'       => 'Wait for the Invitation',
                        'translate' => 'Ждать приглашения',
                    ],
                    [
                        'key'       => 'To Inform',
                        'translate' => 'Информировать',
                    ],
                ]),
            ];
        }
        // $Theme Тема ложного Я
        {
            $str = $this->getTag($trArray[4], 'td');
            $Theme = [
                'text' => $this->translate($str, [
                    [
                        'key'       => 'Bitterness',
                        'translate' => 'Горечь',
                    ],
                    [
                        'key'       => 'Frustration',
                        'translate' => 'Фрустрация',
                    ],
                    [
                        'key'       => 'Anger',
                        'translate' => 'Гнев',
                    ],
                ]),
            ];
        }
        // $Cross Инкарнационный крест
        {
            $str = $this->getTag($trArray[9], 'td');
            $Cross = [
                'text' => $this->getCross($str),
            ];
        }
        // $Signature Подпись
        {
            $str = $this->getTag($trArray[5], 'td');
            $Signature = [
                'text' => $this->translate($str, [
                    [
                        'key' => 'Success',
                        'translate' => 'Успех',
                    ],
                    [
                        'key' => 'Peace',
                        'translate' => 'Мир',
                    ],
                    [
                        'key' => 'Satisfaction',
                        'translate' => 'Удовлетворение',
                    ],
                ]),
            ];
        }
        // $Extended
        {
            $rows = explode('require.config.globals.bodyGraphChart = ', $html);
            $rows = explode('</script>', $rows[1]);
            $rows = trim($rows[0]);
            $rows = substr($rows, 0, strlen($rows)-1);
            $Extended = $rows;
        }

        return [
            'type'           => $Type,
            'profile'        => $Profile,
            'definition'     => $Definition,
            'inner'          => $Inner,
            'strategy'       => $Strategy,
            'theme'          => $Theme,
            'cross'          => $Cross,
            'signature'      => $Signature,
            'extended'       => $Extended,
        ];
    }

    /**
     * @param string $text text with (10/15 | 17/18)
     * @return string
     */
    private function getCross($text)
    {
        $arr = explode('(', $text);
        $text = trim($arr[0]);
        $arr = explode(')', $arr[1]);
        $arr = explode('|', $arr[0]);
        $arr[0] = trim($arr[0]);
        $arr[1] = trim($arr[1]);
        foreach(self::$cross as $item) {
            if (($item[0] == $arr[0]) and ($item[1] == $arr[1])) {
                return $item[2] . ' (' . $item[0] . ' | ' . $item[1] . ')';
            }
        }

        return $text;
    }

    /**
     *
     * @param $str
     * @param array $arr
     * [
     *     'key'       => ''
     *     'translate' => ''
     * ]
     *
     * @return str
     */
    public function translate($str, $arr)
    {
        foreach($arr as $i) {
            if ($i['key'] == $str) {
                return $i['translate'];
            }
        }

        return $str;
    }

    /**
     * Возвращает текст между символами и убирает пробелы по бокам
     * @param string $text
     * @param string $in
     * @param string $out
     * @return string
     */
    function getBetween($text,$in,$out)
    {
        $items = explode($in, $text);
        $items = explode($out, $items[1]);

        return trim($items[0]);
    }

    /**
     * Возвращает текст между тегов и убирает пробелы по бокам
     * @param $text
     * @param $tag
     * @return string
     */
    function getTag($text, $tag)
    {
        return $this->getBetween($text,'<'.$tag.'>','</'.$tag.'>');
    }
} 