<?php

namespace app\modules\Shop\services;

use app\models\Shop\TreeNodeGeneral;
use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class Shop
{

    /**
     * Возвращает дерево
     *
     * @param int $parentId
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public static function getTree($parentId = null)
    {
        $rows = (new Query())
            ->select('id, name, id_string')
            ->from(TreeNodeGeneral::TABLE)
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC])
            ->all();
        for($i = 0; $i < count($rows); $i++ ) {
            $item = &$rows[$i];
            $rows2 = self::getTree($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }
}