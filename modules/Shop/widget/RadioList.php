<?php

namespace app\modules\Shop\widget;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;

/*
$rows = array gs_unions_shop_dostavka.*
*/


class RadioList extends InputWidget
{
    /**
     * @var string the template for arranging the CAPTCHA image tag and the text input tag.
     * In this template, the token `{image}` will be replaced with the actual image tag,
     * while `{input}` will be replaced with the text input tag.
     */
    public $template = '{input}{label1}{label2}';
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [
    ];

    public $rows;

    private $attrId;
    private $attrName;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->attrId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->attrName = $this->model->formName() . '[' . $this->attribute . ']';
    }

    /**
     * Renders the widget.
     * <div class="custom_label">
     * <input type="radio" class="" id="{*some_id*}" >
     * <label for="{*some_id*}" class="radio_imitation"></label>
     * <label for="{*some_id*}" class="radio_text">{*text*}</label>
     * </div>
     */
    public function run()
    {
        $this->registerClientScript();
        $html = [];
        if ($this->hasModel()) {
            $fieldName = $this->attribute;
            $valueField = $this->model->$fieldName;
            foreach ($this->rows as $i) {
                $item = [];
                $id = $this->attrId . '-' . $i['id'];
                $item[] = Html::radio($this->attrName, null, [
                    'id'    => $id,
                    'value' => $i['id'],
                    'data'  => [
                        'price'           => $i['price'],
                        'is_need_address' => $i['is_need_address'],
                    ],
                ]);
                $item[] = ' ';
                $item[] = Html::label($i['name'], $id);
                if ($i['price'] > 0) {
                    $item[] = ' ';
                    $item[] = Html::tag('span', $i['price'] . ' руб.');
                }
                $html[] = Html::tag('div', join('', $item));
            }
        }

        echo join('', $html);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
    }

    /**
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     * Берет значения из POST и возвраает знаяения для добавления в БД
     *
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array
     */
    public static function onUpdate($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];
        $value = ArrayHelper::getValue(\Yii::$app->request->post(), $model->formName() . '.' . $fieldName, null);
        if ($value == '') $value = null;

        return [$fieldName => $value];
    }

    /**
     * Рисует просмотр файла для детального просмотра
     *
     * @param \yii\base\Model $model
     * @param array $field
     *
     * @return string
     */
    public static function drawDetailView($model, $field)
    {
        $fieldName = $field[ \cs\base\BaseForm::POS_DB_NAME ];
        $value = $model->$fieldName;
        if (is_null($value)) {
            return ArrayHelper::getValue($value, 'type.1.nullString', '<Нет значения>');
        }
        $list = ArrayHelper::getValue($value, 'type.1.list', []);

        return $list[ $value ];
    }
}
