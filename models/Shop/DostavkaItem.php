<?php

namespace app\models\Shop;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class DostavkaItem extends \cs\base\DbRecord
{
    const TABLE = 'gs_unions_shop_dostavka';

}