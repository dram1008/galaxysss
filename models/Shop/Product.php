<?php

namespace app\models\Shop;

use app\models\Form\Shop\ProductImage;
use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class Product extends \cs\base\DbRecord
{
    const TABLE = 'gs_unions_shop_product';
    const MODERATION_STATUS_SEND = 2;

    private $union;

    /**
     * Возвращает картинку
     * @param bool $isScheme
     * @return string
     */
    public function getImage($isScheme = false)
    {
        $url = $this->getField('image', '');
        if ($url == '') return '';

        return Url::to($url, $isScheme);
    }

    public function accept()
    {
        $this->update(['moderation_status' => 1]);
        // ставлю всем изображениям статус отмодерировано
        (new Query())->createCommand()->update(\app\models\Shop\ProductImage::TABLE, ['moderation_status' => 1], ['product_id' => $this->getId()])->execute();
    }

    public function reject()
    {
        $this->update(['moderation_status' => 0]);
    }

    /**
     * @return \app\models\Union
     */
    public function getUnion()
    {
        if (!$this->union) {
            $this->union = Union::find($this->getField('union_id'));
        }

        return $this->union;
    }

    /**
     * Выдает ссылку на товар внутри магазина
     *
     * @param bool $isScheme
     *
     * @return string
     */
    public function getLink($isScheme = false)
    {
        $union = $this->getUnion();

        return Url::to(['union_shop/product', 'union_id' => $union->getId(), 'id' => $this->getId(), 'category' => $union->getCategory()->getField('id_string')], $isScheme);
    }

    /**
     * Возвращает заготовку запроса по выборке изображений
     * без сортировки
     *
     * @return Query
     */
    public function getImages()
    {
        return \app\models\Shop\ProductImage::query(['product_id' => $this->getId()]);
    }
}