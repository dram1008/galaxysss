<?php

namespace app\models\Shop;

use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class ProductImage extends \cs\base\DbRecord
{
    const TABLE = 'gs_unions_shop_product_images';

    /**
     * Возвращает картинку
     * @param bool $isScheme
     * @return string
     */
    public function getImage($isScheme = false)
    {
        $url = $this->getField('image', '');
        if ($url == '') return '';

        return Url::to($url, $isScheme);
    }

    public function accept()
    {
        $this->update(['moderation_status' => 1]);
    }

    public function reject()
    {
        $this->update(['moderation_status' => 0]);
    }

    /**
     * @return \app\models\Shop\Product
     */
    public function getProduct()
    {
        return Product::find($this->getField('product_id'));
    }
}