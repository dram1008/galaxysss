<?php

namespace app\models\Shop;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;

class TreeNodeGeneral extends \cs\base\DbRecord
{
    const TABLE = 'gs_shop_tree';
}