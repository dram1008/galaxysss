<?php

namespace app\models\Shop;

use app\models\Shop;
use app\models\Union;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Request extends \cs\base\DbRecord
{
    const TABLE = 'gs_users_shop_requests';

    const STATUS_USER_NOT_CONFIRMED = 1; // Пользователь заказал с регистрацией пользователя, но не подтвердил свою почту еще
    const STATUS_SEND_TO_SHOP = 2;       // заказ отправлен в магазин
    const STATUS_ORDER_DOSTAVKA = 3;     // клиенту выставлен счет с учетом доставки
    const STATUS_PAID_CLIENT = 5;        // заказ оплачен со стороны клиента
    const STATUS_PAID_SHOP = 6;          // оплата подтверждена магазином

    const STATUS_SEND_TO_USER = 20;          // Послан клиенту
    const STATUS_FINISH_CLIENT = 21;          // Послан клиенту

    // статусы для доставки
    const STATUS_DOSTAVKA_ADDRESS_PREPARE = 8;            // заказ формируется для отправки клиенту
    const STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE = 9;       // заказ сформирован для отправки клиенту
    const STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER = 7;       // заказ отправлен клиенту
    const STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT = 10;     // заказ исполнен, как сообщил клиент
    const STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP = 11;       // заказ исполнен, магазин сам указал этот статус по своим данным

    // статусы для самовывоза
    const STATUS_DOSTAVKA_SAMOVIVOZ_WAIT = 13;             // заказ ожидает клиента
    const STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT = 14;    // клиент сообщил что получил товар
    const STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP = 15;      // заказ исполнен, магазин сам указал этот статус по своим данным


    const STATUS_DONE = 12;              // заказ исполнен

    const DIRECTION_TO_CLIENT = 1;
    const DIRECTION_TO_SHOP   = 2;


    public static $statusList = [
        self::STATUS_USER_NOT_CONFIRMED => [
            'client' => 'Пользователь не подтвердил почту',
            'shop'   => 'Пользователь не подтвердил почту',
            'timeLine' => [
                'icon'  => 'glyphicon-minus',
                'color' => 'default',
            ],
        ],
        self::STATUS_SEND_TO_SHOP       => [
            'client'   => 'Отпрален в магазин',
            'shop'     => 'Пользователь отправил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-ok',
                'color' => 'default',
            ],
        ],
        self::STATUS_ORDER_DOSTAVKA     => [
            'client'   => 'Выставлен счет с учетом доставки',
            'shop'     => 'Выставлен счет с учетом доставки',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'default',
            ],
        ],
        self::STATUS_PAID_CLIENT        => [
            'client'   => 'Оплата сделана',
            'shop'     => 'Оплата сделана',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'warning',
            ],
        ],
        self::STATUS_PAID_SHOP          => [
            'client'   => 'Оплата подтверждена',
            'shop'     => 'Оплата подтверждена',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'success',
            ],
        ],
        self::STATUS_DONE        => [
            'client'   => 'Заказ выполнен',
            'shop'     => 'Заказ выполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_PREPARE        => [
            'client'   => 'Заказ формируется для отправки клиенту',
            'shop'     => 'Заказ формируется для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'default',
            ],
        ],


        self::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE        => [
            'client'   => 'Заказ сформирован для отправки клиенту',
            'shop'     => 'Заказ сформирован для отправки клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER        => [
            'client'   => 'Заказ отправлен клиенту',
            'shop'     => 'Заказ отправлен клиенту',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT        => [
            'client'   => 'Заказ исполнен, как сообщил клиент',
            'shop'     => 'Заказ исполнен, как сообщил клиент',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP        => [
            'client'   => 'Заказ выполнен',
            'shop'     => 'Заказ выполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT        => [
            'client'   => 'Заказ ожидает клиента',
            'shop'     => 'Заказ ожидает клиента',
            'timeLine' => [
                'icon'  => 'glyphicon-plane',
                'color' => 'warning',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT        => [
            'client'   => 'Клиент сообщил что получил товар',
            'shop'     => 'Клиент сообщил что получил товар',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
        self::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP        => [
            'client'   => 'Заказ исполнен',
            'shop'     => 'Заказ исполнен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],


        self::STATUS_SEND_TO_USER        => [
            'client'   => 'Оплата сделана',
            'shop'     => 'Оплата сделана',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'success',
            ],
        ],

        self::STATUS_FINISH_CLIENT        => [
            'client'   => 'Оплата сделана',
            'shop'     => 'Оплата сделана',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'success',
            ],
        ],
    ];


    /**
     * Устанавливает статус для заказа "Оплата подтверждена магазином" self::STATUS_PAID_SHOP
     *
     * @param string $message - сообщение для статуса
     *
     * @return bool
     */
    public function paid($message = null)
    {
        $this->addStatusToShop(self::STATUS_PAID_CLIENT); // клиент совершил оплату
        $this->addStatusToClient(self::STATUS_PAID_SHOP); // магазин подтвердил оплату
        $this->update(['is_paid' => 1]);
        $dostavka = $this->getDostavkaItem();
        if ($dostavka->getField('is_need_address', 0) == 1) {
            // если есть адрес
            $this->addStatusToClient(self::STATUS_DOSTAVKA_ADDRESS_PREPARE);
        } else {
            $this->addStatusToClient(self::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT);
        }
        $shop = $this->getUnion()->getShop();
        Shop::mail($shop->getAdminEmail(), 'Оплата подтверждена по заказу #' . $this->getId() , 'shop/request_shop_answer_pay', [
            'request'    => $this,
            'text'       => '',
        ]);

        return true;
    }


    /**
     * Возвращает тип доставки текстом
     * @return string
     */
    public function getDostavkaText()
    {
        $item = $this->getDostavkaItem();
        if (is_null($item)) return '';

        return $item->getField('name');
    }

    /**
     * Возвращает тип доставки текстом
     * @return \app\models\Shop\DostavkaItem | null
     */
    public function getDostavkaItem()
    {
        return DostavkaItem::find($this->getField('dostavka'));
    }

    /**
     * @param array $fields
     * @return \app\models\Shop\Request
     */
    public static function insert($fields = [])
    {
        if (!isset($fields['user_id'])) {
            $fields['user_id'] = \Yii::$app->user->id;
        }
        $fields['date_create'] = time();

        return parent::insert($fields);
    }

    /**
     * Добавить статус
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatus($status, $direction)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessage($message, $direction)
    {
        return $this->addMessageItem([
            'message' => $message,
        ], $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \app\models\Shop\RequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $fieldsRequest = [
            'is_answer_from_shop'   => ($direction == self::DIRECTION_TO_CLIENT) ? 1 : 0,
            'is_answer_from_client' => ($direction == self::DIRECTION_TO_CLIENT) ? 0 : 1,
            'last_message_time'     => time(),
        ];
        if (isset($fields['status'])) {
            $fieldsRequest['status'] = $fields['status'];
        }
        if ($direction == self::DIRECTION_TO_SHOP) {
            $fieldsRequest['is_hide'] = null;
        }
        $this->update($fieldsRequest);

        return RequestMessage::insert(ArrayHelper::merge($fields, [
            'request_id' => $this->getId(),
            'direction'  => $direction,
            'datetime'   => time(),
        ]));
    }

    /**
     * Добавляет заказ с продуктами
     *
     * @param $fields array поля заказа
     * @param $productList array список продуктов в заказе
     * [
     *    [
     * 'id'     => int,
     * 'count'  => int,
     *    ], ...
     * ]
     * @return self
     */
    public static function add($fields, $productList, $status = self::STATUS_SEND_TO_SHOP)
    {
        $request = self::insert($fields);
        foreach ($productList as $item) {
            RequestProduct::insert([
                'request_id' => $request->getId(),
                'product_id' => $item['id'],
                'count'      => $item['count'],
            ]);
        }
        $message = $request->addStatus($status, self::DIRECTION_TO_SHOP);

        return $request;
    }

    /**
     * Возвращает заготовку запроса для списка товаров для заказа
     * gs_unions_shop_product.*
     *
     * @return \yii\db\Query
     */
    public function getProductList()
    {
        return RequestProduct::query(['request_id' => $this->getId()])
            ->select([
                'gs_unions_shop_product.*',
                'gs_users_shop_requests_products.count',
            ])
            ->innerJoin('gs_unions_shop_product', 'gs_unions_shop_product.id = gs_users_shop_requests_products.product_id');
    }

    /**
     * Возвращает заготовку запроса для сообщений для заказа
     * gs_users_shop_requests_messages.*
     *
     * @return \yii\db\Query
     */
    public function getMessages()
    {
        return RequestMessage::query(['request_id' => $this->getId()]);
    }

    /**
     * Возвращает объединение на которое оформлен заказ
     *
     * @return \app\models\Union
     */
    public function getUnion()
    {
        return Union::find($this->getField('union_id'));
    }

    /**
     * Возвращает объект клиента
     *
     * @return \app\models\User | null
     */
    public function getUser()
    {
        return User::find($this->getField('user_id'));
    }

    /**
     * Возвращает объект клиента
     *
     * @return \app\models\User | null
     */
    public function getClient()
    {
        return $this->getUser();
    }

    public function getStatus()
    {
        return $this->getField('status');
    }
}