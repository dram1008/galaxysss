<?php

namespace app\models;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\db\Query;
use yii\helpers\Url;

class Help extends \cs\base\DbRecord
{
    const TABLE = 'gs_help';


    public function getImage($isScheme = false)
    {
        $image = $this->getField('image', '');
        if ($image == '') return '';

        return \yii\helpers\Url::to($this->getField('image'), $isScheme);
    }
}