<?php
/**
 * Объект рассылки
 */

namespace app\models;


use yii\base\Object;

class SubscribeItem extends Object
{
    public $subject;
    public $text;
    public $html;
    public $type;
}