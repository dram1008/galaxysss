<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:24
 */

class Yandex implements \app\models\Piramida\WalletDestinationInterface
{
    public function getTransactionInfo()
    {
        return [
            'to'       => 'Святослав Архангельский',
            'bill'     => 666,
            'datetime' => time(),
        ];
    }

    public function getTypeId()
    {
        return 1;
    }
}