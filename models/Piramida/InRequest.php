<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * Заявка на пополнение кошелька
 *
 * @property int wallet_id - идентификатор кошелька
 * @property int transaction_id - транзакция для опереции начисления
 * @property int datetime - время операции
 * @property float price - размер пополнения
 * @property int is_paid - размер пополнения
 *
 * Class InRequest
 * @package app\models\Piramida
 */
class InRequest extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_in_requests';
    }

    public function rules()
    {
        return [
            [['wallet_id', 'price', 'datetime',], 'required'],
            [['transaction_id', 'is_paid', 'datetime', 'wallet_id'], 'integer'],
            [['price',], 'double'],
        ];
    }
}