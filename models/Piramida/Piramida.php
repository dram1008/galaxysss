<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

class Piramida extends ActiveRecord
{
    /**
     * @return Wallet
     */
    public static function getGlobalWallet()
    {
        return Wallet::findOne(126);
    }
}