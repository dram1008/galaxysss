<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;

class WalletSource extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_wallets_sources_link';
    }

    public function rules()
    {
        return [
            [['source_id', 'data'], 'required'],
            [['source_id'], 'integer'],
            [['data'], 'string', 'max' => 4000],
        ];
    }
}