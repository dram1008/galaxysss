<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Transaction;

/**
 */
interface WalletDestinationInterface
{
    /**
     * @return array информация о транзакции в платежной системе
     */
    public function getTransactionInfo();

    /**
     * Возвращает идентификатор источника
     *
     * @return mixed
     */
    public function getTypeId();
}