<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * Элементарная операция с кошельком
 * Всего их две:
 * - снять деньги
 * - положить деньги
 * За это отвечают поле `type` и константы self::IN и self::OUT
 * содержит информацию о кошельке с которым производится операция
 * @property int wallet_id - идентификатор кошелька
 * @property int transaction_id - транзакция к которой принадлежит данная операция
 * @property int datetime - время операции
 * @property float before - размер кошелька до операции
 * @property float after - размер кошелька после операции
 * @property float summa - денежный размер операции
 *
 *
 * Class Operation
 * @package app\models\Piramida
 */
class Operation extends ActiveRecord
{
    const IN = 1;
    const OUT = 0;

    public static function tableName()
    {
        return 'nw_operations';
    }

    public function rules()
    {
        return [
            [['wallet_id', 'before', 'after', 'transaction_id', 'summa', 'type', 'datetime'], 'required'],
            [['wallet_id', 'transaction_id', 'type'], 'integer'],
            [['before', 'after', 'summa', 'datetime'], 'double'],
        ];
    }
}