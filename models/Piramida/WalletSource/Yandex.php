<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace app\models\Piramida\WalletSource;

use yii\base\Object;

class Yandex extends Object implements \app\models\Piramida\WalletSourceInterface
{
    public $transaction;

    public function getTransactionInfo()
    {
        return [
            'id' => '410011473018906',
            'transaction' => $this->transaction,
        ];
    }

    public function getTypeId()
    {
        return 2;
    }
} 