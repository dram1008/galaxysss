<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace app\models\Piramida\WalletSource;

class God implements \app\models\Piramida\WalletSourceInterface
{
    public function getTransactionInfo()
    {
        return [
            'from' => 'Бог Творец',
            'bill' => 777,
        ];
    }

    public function getTypeId()
    {
        return 1;
    }
} 