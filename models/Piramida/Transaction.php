<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * 1. Что делает?
 * Хранит информацию о транзакции
 * Содержит
 * кошелек отправителя `from`
 * кошелек получателя `to`
 * сумма перевода `summa`
 * время перевода `datetime`
 * комментарий для транзакции comment например "принадлежит бизнес процессу ##", макс 1000 символов
 *
 *
 * Class Transaction
 * @package app\models\Piramida
 */
class Transaction extends ActiveRecord
{
    public function init()
    {
        if (is_null($this->datetime)) $this->datetime = time();
    }

    public static function tableName()
    {
        return 'nw_transactions';
    }

    public function rules()
    {
        return [
            [['summa', 'datetime'], 'required'],
            [['from', 'to'], 'integer'],
            [['summa', 'datetime'], 'double'],
        ];
    }
}