<?php

namespace app\models\Piramida;

use app\services\Subscribe;
use app\services\UsersInCache;
use cs\services\BitMask;
use cs\services\VarDumper;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\models\Piramida\Transaction;

/**
 * 1. Что делает?
 * Описывает класс кошелька
 *
 * 2. какие есть свойства объекта?
 * @property int id
 * @property float value содержит только велчину кошелька `value`
 * @property int parent_id хранит идентификатор человека от которого он пришел `parent_id`
 *
 * 3. кикими функциями обладает?
 * Имеет только три функции
 * - Ввести деньги из внешней системы
 * - Вывести деньги на внешнюю систему
 * - Перевести средства внутри системы
 *
 * Во время каждой функции создается транзакция и операции для нее
 * Операций для "Ввести" и "Вывести" всего одна (начисление или вычитание) так как не указывается второй кошелек,
 * так как он вне системы
 *
 * Class Wallet
 *
 * @package app\models\Piramida
 */
class Wallet extends ActiveRecord
{
    public $childs;

    public static function findOne($i)
    {
        $v = parent::findOne($i);
        if (is_null($v)) $v = new self([
            'id'    => $i,
            'value' => 0,
        ]);

        return $v;
    }

    public static function tableName()
    {
        return 'nw_wallets';
    }

    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'double'],
            [['parent_id'], 'integer'],
        ];
    }

    /**
     * увеличивает свой кошелек и сохраняет
     *
     * @param float $value может быть только положительным
     * @param \app\models\Piramida\Transaction $transaction транзакция которой принадлежит эта операция
     *
     * @return \app\models\Piramida\Operation
     *
     * @throws \Exception
     */
    private function add($value, $transaction)
    {
        if ($value < 0) throw new Exception('Нельзя прибавлять отрицательную сумму');
        $before = $this->value;
        $this->value += $value;
        $after = $this->value;
        $this->save();
        $o = new Operation([
            'wallet_id'      => $this->id,
            'transaction_id' => $transaction->id,
            'type'           => Operation::IN,
            'datetime'       => microtime(true),
            'before'         => $before,
            'after'          => $after,
            'summa'          => $value,
        ]);
        $o->save();

        return $o;
    }

    /**
     * уменьшает свой кошелек и сохраняет
     *
     * @param float $value может быть только положительным
     * @param \app\models\Piramida\Transaction $transaction транзакция которой принадлежит эта операция
     *
     * @return \app\models\Piramida\Operation
     *
     * @throws Exception
     */
    private function sub($value, $transaction)
    {
        if ($value < 0) throw new Exception('Нельзя вычитать отрицательную сумму');
        if ($this->value < $value) throw new Exception('Попытка снять денег больше сколько есть на счету');
        $before = $this->value;
        $this->value -= $value;
        $after = $this->value;
        $this->save();
        $o = new Operation([
            'wallet_id'      => $this->id,
            'transaction_id' => $transaction->id,
            'type'           => Operation::OUT,
            'datetime'       => microtime(true),
            'before'         => $before,
            'after'          => $after,
            'summa'          => $value,
        ]);
        $o->save();

        return $o;
    }

    /**
     * Осоуществляет элементарную операцию движения средств с созданием транзакции.
     * Вычитает с кошелька источника.
     * Добавляет кошельку получателя.
     * Запиывает трензакционную запись о переводе.
     *
     * @param int | \app\models\Piramida\Wallet $to
     * @param float $sum может быть только положительным
     * @param string $comment комментарий для перевода
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function move($to, $sum, $comment = null)
    {
        if (!is_object($to)) $to = Wallet::findOne($to);
        /** @var \app\models\Piramida\Wallet $to */
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // делаю перевод
            $fields = [
                'from'     => $this->id,
                'to'       => $to->id,
                'summa'    => $sum,
                'datetime' => microtime(true),
            ];
            if (!is_null($comment)) $fields['comment'] = $comment;
            $t = new Transaction($fields);
            $t->save();
            $this->sub($sum, $t);
            $to->add($sum, $t);
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();

            throw $e;
        }

        return true;
    }

    /**
     * Начисляет деньги на кошелек из внешней платежной системы.
     * Создает транзакционную запись и сохраняет информацию о том, откуда пришли деньги.
     *
     * @param float $summa
     * @param WalletSourceInterface $source данные о транзакции внешней платежной системы
     * @param string $comment комментарий для перевода
     *
     * @return \app\models\Piramida\Transaction
     *
     * @throws \Exception
     */
    public function in($summa, $source, $comment = null)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // делаю перевод
            {
                $fields = [
                    'to'       => $this->id,
                    'summa'    => $summa,
                    'datetime' => microtime(true),
                ];
                if (!is_null($comment)) $fields['comment'] = $comment;
                $t = new Transaction($fields);
                $t->save();
            }
            $this->add($summa, $t);
            // делаю запись об источнике денег
            {
                $s = new \app\models\Piramida\WalletSource([
                    'id'        => $t->id,
                    'source_id' => $source->getTypeId(),
                    'data'      => json_encode($source->getTransactionInfo()),
                ]);
                $s->save();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();

            throw $e;
        }

        return $t;
    }

    /**
     * Выводит деньги с кошелка на внешнюю платежную систему.
     * Вычитает деньги из кошелька.
     * Создает транзакционную запись и сохраняет информацию о том, кула ушли деньги.
     *
     * @param float $summa
     * @param WalletDestinationInterface $destination данные о транзакции внешней платежной системы
     * @param string $comment комментарий для перевода
     *
     * @return \app\models\Piramida\Transaction
     *
     * @throws \Exception
     */
    public function out($summa, $destination, $comment = null)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // делаю перевод
            {
                $fields = [
                    'from'     => $this->id,
                    'summa'    => $summa,
                    'datetime' => microtime(true),
                ];
                if (!is_null($comment)) $fields['comment'] = $comment;
                $t = new Transaction($fields);
                $t->save();
            }
            $this->sub($summa, $t);
            // делаю запись об транзакции выхода денег в банке получателя
            $s = new \app\models\Piramida\WalletDestination([
                'id'             => $t->id,
                'destination_id' => $destination->getTypeId(),
                'data'           => json_encode($destination->getTransactionInfo()),
            ]);
            $s->save();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();

            throw $e;
        }

        return $t;
    }

    public function getChilds()
    {
        return $this->childs;
    }

    public function setChilds($value)
    {
        $this->childs = $value;
    }

    /**
     * Возвращает дерево детей
     */
    public function _getChilds()
    {
        $arr = self::find()->where(['parent_id' => $this->id])->asArray()->all();
        for($i = 0; $i < count($arr); $i++) {
            $item = &$arr[$i];
            $item['childs'] = (new self($item))->_getChilds();
        }

        return $arr;
    }

    /**
     * @return array
     */
    public function getUserCached()
    {
        return UsersInCache::find($this->id);
    }
}