<?php

namespace app\models\Form\admin;

use app\models\Form\Stock;
use app\models\User;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class UserSearch extends \app\models\Form\admin\User
{

    public function rules()
    {
        return [
            [
                ['email'], 'default'
            ]
        ];
    }

    public function search($params)
    {
        $query = \app\models\User::query()->orderBy(['id' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
