<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\User;
use app\services\GsssHtml;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 * ContactForm is the model behind the contact form.
 */
class Help extends \cs\base\BaseForm
{
    const TABLE = 'gs_help';

    public $id;
    public $name;
    public $content;
    public $date_insert;
    public $tree_node_id;
    public $sort_index;
    public $image;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            ['image', 'Картинка для репоста', 0, 'default', 'widget' => [FileUpload::className(), ['options' => [
                'small' => [
                    800,
                    400,
                    \cs\Widget\FileUpload2\FileUpload::MODE_THUMBNAIL_CUT
                ]
            ]]]],
            [
                'tree_node_id',
                'Категории',
                0,
                'default',
                'widget' => [
                    'cs\Widget\RadioListTree\RadioListTree',
                    [
                        'tableName' => 'gs_help_tree',
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }
}
