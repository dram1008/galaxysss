<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\Shop;
use app\models\User;
use app\services\GsssHtml;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class Video extends \cs\base\BaseForm
{
    const TABLE = 'gs_video';

    public $id;
    public $tree_node_id;
    public $name;
    public $url;
    public $content;
    public $content_video;
    public $description;
    public $image;
    public $datetime_insert;
    public $torrent_file;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'name',
                'Название',
                0,
                'string'
            ],
            [
                'url',
                'Ссылка',
                1,
                'string'
            ],
            [
                'description',
                'Описание',
                0,
                'string'
            ],
            [
                'content_video',
                'content_video',
                0,
                'string'
            ],
            [
                'content',
                'Полное описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'torrent_file',
                'Торрент файл',
                0,
                'default',
                [],
                'widget' => ['cs\Widget\FileUpload5\FileUpload']
            ],
            [
                'image',
                'Картинка',
                0,
                'default', [],
                'widget' => [FileUpload::className(), ['options' => [
                    'small' => [
                        600,
                        337,
                        FileUpload::MODE_THUMBNAIL_CUT
                    ] // формат 16:9
                ]]]
            ],
            [
                'tree_node_id',
                'Категория',
                0,
                'default',
                'widget' => [
                    'cs\Widget\RadioListTree\RadioListTree',
                    [
                        'tableName' => 'gs_video_tree',
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }

    public function insert($fields = null)
    {
        $fields = parent::insert([
            'beforeInsert' => function ($fields) {
                $fields['datetime_insert'] = time();
                return $fields;
            }
        ]);
        if ($fields === false) return false;

        $item = new \app\models\Video($fields);
        $url = new Url($this->url);
        $fields = [];
        if (strpos($url->host, 'youtube.com') !== false) {
            /**
             * {
             * "width": 480,
             * "provider_name": "YouTube",
             * "author_url": "https://www.youtube.com/channel/UCNYAASJIa8RjetgEpNrvj0g",
             * "title": "6 -15  Импульс  Позитронов.",
             * "provider_url": "https://www.youtube.com/",
             * "version": "1.0",
             * "thumbnail_height": 360,
             * "type": "video",
             * "thumbnail_width": 480,
             * "html": "<iframe width=\"480\" height=\"270\" src=\"https://www.youtube.com/embed/W9zlKIbBfcw?feature=oembed\" frameborder=\"0\" allowfullscreen></iframe>",
             * "thumbnail_url": "https://i.ytimg.com/vi/W9zlKIbBfcw/hqdefault.jpg",
             * "height": 270,
             * "author_name": "32 Импульса"
             * }
             */
            // youTube
            if ($item->get('content_video') == '') {
                $fields['content_video'] = \yii\helpers\Html::tag('iframe', null, [
                    'width'       => '100%',
                    'height'      => '300',
                    'src'         => 'https://www.youtube.com/embed/' . $url->getParam('v'),
                    'frameborder' => 0,
                ]);
            }
            if ($item->get('image') == '') {
                $data = file_get_contents(
                    'http://img.youtube.com/vi/' . $url->getParam('v') . '/0.jpg'
                );
                $fields = ArrayHelper::merge($fields, FileUpload::save(File::content($data), 'jpg', self::findField('image'), $this));
            }
        }
        if (strpos($url->host, 'vimeo.com') !== false) {
            /**
             * {
             * "type": "video",
             * "version": "1.0",
             * "provider_name": "Vimeo",
             * "provider_url": "https:\/\/vimeo.com\/",
             * "title": "The New Vimeo Player (You Know, For Videos)",
             * "author_name": "Vimeo Staff",
             * "author_url": "https:\/\/vimeo.com\/staff",
             * "is_plus": "0",
             * "html": "<iframe src=\"https:\/\/player.vimeo.com\/video\/76979871\" width=\"1280\" height=\"720\" frameborder=\"0\" title=\"The New Vimeo Player (You Know, For Videos)\" webkitallowfullscreen mozallowfullscreen allowfullscreen><\/iframe>",
             * "width": 1280,
             * "height": 720,
             * "duration": 62,
             * "description": "It may look (mostly) the same on the surface, but under the hood we totally rebuilt our player. Here\u2019s a quick rundown of some of the coolest new features:\n\n\u2022 Lightning fast playback\n\u2022 Redesigned Share screen\n\u2022 Closed caption and subtitle compatible\n\u2022 HTML5 by default\n\u2022 Purchase-from-player functionality for embedded Vimeo On Demand trailers\n\u2022 More responsive than ever (go ahead, resize it, we dare you!!!)\n\nWe\u2019re really proud of these updates. So proud that we made a spiffy new page to showcase all the reasons why we have the best video player in the galaxy. Check it out here: http:\/\/vimeo.com\/player\n\nIn short, this is a player that even haters can love.",
             * "thumbnail_url": "https:\/\/i.vimeocdn.com\/video\/452001751_1280.webp",
             * "thumbnail_width": 1280,
             * "thumbnail_height": 720,
             * "thumbnail_url_with_play_button": "https:\/\/i.vimeocdn.com\/filter\/overlay?src=https:\/\/i.vimeocdn.com\/video\/452001751_1280.webp&src=http:\/\/f.vimeocdn.com\/p\/images\/crawler_play.png",
             * "upload_date": "2013-10-15 14:08:29",
             * "video_id": 76979871,
             * "uri": "\/videos\/76979871"
             * }
             */
            // vimeo
            $data = json_decode(
                file_get_contents(
                    (new Url('https://vimeo.com/api/oembed.json'))
                        ->addParam('url', $this->url)
                )
            );
            if ($item->get('name') == '') {
                $fields['name'] = $data->title;
            }
            if ($item->get('content_video') == '') {
                $fields['content_video'] = \yii\helpers\Html::tag('iframe', null, [
                    'width'  => '100%',
                    'height' => '300',
                    'src'    => 'https://player.vimeo.com/video/' . substr($url->path, 1),
                ]);
            }
            if ($item->get('content') == '') {
                $fields['content'] = nl2br($data->description);
            }
            if ($item->get('image') == '') {
                $data = file_get_contents(
                    $data->thumbnail_url
                );
                $fields = ArrayHelper::merge($fields, FileUpload::save(File::content($data), 'jpg', self::findField('image'), $this));

            }
        }
        if (count($fields) > 0) {
            $item->update($fields);
        }

        return $item;
    }
}
