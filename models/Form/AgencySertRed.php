<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\User;
use app\services\GsssHtml;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 * ContactForm is the model behind the contact form.
 */
class AgencySertRed extends \cs\base\BaseForm
{
    const TABLE = 'gs_sert_red';

    public $id;
    public $identy;
    public $reason;
    public $datetime;
    public $content;
    public $user_id;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'identy',
                'Провинившийся',
                1,
                'string'
            ],
            [
                'reason',
                'Причина',
                1,
                'string'
            ],
            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }

    public function insert($fieldsCols = null)
    {
        return parent::insert([
            'beforeInsert' => function ($fields) {
                $fields['datetime'] = time();
                $fields['user_id'] = Yii::$app->user->id;

                return $fields;
            },
        ]);
    }
}
