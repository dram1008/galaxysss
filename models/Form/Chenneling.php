<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\User;
use app\services\GsssHtml;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class Chenneling extends \cs\base\BaseForm
{
    const TABLE = 'gs_cheneling_list';

    public $id;
    public $header;
    public $sort_index;
    public $content;
    public $date_insert;
    public $img;
    public $id_string;
    public $source;
    public $view_counter;
    public $description;
    public $date;
    public $user_id;
    public $is_added_site_update;
    /** @var  int маска которая содержит идентификаторы разделов к которому принадлежит ченелинг */
    public $tree_node_id_mask;
    /** @var  bool */
    public $is_add_image = true;
    public $moderation_status;
    public $date_created;
    public $author_id;
    public $power_id;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'header',
                'Название',
                1,
                'string'
            ],
            [
                'author_id',
                'Автор',
                0,
                'integer'
            ],
            [
                'power_id',
                'Высшие силы',
                0,
                'integer'
            ],
            [
                'source',
                'Ссылка',
                0,
                'url'
            ],

            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'is_add_image',
                'Добавлять картинку вначале статьи?',
                0,
                'cs\Widget\CheckBox2\Validator',
                'widget' => [
                    'cs\Widget\CheckBox2\CheckBox',
                ],
                'isFieldDb' => false,
            ],
            [
                'description',
                'Описание краткое',
                0,
                'string'
            ],
            [
                'img',
                'Картинка',
                0,
                'default',
                'widget' => [
                    FileUpload::className(),
                    [
                        'options' => [
                            'small' => \app\services\GsssHtml::$formatIcon
                        ]
                    ]
                ]
            ],
            [
                'tree_node_id_mask',
                'Категории',
                0,
                'cs\Widget\CheckBoxListMask\Validator',
                'widget' => [
                    'cs\Widget\CheckBoxTreeMask\CheckBoxTreeMask',
                    [
                        'tableName' => 'gs_cheneling_tree'
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }


    /**
     * @param null $fieldsCols
     *
     * @return \app\models\Chenneling
     */
    public function insert($fieldsCols = null)
    {
        $row = parent::insert([
            'beforeInsert' => function ($fields) {
                $fields['date_created'] = time();
                $fields['id_string'] = Str::rus2translit($fields['header']);
                $fields['date'] = gmdate('Y-m-d');
                $fields['moderation_status'] = 1;

                return $fields;
            },
        ]);
        if ($row === false) return false;

        $item = new \app\models\Chenneling($row);
        if ($this->is_add_image) {
            $fields = ['content' => Html::tag('p', Html::img(\cs\Widget\FileUpload3\FileUpload::getOriginal($item->getField('img')), [
                    'class' => 'thumbnail',
                    'style' => 'width:100%;',
                    'alt'   => $item->getField('header'),
                ])) . $item->getField('content')];
        }
        if ($row['description'] == '') {
            $fields['description'] = GsssHtml::getMiniText($row['content']);
        }
        $item->update($fields);

        return $item;
    }

    public function update($fieldsCols = null)
    {
        $fields = parent::update([
            'beforeUpdate' => function ($fields, \app\models\Form\Chenneling $model) {
                if ($fields['description'] == '') {
                    $fields['description'] = GsssHtml::getMiniText($fields['content']);
                }
                return $fields;
            }
        ]);
        if ($fields === false) return false;

        $item = \app\models\Chenneling::find($this->id);
        if ($this->is_add_image) {
            $content = Html::tag('p', Html::img(\cs\Widget\FileUpload2\FileUpload::getOriginal($item->getField('img')), [
                    'class' => 'thumbnail',
                    'style' => 'width:100%;',
                    'alt'   => $item->getField('header'),
                ])) . $item->getField('content');
            $item->update(['content' => $content]);
        }

        return $fields;
    }

}
