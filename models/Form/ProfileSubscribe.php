<?php

namespace app\models\Form;

use app\models\User;
use cs\base\BaseForm;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use cs\Widget\FileUpload2\FileUpload;

/**
 *
 */
class ProfileSubscribe extends BaseForm
{
    const TABLE = 'gs_users';

    public $id;
    public $name_first;
    public $name_last;
    public $email;
    public $password;
    public $is_admin;
    public $is_active;
    public $is_confirm;
    public $datetime_reg;
    public $datetime_activate;
    public $avatar;
    public $phone;
    public $gender;
    public $date_insert;
    public $date_update;
    public $vk_id;
    public $vk_link;
    public $fb_id;
    public $fb_link;
    public $birth_date;
    public $last_action;
    public $human_design;
    public $birth_time;
    public $birth_country;
    public $birth_town;
    public $birth_lat;
    public $birth_lng;
    public $birth_place;
    public $zvezdnoe;
    public $referal_code;
    public $mission;
    public $mode_chenneling;
    public $is_accept_nmp;
    public $mode_chenneling_is_subscribe;
    public $time_zone;
    public $is_show_shop_help_window;

    public $subscribe_is_news;
    public $subscribe_is_site_update;
    public $subscribe_is_manual;
    public $subscribe_is_test;
    public $subscribe_is_tesla;
    public $subscribe_is_rod;
    public $subscribe_is_bogdan;
    public $subscribe_is_new_earth;
    public $subscribe_is_god;

    function __construct($fields = [])
    {
        static::$fields = [
            ['subscribe_is_news', 'Новости Планеты', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
            ['subscribe_is_site_update', 'Обновления сайта', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
            ['subscribe_is_manual', 'Ручные рассылки', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
            ['subscribe_is_rod', 'Агентство Сохранения Рода', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
            ['subscribe_is_tesla', 'Проект TeslaGen', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
            ['subscribe_is_bogdan', 'Авиалини БогДан', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
            ['subscribe_is_new_earth', 'Проект Новая Земля', 0, 'default',
                'widget' => ['cs\Widget\CheckBox2\CheckBox']
            ],
        ];

        parent::__construct($fields);
    }
}
