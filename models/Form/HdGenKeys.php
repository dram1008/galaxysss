<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\User;
use app\services\GsssHtml;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class HdGenKeys extends \cs\base\BaseForm
{
    const TABLE = 'gs_hd_gen_keys';

    public $id;
    public $num;
    public $name;
    public $ten;
    public $dar;
    public $siddhi;
    public $image;
    public $codon;
    public $fiz;
    public $amin;
    public $patern;
    public $content;
    public $content_ten;
    public $content_dar;
    public $content_siddhi;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'num',
                'Номер',
                1,
                'integer'
            ],
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'dar',
                'Дар',
                0,
                'string'
            ],
            [
                'ten',
                'Тень',
                0,
                'string'
            ],
            [
                'siddhi',
                'Сиддхи',
                0,
                'string'
            ],
            [
                'codon',
                'Кодоновое кольцо',
                0,
                'string'
            ],
            [
                'fiz',
                'Физиология',
                0,
                'string'
            ],
            [
                'patern',
                'Программный партнёр',
                0,
                'string'
            ],
            [
                'amin',
                'Аминокислота',
                0,
                'string'
            ],
            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'content_ten', 'Описание тень', 0, 'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                ]
            ],
            [
                'content_dar', 'Описание дар', 0, 'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                ]
            ],
            [
                'content_siddhi', 'Описание сиддхи', 0, 'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                ]
            ],
            [
                'image',
                'Картинка',
                0,
                'string',
                'widget' => [
                    FileUpload::className(),
                    [
                        'options' => [
                            'small' => \app\services\GsssHtml::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }
}
