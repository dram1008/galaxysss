<?php

namespace app\models\Form;

use app\models\User;
use cs\base\BaseForm;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use cs\Widget\FileUpload3\FileUpload;

/**
 *
 */
class Profile extends BaseForm
{
    const TABLE = 'gs_users';

    public $id;
    public $name_first;
    public $name_last;
    public $email;
    public $password;
    public $is_admin;
    public $is_active;
    public $is_confirm;
    public $datetime_reg;
    public $datetime_activate;
    public $avatar;
    public $phone;
    public $gender;
    public $date_insert;
    public $date_update;
    public $vk_id;
    public $vk_link;
    public $fb_id;
    public $fb_link;
    public $birth_date;
    public $last_action;
    public $human_design;
    public $birth_time;
    public $birth_country;
    public $birth_town;
    public $birth_lat;
    public $birth_lng;
    public $birth_place;
    public $zvezdnoe;
    public $referal_code;
    public $mission;
    public $mode_chenneling;
    public $is_accept_nmp;
    public $mode_chenneling_is_subscribe;
    public $time_zone;
    public $is_show_shop_help_window;

    public $subscribe_is_news;
    public $subscribe_is_site_update;
    public $subscribe_is_mailing;
    public $subscribe_is_manual;
    public $subscribe_is_test;
    public $subscribe_is_tesla;
    public $subscribe_is_rod;
    public $subscribe_is_bogdan;
    public $subscribe_is_new_earth;
    public $subscribe_is_god;

    function __construct($fields = [])
    {
        static::$fields = [
            ['name_first', 'Имя', 1, 'string'],
            ['mission', 'Миссия', 0, 'string'],
            ['name_last', 'Фамилия', 0, 'string'],
            ['gender', 'Пол', 0, 'integer'],
            ['avatar', 'Картинка', 0, 'default', 'widget' => [FileUpload::className(), ['options' => [
                'small' => \app\services\GsssHtml::$formatIcon
            ]]]],
            ['birth_date', 'Дата рождения', 0, 'cs\Widget\DatePicker\Validator', 'widget' => [\cs\Widget\DatePicker\DatePicker::className(), ['dateFormat' => 'php:d.m.Y']]],
        ];
        parent::__construct($fields);
    }
}
