<?php

namespace app\models\Form\Shop;

use app\models\NewsItem;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\User;
use app\services\GsssHtml;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class ProductImage extends \cs\base\BaseForm
{
    const TABLE = 'gs_unions_shop_product_images';

    public $id;
    public $product_id;
    public $name;
    public $image;
    public $moderation_status;
    public $sort_index;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'name',
                'Название',
                0,
                'string'
            ],
            [
                'image',
                'Картинка',
                0,
                'default',
                'widget' => [
                    FileUpload::className(),
                    [
                        'options' => [
                            'small' => \app\services\GsssHtml::$formatIcon,
                            'quality' => 100,
                        ]
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }

    public function add($id)
    {
        $this->product_id = $id;

        $fields = parent::insert([
            'beforeInsert' => function ($fields, \app\models\Form\Shop\ProductImage $model) {
                $fields['product_id'] = $model->product_id;

                return $fields;
            },
        ]);
        $image = Shop\ProductImage::find($fields['id']);
        $product = Product::find($this->product_id);
        if ($product->getField('moderation_status', 0) == 1) {
            // высылаю письмо
            $shop = Shop::find(['union_id' => $product->getField('union_id')]);
            if ($shop->getModerationMode() == 1) {
                // нужна модерация
                foreach(User::getQueryByRole(User::USER_ROLE_MODERATOR)
                            ->select(['email'])
                            ->column() as $moderatorMail) {
                    Application::mail($moderatorMail, 'Добавлена картинка', 'shop/moderation_product_image_add', [
                        'shop'    => $shop,
                        'product' => $product,
                        'image'   => $image,
                    ]);
                }
            } else {
                // не нужна модерация
                $image->update(['moderation_status' => 1]);
            }
        } else {
            // ничего не делаю
        }

        return $fields;
    }

    /**
     * @param null $fields
     * @return array|bool
     */
    public function update($fields = null)
    {
        $fields = parent::update();
        $product = Product::find($this->product_id);
        $image = Shop\ProductImage::find($this->id);
        if ($product->getField('moderation_status', 0) == 1) {
            // высылаю письмо
            $shop = Shop::find(['union_id' => $product->getField('union_id')]);
            if ($shop->getModerationMode() == 1) {
                // нужна модерация
                foreach(User::getQueryByRole(User::USER_ROLE_MODERATOR)
                            ->select(['email'])
                            ->column() as $moderatorMail) {
                    Application::mail($moderatorMail, 'Обновлена картинка', 'shop/moderation_product_image_edit', [
                        'shop'    => $shop,
                        'product' => $product,
                        'image'   => $image,
                    ]);
                }
                $image->update(['moderation_status' => null]);
            } else {
                // не нужна модерация
                $image->update(['moderation_status' => 1]);
            }
        } else {
            // ничего не делаю
        }

        return $fields;
    }
}
