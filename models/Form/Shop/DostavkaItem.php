<?php

namespace app\models\Form\Shop;

use app\models\NewsItem;
use app\models\User;
use app\services\GsssHtml;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 */
class DostavkaItem extends \cs\base\BaseForm
{
    const TABLE = 'gs_unions_shop_dostavka';

    public $id;
    public $name;
    public $description;
    public $content;
    public $union_id;
    public $price;
    public $is_need_address;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'description',
                'Описание краткое',
                0,
                'string'
            ],
            [
                'price',
                'Стоимость',
                0,
                'integer'
            ],
            [
                'is_need_address',
                'Нужен адрес?',
                0,
                'cs\Widget\CheckBox2\Validator',
                'widget' => ['cs\Widget\CheckBox2\CheckBox', []]
            ],

        ];
        parent::__construct($fields);
    }

    public function insert2($id)
    {
        $this->union_id = $id;

        return parent::insert([
            'beforeInsert' => function ($fields, \app\models\Form\Shop\DostavkaItem $model) {
                $fields['union_id'] = $model->union_id;

                return $fields;
            },
        ]);
    }

    public function update($fieldsCols = null)
    {
        return parent::update([
            'beforeUpdate' => function ($fields) {
                if ($fields['description'] == '') {
                    $fields['description'] = GsssHtml::getMiniText($fields['content']);
                }

                return $fields;
            }
        ]);
    }

}
