<?php

namespace app\models\Form\Shop;

use app\models\NewsItem;
use app\models\Shop;
use app\models\User;
use app\services\GsssHtml;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 * ContactForm is the model behind the contact form.
 */
class Product extends \cs\base\BaseForm
{
    const TABLE = 'gs_unions_shop_product';

    public $id;
    public $tree_node_id;
    public $name;
    public $description;
    public $date_insert;
    public $sort_index;
    public $content;
    public $image;
    public $union_id;
    public $price;
    public $moderation_status;
    public $shop_node_id;
    public $is_electron;
    public $electron_text;
    public $attached_files;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'electron_text',
                'Текст электронного товара',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'description',
                'Описание краткое',
                0,
                'string'
            ],
            [
                'price',
                'Цена',
                0,
                'integer', [], 'В рублях'
            ],
            [
                'image',
                'Картинка',
                0,
                'default',
                'widget' => [
                    FileUpload::className(),
                    [
                        'options' => [
                            'small' => \app\services\GsssHtml::$formatIcon,
                            'quality' => 100,
                        ]
                    ]
                ]
            ],
            [
                'is_electron',
                'Электронный товар?',
                0,
                'cs\Widget\CheckBox2\Validator',
                'widget' => [
                    'cs\Widget\CheckBox2\CheckBox',
                ]
            ],
            [
                'tree_node_id',
                'Категория частная',
                0,
                'default',
                'widget' => [
                    'app\modules\Shop\services\CheckBoxTreeMask\CheckBoxTreeMask',
                    [
                        'rows'      => (new Query())
                            ->select([
                                'id',
                                'name'
                            ])
                            ->where([
                                'union_id' => $fields['union_id'],
                            ])
                            ->from('gs_unions_shop_tree')
                            ->all()
                        ,
                        'tableName' => 'gs_unions_shop_tree',
                        'union_id'  => $fields['union_id'],
                    ]
                ]
            ],
            [
                'shop_node_id',
                'Категория общая',
                0,
                'default',
                'widget' => [
                    'cs\Widget\CheckBoxTreeTable\CheckBoxTreeTable',
                    [
                        'tableName'    => 'gs_shop_tree',
                        'tableLink'    => 'gs_shop_tree_products_link',
                        'fieldTree'    => 'tree_node_id',
                        'fieldProduct' => 'product_id',
                    ]
                ]
            ],
            [
                'attached_files',
                'Прикрепляемые файлы',
                0,
                'default',
                'widget' => [
                    'cs\Widget\FileUploadMany\FileUploadMany',
                ]
            ],
        ];
        parent::__construct($fields);
    }

    public function insert2($id)
    {
        $this->union_id = $id;

        return parent::insert([
            'beforeInsert' => function ($fields, \app\models\Form\Shop\Product $model) {
                $fields['date_insert'] = time();
                $fields['union_id'] = $model->union_id;
                $shop = Shop::find(['union_id' => $model->union_id]);
                if ($shop->getModerationMode() == 1) {
                    // нужна модерация
                } else {
                    // не нужна модерация
                    $fields['moderation_status'] = 1;
                }

                return $fields;
            },
        ]);
    }

    public function update($fieldsCols = null)
    {
        return parent::update([
            'beforeUpdate' => function ($fields, \app\models\Form\Shop\Product $model) {
                if ($fields['description'] == '') {
                    $fields['description'] = GsssHtml::getMiniText($fields['content']);
                }
                $shop = Shop::find(['union_id' => $model->union_id]);
                if ($shop->getModerationMode() == 1) {
                    // нужна модерация
                    $fields['moderation_status'] = null;
                } else {
                    // не нужна модерация
                }

                return $fields;
            }
        ]);
    }

}
