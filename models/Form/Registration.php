<?php

namespace app\models\Form;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Registration extends \cs\base\BaseForm
{
    public $email;
    public $password1;
    public $password2;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email', 'password1', 'password2'], 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            // email has to be a valid email address
            ['email', 'email', 'message' => 'Email должен быть верным'],
            ['email', 'validateEmail'],
            ['password1', 'validatePassword'],
            ['password2', 'validatePassword'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'message' => 'Неверный код'],
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => ['email', 'password1', 'password2', 'verifyCode'],
            'ajax'   => ['email', 'password1', 'password2'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Проверочный код',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($this->password1 != '' && $this->password2 != '') {
            if ($this->password1 != $this->password2) {
                $this->addError($attribute, 'Пароли должны совпадать');
            }
        }
    }

    public function validateEmail($attribute, $params)
    {
        if (User::query(['email' => $this->email])->exists()) {
            $this->addError($attribute, 'Такой пользователь уже есть');
        }
    }

    /**
     * @param  \app\models\User $u пользователь который его пригласил
     *
     * @return boolean whether the model passes validation
     */
    public function register($u)
    {
        if ($this->validate()) {
            $user = \app\models\User::registration($this->email, $this->password1);
            $wallet = $user->getWallet();
            $wallet->parent_id = $u->getId();
            $wallet->save();


            $id = $wallet->id;
            $wallet1 = User::find($id)->getWallet();
            $wallet1->in(10000, new \app\models\Piramida\WalletSource\God(), 'Приход с яндекс денег');
            $wallet2 = Wallet::findOne($wallet1->parent_id);
            $comment = 'Перевод по регистрации $id=' . $id;
            $wallet1->move($wallet2, 2000, $comment);
            if ($wallet2->parent_id) {
                $wallet3 = Wallet::findOne($wallet2->parent_id);
                $wallet1->move($wallet3, 2000, $comment);
                if ($wallet3->parent_id) {
                    $wallet4 = Wallet::findOne($wallet3->parent_id);
                    $wallet1->move($wallet4, 2000, $comment);
                }
            }
            $wallet1->move(Piramida::getGlobalWallet(), 4000, $comment);

            return true;
        } else {
            return false;
        }
    }
}
