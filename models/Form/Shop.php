<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\Union;
use app\models\User;
use app\services\GsssHtml;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 * ContactForm is the model behind the contact form.
 */
class Shop extends \cs\base\BaseForm
{
    const TABLE = 'gs_unions_shop';

    public $id;
    public $union_id;
    public $name;
    public $dostavka;
    public $admin_email;
    public $yandex_id;
    public $rekvizit;
    public $mail_image;
    public $signature;
    public $moderation_mode;
    public $contact;
    public $secret;

    function __construct($fields = [])
    {
        static::$fields = [
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'moderation_mode',
                'Свободное размещение товаров без модерации?',
                0,
                'cs\Widget\CheckBox2\Validator',
                'widget' => [
                    'cs\Widget\CheckBox2\CheckBox',
                ],
            ],
            [
                'signature',
                'Подпись в письме',
                0,
                'string'
            ],
            [
                'contact',
                'Контактная информация',
                0,
                'string', [], 'Только текст'
            ],
            [
                'rekvizit',
                'Реквизиты',
                0,
                'string', [], 'Номера счетов для выставления счета'
            ],
            [
                'secret',
                'Секретный код',
                0,
                'string', [], 'Этот код нужен для приема уведомления об оплате клиентом. https://www.galaxysss.com/shop/order/success'
            ],
            [
                'yandex_id',
                'Идентификатор кошелька Yandex',
                0,
                'integer',
                [],
                'Для автоматизации сбора средств'
            ],
            [
                'dostavka',
                'Доставка',
                0,
                'string',
                'widget' => [
                    'cs\Widget\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'mail_image',
                'Шапка письма',
                0,
                'default',[], 'Это изображение будет использоваться как шапка в письмах с клиентами',
                'widget' => [FileUpload::className(), ['options' => [
                    'small' => [
                        600,
                        200,
                        \cs\Widget\FileUpload2\FileUpload::MODE_THUMBNAIL_CUT
                    ]
                ]]]
            ],
            [
                'admin_email',
                'email для заказов',
                0,
                'email'
            ],
        ];
        parent::__construct($fields);
    }

    /**
     * @param int $id идентификатор объединения gs_unions.id
     * @return array|bool
     */
    public function update2($id)
    {
        if ($this->id) {
            return parent::update();
        } else {
            $this->union_id = $id;
            return parent::insert([
                'beforeInsert' => function ($fields, \app\models\Form\Shop $model) {
                    $fields['union_id'] = $model->union_id;
                    (new Query())->createCommand()->insert('gs_unions_shop_tree', [
                        'union_id' => $model->union_id,
                        'name'     => 'Корневой раздел'
                    ])->execute();
                    (new Query())->createCommand()->update(Union::TABLE, [
                        'is_shop' => 1,
                    ], ['id' => $model->union_id])->execute();

                    return $fields;
                },
            ]);
        }
    }
}
