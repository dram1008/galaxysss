<?php

namespace app\models\Form;

use app\models\User;
use app\services\GsssHtml;
use cs\Application;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;

/**
 */
class VozdayanieItem extends \cs\base\BaseForm
{
    const TABLE = 'gs_vozdayanie';

    public $id;
    public $image;
    public $sort_index;

    function __construct($fields = [])
    {
        static::$fields = [

            [
                'image',
                'Картинка',
                0,
                'string',
                'widget' => [
                    FileUpload::className(),
                    [
                        'options' => [
                            'small' => \app\services\GsssHtml::$formatIcon,
                            'extended' => [
                                'share' => [
                                    470*2,
                                    245*2,
                                    FileUpload::MODE_THUMBNAIL_CUT
                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ];
        parent::__construct($fields);
    }

}
