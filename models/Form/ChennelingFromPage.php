<?php

namespace app\models\Form;

use app\models\Article;
use app\models\Chenneling;
use app\models\Union;
use app\models\User;
use app\services\GetArticle\Collection;
use cs\base\BaseForm;
use cs\services\BitMask;
use cs\services\File;
use cs\services\Str;
use cs\services\Url;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\db\Query;

/**
 */
class ChennelingFromPage extends BaseForm
{
    const TABLE = 'gs_cheneling_list';

    public $id;
    public $url;
    public $provider;
    public $tree_node_id_mask;
    public $author_id;
    public $power_id;

    public function rules()
    {
        return [
            [
                [
                    'url',
                ],
                'required',
                'message' => 'Поле должно быть заполнено обязательно'
            ],
            [
                ['author_id', 'power_id'],
                'integer'
            ],
            [
                [
                    'url',
                ],
                'url',
            ],
            [
                [
                    'provider',
                ],
                'string',
            ],
            [
                [
                    'tree_node_id_mask',
                ],
                'cs\Widget\CheckBoxListMask\Validator',
            ],
        ];
    }

    public function insert($fieldsCols = NULL)
    {
        $extractorConfig = Collection::findUrl($this->url);
        if (is_null($extractorConfig)) {
            throw new Exception('Не верный extractor');
        }
        $extractorClass = $extractorConfig['class'];
        /** @var \app\services\GetArticle\ExtractorInterface $extractor */
        $extractor = new $extractorClass($this->url);
        $articleObject = Chenneling::insertExtractorInterface($extractor);
        $fields['tree_node_id_mask'] = (new BitMask($this->tree_node_id_mask))->getMask();
        if ($this->author_id) $fields['tree_node_id_mask'] = $this->author_id;
        if ($this->power_id) $fields['power_id'] = $this->power_id;
        $articleObject->update($fields);

        return true;
    }
}
