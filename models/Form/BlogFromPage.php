<?php

namespace app\models\Form;

use app\models\Article;
use app\models\Blog;
use app\models\Chenneling;
use app\models\Union;
use app\models\User;
use app\services\GetArticle\Collection;
use cs\base\BaseForm;
use cs\services\BitMask;
use cs\services\File;
use cs\services\Str;
use cs\services\Url;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload2\FileUpload;
use yii\db\Query;

/**
 */
class BlogFromPage extends BaseForm
{
    const TABLE = 'gs_blog';

    public $id;
    public $url;
    public $provider;
    public $tree_node_id_mask;

    public function rules()
    {
        return [
            [
                [
                    'url',
                ],
                'required',
                'message' => 'Поле должно быть заполнено обязательно'
            ],
            [
                [
                    'url',
                ],
                'url',
            ],
            [
                [
                    'provider',
                ],
                'string',
            ],
        ];
    }

    public function insert($fieldsCols = NULL)
    {
        $extractorConfig = Collection::findUrl($this->url);
        if (is_null($extractorConfig)) {
            throw new Exception('Не верный extractor');
        }
        $extractorClass = $extractorConfig['class'];
        /** @var \app\services\GetArticle\ExtractorInterface $extractor */
        $extractor = new $extractorClass($this->url);
        $articleObject = Blog::insertExtractorInterface($extractor);

        return $articleObject;
    }
}
