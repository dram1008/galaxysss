<?php

namespace app\models\Form;

use app\models\NewsItem;
use app\models\User;
use app\services\GsssHtml;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;

/**
 * ContactForm is the model behind the contact form.
 */
class WalletInvite extends Model
{
    public $id;
    public $mails;


    public function rules()
    {
        return [
            [['mails'], 'required'],
            [['mails'], 'string', 'min' => 1],
            [['mails'], 'validateMails'],
        ];
    }

    public function validateMails($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $text = str_replace("\n", ' ', $this->mails);
            $text = str_replace("\r", ' ', $text);
            $text = str_replace(",", ' ', $text);
            $text = str_replace(";", ' ', $text);
            $array = explode(' ', $text);
            $emailList = [];
            foreach($array as $item) {
                $value = trim($item);
                if ($value != '') {
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        $this->addError($attribute, 'Не верный формат email: '. $value);
                        return;
                    } else {
                        $emailList[] = $value;
                    }
                }
            }
            $this->mails = $emailList;
        }
    }

    public function send()
    {
        if (!$this->validate()) return false;

        foreach($this->mails as $mail) {
            Application::mail($mail, 'Приглашение на Новую Землю', 'invite', [
                'user' => Yii::$app->user->identity,
            ]);
        }

        return true;
    }
}
