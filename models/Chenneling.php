<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 05.05.2015
 * Time: 23:41
 */

namespace app\models;


use app\services\Subscribe;
use cs\services\BitMask;
use cs\services\File;
use cs\services\Str;
use cs\services\Url;
use cs\services\VarDumper;
use cs\Widget\FileUpload3\FileUpload;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Chenneling extends \cs\base\DbRecord implements SiteContentInterface
{
    const TABLE = 'gs_cheneling_list';

    /**
     * Ключ для memcache для сохранения страницы всех ченелингов
     */
    const MEMCACHE_KEY_LIST = '\app\controllers\PageController::actionChenneling';

    public function incViewCounter()
    {
        $this->update(['view_counter' => $this->getField('view_counter') + 1]);
    }

    public static function clearCache()
    {
        \Yii::$app->cache->delete(self::MEMCACHE_KEY_LIST);
    }

    /**
     * @return \app\models\User
     */
    public function getUser()
    {
        $uid = $this->get('user_id');
        if (is_null($uid)) return null;

        return User::find($uid);
    }

    public function getName()
    {
        return $this->getField('header');
    }

    public function getImage($isScheme = false)
    {
        if ($isScheme) {
            return \yii\helpers\Url::to($this->getField('img'), true);
        }

        return $this->getField('img');
    }

    public function accept()
    {
        $this->update(['moderation_status' => 1]);
    }

    public function reject()
    {
        $this->update(['moderation_status' => 0]);
    }

    /**
     * @inheritdoc
     */
    public function getMailContent()
    {
        // шаблон
        $view = 'subscribe/channeling';
        // опции шаблона
        $options = [
            'item' => $this,
            'user' => \Yii::$app->user->identity,
        ];

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $text = $mailer->render('text/' . $view, $options, 'layouts/text/subscribe');
        $html = $mailer->render('html/' . $view, $options, 'layouts/html/subscribe');

        $subscribeItem = new SubscribeItem();
        $subscribeItem->subject = $this->getName();
        $subscribeItem->html = $html;
        $subscribeItem->text = $text;
        $subscribeItem->type = Subscribe::TYPE_SITE_UPDATE;

        return $subscribeItem;
    }

    /**
     * @inheritdoc
     */
    public function getSiteUpdateItem($isScheme = false)
    {
        $siteUpdateItem = new SiteUpdateItem();
        $siteUpdateItem->name = $this->getName();
        $siteUpdateItem->image = $this->getImage($isScheme);
        $siteUpdateItem->link = $this->getLink($isScheme);
        $siteUpdateItem->type = SiteUpdateItem::TYPE_CHANNELING;

        return $siteUpdateItem;
    }

    /**
     * Возвращает ссылку на ченелинг
     *
     * @param bool $isScheme надо ли добавлять полный путь
     *
     * @return string
     */
    public function getLink($isScheme = false)
    {
        $date = $this->getField('date');
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);

        return \yii\helpers\Url::to([
            'page/chenneling_item',
            'year'  => $year,
            'month' => $month,
            'day'   => $day,
            'id'    => $this->getField('id_string'),
        ], $isScheme);
    }

    /**
     * @return \yii\db\Query
     */
    public static function queryList()
    {
        return self::query()->select([
                'gs_cheneling_list.id',
                'gs_cheneling_list.header',
                'gs_cheneling_list.date_insert',
                'gs_cheneling_list.id_string',
                'gs_cheneling_list.img',
                'gs_cheneling_list.view_counter',
                'gs_cheneling_list.description',
                'gs_cheneling_list.date',
                'gs_cheneling_list.user_id',
                'gs_cheneling_list_author_id.name as author_name',
                'gs_cheneling_list_power_id.name as power_name',
            ])
            ->leftJoin('gs_cheneling_list_author_id', 'gs_cheneling_list_author_id.id = gs_cheneling_list.author_id')
            ->leftJoin('gs_cheneling_list_power_id', 'gs_cheneling_list_power_id.id = gs_cheneling_list.power_id')
            ;
    }

    /**
     * Добавить послание из GetArticle
     *
     * @param \app\services\GetArticle\ExtractorInterface $extractor
     *
     * @return static
     * @throws \yii\base\Exception
     */
    public static function insertExtractorInterface($extractor)
    {
        $row = $extractor->extract();
        if (is_null($row['header'])) {
            throw new Exception('Нет заголовка');
        }
        if ($row['header'] == '') {
            throw new Exception('Нет заголовка');
        }
        if (is_null($row['description'])) {
            throw new Exception('Нет описания');
        }
        if ($row['description'] == '') {
            throw new Exception('Нет описания');
        }
        $fields = [
            'header'            => $row['header'],
            'content'           => $row['content'],
            'description'       => $row['description'],
            'source'            => $extractor->getUrl(),
            'id_string'         => Str::rus2translit($row['header']),
            'date_created'      => time(),
            'date'              => gmdate('Ymd'),
            'img'               => '',
            'moderation_status' => 1,
        ];
        $articleObject = self::insert($fields);
        $model = new \app\models\Form\Chenneling();
        $model->id = $articleObject->getId();
        $image = $row['image'];
        if ($image) {
            try {
                $imageContent = file_get_contents($image);
                $url = new Url($image);
                $channelingForm = new \app\models\Form\Chenneling();
                $fields2 = FileUpload::save(File::content($imageContent), $url->getExtension('jpg'), $channelingForm->findField('img'), $model);
                $fields = ArrayHelper::merge($fields, $fields2);
                if ($fields) {
                    $fields['content'] = Html::tag('p', Html::img(\cs\Widget\FileUpload3\FileUpload::getOriginal($fields['img']), [
                        'class' => 'thumbnail',
                        'style' => 'width:100%;',
                        'alt'   => $fields['header'],
                    ])) . $fields['content'];
                }
                $articleObject->update($fields);
            } catch (\Exception $e) {
                VarDumper::dump($e);
            }
        }

        return $articleObject;
    }
}