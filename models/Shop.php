<?php

namespace app\models;

use app\services\Subscribe;
use cs\Application;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class Shop extends \cs\base\DbRecord
{
    const TABLE = 'gs_unions_shop';

    public function getName()
    {
        return $this->getField('name', '');
    }

    public function getAdminEmail()
    {
        return $this->getField('admin_email', '');
    }

    /**
     * @return int 0|1
     */
    public function getModerationMode()
    {
        $v = $this->getField('moderation_mode', 1);

        return (is_null($v))? 1 : $v;
    }


    /**
     * @return \app\models\Union
     */
    public function getUnion()
    {
        $unionId = $this->getField('union_id');

        return Union::find($unionId);
    }

    public function getLink($isScheme = false)
    {
        $union = $this->getUnion();

        return Url::to(['union_shop/index', 'id' => $union->getId(), 'category' => $union->getCategory()->getField('id_string')]);
    }

    /**
     * Возвращает список доставки
     *
     * @return array
     */
    public function getDostavkaRows()
    {
        return \app\models\Shop\DostavkaItem::query(['union_id' => $this->getField('union_id')])->all();
    }

    /**
     * Выдает подпись для магазина
     */
    public function getMailSignature()
    {
        $v = $this->getField('signature', '');
        return ($v == '')? 'С уважением.' : $v;
    }

    /**
     *
     */
    public function getMailImage()
    {
        return \yii\helpers\Url::to($this->getField('mail_image', '/images/page/mission/ab.jpg'), true);
    }

    /**
     * Отправляет письмо в формате html
     *
     * @param string       $email   куда
     * @param string       $subject тема
     * @param string       $view    шаблон, лежит в /mail/html и /mail/text
     * @param array        $options параметры для шаблона
     *
     * @return boolean
     */
    public static function mail($email, $subject, $view, $options = [])
    {
        return Application::mail($email, $subject, $view, $options, ['html' => 'layouts/html/shop']);
    }
}