<?php

namespace app\models;

use app\models\Form\Shop\ProductImage;
use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class Video extends \cs\base\DbRecord
{
    const TABLE = 'gs_video';

    /**
     * Возвращает картинку для видео
     * @param bool $isScheme
     * @return string
     */
    public function getImage($isScheme = false)
    {
        $url = $this->getField('image', '');
        if ($url == '') return '';

        return Url::to($url, $isScheme);
    }

    public function getLink($isSchema = false)
    {
        return Url::to(['video/item', 'id' => $this->getId()], $isSchema);
    }
}