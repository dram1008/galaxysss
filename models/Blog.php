<?php

namespace app\models;

use app\services\Subscribe;
use cs\services\BitMask;
use cs\services\File;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Url;

class Blog extends \cs\base\DbRecord implements SiteContentInterface
{
    const TABLE = 'gs_blog';

    public function incViewCounter()
    {
        $this->update(['view_counter' => $this->getField('view_counter') + 1]);
    }

    /**
     * Выдает элементы которые соответствуют определенной категории
     *
     * @param int $id идентификатор категории
     *
     * @return array
     */
    public static function getByTreeNodeId($id)
    {
        return self::query(['&', 'tree_node_id_mask', (new BitMask([$id]))->getMask()])
            ->orderBy(['date_insert' => SORT_DESC])
            ->all();
    }

    /**
     * Выдает элементы которые соответствуют определенной категории
     *
     * @param int $id идентификатор категории
     *
     * @return Query
     */
    public static function getByTreeNodeIdQuery($id)
    {
        return self::query(['&', 'tree_node_id_mask', (new BitMask([$id]))->getMask()])
            ->orderBy(['date_insert' => SORT_DESC])
            ;
    }

    public function getName()
    {
        return $this->getField('header');
    }

    public function getImage($isScheme = false)
    {
        $image = $this->getField('image', '');
        if ($image == '') return '';

        return \yii\helpers\Url::to($this->getField('image'), $isScheme);
    }

    /**
     * @inheritdoc
     */
    public function getMailContent()
    {
        // шаблон
        $view = 'subscribe/blog';
        // опции шаблона
        $options = [
            'item' => $this,
            'user' => \Yii::$app->user->identity,
        ];

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $text = $mailer->render('text/' . $view, $options, 'layouts/text/subscribe');
        $html = $mailer->render('html/' . $view, $options, 'layouts/html/subscribe');

        $subscribeItem = new SubscribeItem();
        $subscribeItem->subject = $this->getName();
        $subscribeItem->html = $html;
        $subscribeItem->text = $text;
        $subscribeItem->type = Subscribe::TYPE_SITE_UPDATE;

        return $subscribeItem;
    }

    /**
     * @inheritdoc
     */
    public function getSiteUpdateItem($isScheme = false)
    {
        $siteUpdateItem = new SiteUpdateItem();
        $siteUpdateItem->name = $this->getName();
        $siteUpdateItem->image = $this->getImage($isScheme);
        $siteUpdateItem->link = $this->getLink($isScheme);
        $siteUpdateItem->type = SiteUpdateItem::TYPE_BLOG;

        return $siteUpdateItem;
    }

    /**
     * Возвращает ссылку на статью
     *
     * @param bool $isScheme надо ли добавлять полный путь
     *
     * @return string
     */
    public function getLink($isScheme = false)
    {
        $date = $this->getField('date');
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);
        $id = $this->getField('id_string');
        $url = "/blog/{$year}/{$month}/{$day}/{$id}";

        return \yii\helpers\Url::to($url, $isScheme);
    }

    /**
     * Добавить послание из GetArticle
     *
     * @param \app\services\GetArticle\ExtractorInterface $extractor
     *
     * @return static
     * @throws \yii\base\Exception
     */
    public static function insertExtractorInterface($extractor)
    {
        $row = $extractor->extract();
        if (is_null($row['header'])) {
            throw new Exception('Нет заголовка');
        }
        if ($row['header'] == '') {
            throw new Exception('Нет заголовка');
        }
        if (is_null($row['description'])) {
            throw new Exception('Нет описания');
        }
        if ($row['description'] == '') {
            throw new Exception('Нет описания');
        }
        $fields = [
            'header'            => $row['header'],
            'content'           => $row['content'],
            'description'       => $row['description'],
            'source'            => $extractor->getUrl(),
            'id_string'         => Str::rus2translit($row['header']),
            'date_insert'       => time(),
            'date'              => gmdate('Ymd'),
            'image'             => '',
        ];
        $articleObject = self::insert($fields);
        $model = new \app\models\Form\Blog();
        $model->id = $articleObject->getId();
        $image = $row['image'];
        if ($image) {
            try {
                $imageContent = file_get_contents($image);
                $url = new \cs\services\Url($image);
                $blogForm = new \app\models\Form\Blog();
                $fields = FileUpload::save(File::content($imageContent), $url->getExtension('jpg'), $blogForm->findField('image'), $model);
                $articleObject->update($fields);
            } catch (\Exception $e) {

            }
        }

        return $articleObject;
    }

}