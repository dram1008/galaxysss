<?php

namespace app\models;

use app\models\Form\Shop\ProductImage;
use app\models\Union;
use app\services\Subscribe;
use cs\services\BitMask;
use yii\db\Query;
use yii\helpers\Url;

class VideoTreeNode extends \cs\base\DbRecord
{
    const TABLE = 'gs_video_tree';


    public function getLink($isSchema = false)
    {
        return Url::to(['video/category', 'id' => $this->getId()], $isSchema);
    }

}