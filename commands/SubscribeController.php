<?php

namespace app\commands;

use app\models\SubscribeMailItem;
use yii\console\Controller;
use yii\console\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Занимается рассылкой писем
 */
class SubscribeController extends Controller
{
    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionSend()
    {
        try{
            $time = microtime(true);
            $list = SubscribeMailItem::query()
                ->limit(10)
                ->orderBy(['date_insert' => SORT_ASC])
                ->all();
            $idsSuccess = [];
            if (count($list) > 0) {

                foreach($list as $mailItem) {
                    $mail = \Yii::$app->mailer
                        ->compose()
                        ->setFrom(\Yii::$app->params['mailer']['from'])
                        ->setTo($mailItem['mail'])
                        ->setSubject($mailItem['subject'])
                        ->setHtmlBody($mailItem['html']);
                    if (isset($mailItem['text'])) {
                        if ($mailItem['text'] != '') {
                            $mail->setTextBody($mailItem['text']);
                        }
                    }
                    $result = $mail->send();
                    if ($result == false) {
                        \Yii::info('Не удалось доствить: ' . VarDumper::dumpAsString($mailItem), 'gs\\app\\commands\\SubscribeController::actionSend');
                    } else {
                        $idsSuccess[] = $mailItem['id'];
                    }
                }
                $result = SubscribeMailItem::deleteByCondition([
                    'in', 'id', $idsSuccess
                ]);
                $time = microtime(true) - $time;
            }

            \Yii::$app->end();
        } catch (\Exception $e) {
            \Yii::info($e->getMessage(), 'gs\\send');
        }
    }
}
