<?php

namespace app\commands;

use app\models\SiteUpdate;
use app\models\SubscribeMailItem;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use yii\console\Controller;
use yii\console\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * Формирует письмо для рассылки: отчет за неделю
 */
class Week_mailController extends Controller
{
    /**
     */
    public function actionIndex()
    {
        $timeStart = time() - 60 * 60 * 24 * 7;
        $new = SiteUpdate::query(['>', 'date_insert', $timeStart])->orderBy(['date_insert' => SORT_DESC])->all();
        $items = [];
        foreach (\app\models\SiteUpdateItem::$names as $index => $name) {
            $r = [];
            foreach ($new as $row) {
                if ($row['type'] == $index) {
                    $r[] = $row;
                }
            }
            if (count($r) > 0) {
                $items[$index] = [
                    'index' => $index,
                    'name'  => $name,
                    'rows'  => $r,
                ];
            }
        }

        // опции шаблона
        $options = [
            'items' => $items,
        ];
        $subscribeItem = new \app\models\SubscribeItem([
            'type'    => \app\services\Subscribe::TYPE_SITE_UPDATE,
            'subject' => 'Обновления за неделю',
            'html' => [
                function($options) {
                    // шаблон
                    $view = 'subscribe/all';
                    /** @var \yii\swiftmailer\Mailer $mailer */
                    $mailer = \Yii::$app->mailer;

                    return $mailer->render('html/' . $view, $options, 'layouts/html/subscribe');
                },
                $options
            ],
        ]);

        Subscribe::add2($subscribeItem);
    }
}
