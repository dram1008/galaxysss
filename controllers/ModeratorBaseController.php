<?php

namespace app\controllers;

use app\models\Article;
use app\models\Shop\Product;
use app\models\SiteUpdate;
use app\models\Union;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\base\BaseController;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;

class ModeratorBaseController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            /** @var \app\models\User $user */
                            $user = \Yii::$app->user->identity;
                            return $user->hasRole(User::USER_ROLE_MODERATOR);
                        }
                    ],
                ],
            ],
        ];
    }

}
