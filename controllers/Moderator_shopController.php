<?php

namespace app\controllers;

use app\models\Article;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\ProductImage;
use app\models\SiteUpdate;
use app\models\Union;
use app\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class Moderator_shopController extends AdminBaseController
{

    public function actionProduct_list()
    {
        return $this->render([
        ]);
    }

    /**
     * просмотр товара
     * @param int $id идентификатор продукта
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionView($id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            throw new Exception('Товар не найден');
        }

        return $this->render([
            'product' => $product,
        ]);
    }

    /**
     * одобряет товар
     * @param int $id идентификатор продукта
     * @return string
     */
    public function actionAccept($id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            return self::jsonErrorId(101, 'Товар не найден');
        }
        $product->accept();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * отклоняет товар
     *
     * REQUEST:
     * - text - string - причина отклонения
     *
     * @param int $id идентификатор продукта
     * @return string
     */
    public function actionReject($id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            return self::jsonErrorId(101, 'Товар не найден');
        }
        $shop = $product->getUnion()->getShop();
        // отправить пиьсмо владельцу магазина
        Shop::mail($shop->getAdminEmail(), 'Ваш товар не прошел модерацию', 'shop/moderation_product_reject', [
            'shop'    => $shop,
            'product' => $product,
            'text'    => self::getParam('text'),
        ]);
        $product->reject();

        return self::jsonSuccess();
    }

    public function actionImage_list()
    {
        return $this->render([]);
    }

    /**
     * одобряет изображение
     * @param int $id идентификатор изображения
     * @return string
     */
    public function actionImage_list_accept($id)
    {
        $product = ProductImage::find($id);
        if (is_null($product)) {
            return self::jsonErrorId(101, 'Изображение не найдено');
        }
        $product->accept();

        return self::jsonSuccess();
    }

}
