<?php

namespace app\controllers;

use app\models\Form\Union;
use app\models\HD;
use app\models\HdGenKeys;
use app\models\HDtown;
use app\models\Piramida\InRequest;
use app\models\SiteUpdate;
use app\models\User;
use app\services\Subscribe;
use cs\Application;
use cs\services\SitePath;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class Cabinet_walletController extends BaseController
{
    public $layout = 'menu';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            if ($this->action != 'logout') {
                throw new Exception(Yii::$app->params['isTransfere_string']);
            }
        }
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        return $this->render();
    }

    public function actionChilds()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Выдает список друзей у которых указана полная дата рождения
     *
     * @return string json
     *                [ 'friends' => [
     *                  [
     *                      'id' => int
     *                      'id' => int
     *                      'id' => int
     *                      'id' => int
     * ], ...
     * ]]
     *
     */
    public function actionFacebookFriends()
    {
        /** @var \app\services\authclient\VKontakte $client */
        $client = Yii::$app->authClientCollection->clients['facebook'];
        $data = $client->api('me', 'GET');
        VarDumper::dump($data);
        return self::jsonSuccess($data);
    }


    /**
     * AJAX
     *
     * REQUEST:
     * - value - float - сумма для начисления с плавающей точкой '.'
     *
     * @return string
     */
    public function actionAddAjax()
    {
        $value = self::getParam('value');
        if ($value) {
            if ($value <= 0) {
                return self::jsonErrorId(101, 'Сумма начисления не может быть 0 или отрицательной');
            }
        } else {
            return self::jsonErrorId(101, 'Сумма начисления не может быть 0 или отрицательной');
        }
        $request = new InRequest([
            'wallet_id' => Yii::$app->user->identity->getWallet()->id,
            'price'     => $value,
            'datetime'  => time(),
        ]);
        $request->save();

        return self::jsonSuccess($request->id);
    }

    public function actionReferal_link()
    {
        $model = new \app\models\Form\WalletInvite();
        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }
}
