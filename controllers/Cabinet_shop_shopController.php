<?php

namespace app\controllers;

use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Query;


/**
 * Обслуживает действия магазина
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class Cabinet_shop_shopController extends CabinetBaseController
{
    /**
     * Редактирование магазина
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     */
    public function actionIndex($id)
    {
        $union = Union::find($id);
        $model = \app\models\Form\Shop::find(['union_id' => $id]);
        if (is_null($model)) $model = new \app\models\Form\Shop();
        if ($model->load(Yii::$app->request->post()) && $model->update2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * Добавление товара
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionProduct_list_add($id)
    {
        $model = new \app\models\Form\Shop\Product([
            'union_id' => $id,
        ]);

        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->insert2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * AJAX
     * Отправка продукиа на модерацию
     *
     * @param int $id идентификатор товара
     *
     * @return string json
     */
    public function actionProduct_list_send_moderation($id)
    {
        $product = Product::find($id);
        $union = Union::find($product->getField('union_id'));
        if ($union->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(103, 'Товар принадлежит не вашему объединению');
        }
        $shop = Shop::find(['union_id' => $product->getField('union_id')]);
        if ($shop->getModerationMode() == 1) {
            // нужна модерация
            $product->update(['moderation_status' => Product::MODERATION_STATUS_SEND]);
            foreach(User::getQueryByRole(User::USER_ROLE_MODERATOR)
                        ->select(['email'])
                        ->column() as $moderatorMail) {
                Application::mail($moderatorMail, 'Добавлен товар', 'shop/moderation_product_add', [
                    'shop'    => $shop,
                    'product' => $product,
                ]);
            }

            return self::jsonSuccess();
        } else {
            return self::jsonSuccess([102, 'Модерация у объединения отключена']);
        }
    }

    /**
     * редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionProduct_list_edit($id)
    {
        $model = \app\models\Form\Shop\Product::find($id);
        $union = \app\models\Union::find($model->union_id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * AJAX
     * удаление товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionProduct_list_delete($id)
    {
        $model = \app\models\Form\Shop\Product::find($id);
        $union = \app\models\Union::find($model->union_id);
        if (is_null($union)) {
            return self::jsonErrorId(101, 'Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Вы не можете удалять товары чужого магазина');
        }
        if (RequestProduct::query(['product_id' => $id])->exists()) {
            return self::jsonErrorId(103, 'Этот товар уже закреплен за одним из заказов');
        }
        $model->delete();

        return self::jsonSuccess();
    }

    /**
     * @param int $id идентификатор объединения
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionProduct_list($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    /**
     * Выводит список доставки
     *
     * @param int $id идентификатор объединения
     * @return string
     *
     * @throws \cs\web\Exception
     */
    public function actionDostavka_list($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    /**
     * AJAX
     * Удаляет заказ
     *
     * @param int $id идентификатор заказа
     * @return string
     */
    public function actionRequest_list_delete($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(101, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Вы не можете удаялять заказы чужого магазина');
        }
        (new Query())->createCommand()->delete(RequestMessage::TABLE, ['request_id' => $request->getId()])->execute();
        (new Query())->createCommand()->delete(RequestProduct::TABLE, ['request_id' => $request->getId()])->execute();
        $request->delete();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Скрывает заказ
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionRequest_list_hide($id)
    {
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(101, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Вы не можете скрывать заказы чужого магазина');
        }
        $request->update(['is_hide' => 1]);

        return self::jsonSuccess();
    }

    /**
     * Добавление доставки
     * @param int $id идентификатор объединения gs_unions.id
     * @return string|\yii\web\Response
     * @throws \cs\web\Exception
     */
    public function actionDostavka_list_add($id)
    {
        $model = new \app\models\Form\Shop\DostavkaItem([
            'union_id' => $id,
        ]);

        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->insert2($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * редактирование товара
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionDostavka_list_edit($id)
    {
        $model = \app\models\Form\Shop\DostavkaItem::find($id);
        $union = \app\models\Union::find($model->union_id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union' => $union,
            ]);
        }
    }

    /**
     * @param int $id идентификатор объединения
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionRequest_list($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    /**
     * Показывает все заказы, в том числе исполненные и скрытые
     * @param int $id идентификатор объединения
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionRequest_list_all($id)
    {
        $union = \app\models\Union::find($id);
        if (is_null($union)) {
            throw new Exception('Не найдено такое объединение');
        }
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Вы не можете редактировать товары чужого магазина');
        }

        return $this->render([
            'union_id' => $id,
            'union'    => $union,
        ]);
    }

    public function actionRequest_list_item($id)
    {
        return $this->render([
            'request' => Request::find($id),
        ]);
    }

    /**
     * AJAX
     * Отправляет сообщение к заказу для клиента
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_message($id)
    {
        $text = self::getParam('text');
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(201, 'Этот заказ принадлежит объединению которое принадлежит не вам');
        }
        $request->addMessageToClient($text);
        // отправляю письмо
        Shop::mail($request->getUser()->getEmail(), 'Заказ #'.$request->getId().'. Новое сообщение', 'shop/request_to_client_message', [
            'request' => $request,
            'text'    => $text,
        ]);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Оплата подтверждена
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_answer_pay($id)
    {
        return $this->sendStatus($id, Request::STATUS_PAID_SHOP, function (Request $request, $text) {
            $request->update(['is_paid' => 1]);
            $dostavka = $request->getDostavkaItem();
            if ($dostavka->getField('is_need_address', 0) == 1) {
                // если есть адрес
                $request->addStatusToClient(Request::STATUS_DOSTAVKA_ADDRESS_PREPARE);
            } else {
                $request->addStatusToClient(Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT);
            }

            // отправляю письмо
            Shop::mail($request->getUser()->getEmail(), 'Заказ #'.$request->getId().'. Оплата подтверждена', 'shop/request_to_client_answer_pay', [
                'request' => $request,
                'text'    => $text,
            ]);
        });
    }

    /**
     * AJAX
     * Устанавливает статус для заказа: Клиенту вручен заказ
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_samovivoz_done($id)
    {
        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP);
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Заказ выполнен
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_send($id)
    {
        $request = Request::find($id);
        if ($request->getStatus() == Request::STATUS_DOSTAVKA_ADDRESS_PREPARE) {
            $request->addStatusToClient(Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE);
        }

        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER, function (Request $request, $text) {
            // отправляю письмо
            Shop::mail($request->getUser()->getEmail(), 'Заказ #'.$request->getId().'. Товар отправлен клиенту', 'shop/request_to_client_send', [
                'request' => $request,
                'text'    => $text,
            ]);
        });
    }

    /**
     * AJAX
     * Отправляет сообщение для клиента: Заказ выполнен
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     *
     * @return \yii\web\Response json
     */
    public function actionRequest_list_item_prepared($id)
    {
        return $this->sendStatus($id, Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE, function (Request $request, $text) {
            // отправляю письмо
            Shop::mail($request->getUser()->getEmail(), 'Заказ #'.$request->getId().'. Товар подготовлен к отправке', 'shop/request_to_client_prepared', [
                'request' => $request,
                'text'    => $text,
            ]);
        });
    }

    /**
     * Заготовка для отправки статуса с сообщением
     *
     * REQUEST:
     * - text - string - текст сообщения
     *
     * @param int $id идентификатор заказа gs_users_shop_requests.id
     * @param int $status статус
     *
     * @return \yii\web\Response json
     */
    private function sendStatus($id, $status, \Closure $callback = null)
    {
        $text = self::getParam('text');
        $request = Request::find($id);
        if (is_null($request)) {
            return self::jsonErrorId(102, 'Не найден заказ');
        }
        if ($request->getUnion()->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(201, 'Этот заказ принадлежит объединению которое принадлежит не вам');
        }
        $request->addStatusToClient([
            'message' => $text,
            'status'  => $status,
        ]);
        if ($callback) {
            $callback($request, $text);
        }

        return self::jsonSuccess();
    }
}
