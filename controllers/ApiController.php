<?php

namespace app\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class ApiController extends BaseController
{
    public $db = [
        'dsn'      => 'mysql:host=localhost;dbname=information_schema',
        'username' => 'root',
        'password' => '1234',
        'charset'  => 'utf8',
    ];

    /**
     * POST
     * REQUEST:
     *
     */
    public function actionDbGetStructure()
    {
        $db = new Connection($this->db);
        $tablesRules = [
            'TABLE_NAME'      => 'name',
            'ENGINE'          => 'engine',
            'TABLE_COLLATION' => 'collation',
        ];
        $columnsRules = [
            'COLUMN_NAME'       => 'name',
            'COLUMN_DEFAULT'    => 'default',
            'IS_NULLABLE'       => 'is_nullable',
            'DATA_TYPE'         => 'data_type',
            'NUMERIC_PRECISION' => 'numeric_precision',
            'NUMERIC_SCALE'     => 'numeric_scale',
            'COLUMN_TYPE'       => 'type',
            'COLUMN_KEY'        => 'key',
            'EXTRA'             => 'extra',
        ];

        $tables = (new Query())->select('*')->from('TABLES')->where(['TABLE_SCHEMA' => 'galaxysss_2'])->all($db);
        $res = [];
        foreach ($tables as $table) {
            $item = $this->convertRow($table, $tablesRules);
            $columns = (new Query())->select('*')->from('COLUMNS')->where([
                'TABLE_SCHEMA' => 'galaxysss_2',
                'TABLE_NAME'   => $table['TABLE_NAME'],
            ])->orderBy(['ORDINAL_POSITION' => SORT_ASC])->all($db);
            foreach($columns as $column) {
                $item['columns'][] = $this->convertRow($column, $columnsRules);
            }
            $res[] = $item;
        }

        return self::jsonSuccess($res);
    }

    /**
     * POST
     * REQUEST:
     * - key - string - ключ приложения `Yii::$app->params['api']['key']`
     * - limit - int - кол-во строк в выдаче, по умолчанию 500
     * - offset - индекс первой строки, по умолчанию 0
     * + table - string - название
     */
    public function actionDbGetRows()
    {
        $limit = self::getParam('limit', 500);
        $offset = self::getParam('offset', 0);
        $table = self::getParam('table');
        if (is_null($table)) {
            return self::jsonErrorId(101, 'Не указан обязательный параметр table');
        }
        $array = (new Query())->select('*')->from($table)->limit($limit)->offset($offset)->all();
        $rows = [];
        foreach($array as $row) {
            $rows[] = array_values($row);
        }
        if ($array) {
            $columns = array_keys($array[0]);
            return self::jsonSuccess([$columns, $rows]);
        } else {
            return self::jsonSuccess([]);
        }
    }

    /**
     * Выдает список файлов в папке `upload`
     * POST
     */
    public function actionUploadFiles()
    {
        $zip = new \ZipArchive();
        $path = Yii::getAlias('@runtime/1.zip');
        $zip->open($path, \ZipArchive::CREATE);
        $zip->addFromString('files.json', json_encode(UploadFolderDispatcher::getStructure()));
        $zip->close();

        return Yii::$app->response->sendContentAsFile(file_get_contents($path), 'files.zip');
    }

    /**
     * Преобразовывает строку по правилам
     * @param $row
     * @param $rules
     *
     * @return array
     */
    public function convertRow($row, $rules)
    {
        $item = [];
        $keys = array_keys($rules);
        foreach($row as $key => $value) {
            if (in_array($key, $keys)) {
                $item[ $rules[$key] ] = $row[$key];
            }
        }

        return $item;
    }

}
