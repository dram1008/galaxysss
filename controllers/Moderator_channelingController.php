<?php

namespace app\controllers;

use app\models\Article;
use app\models\Chenneling;
use app\models\SiteUpdate;
use app\models\Union;
use app\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class Moderator_channelingController extends AdminBaseController
{

    private function doAction($id, $callback)
    {
        $item = \app\models\Chenneling::find($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдено послание');
        }
        $callback($item);

        return self::jsonSuccess();
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionView($id)
    {
        $i = Chenneling::find($id);
        if (is_null($i)) {
            throw new Exception('Нет такого послания');
        }

        return $this->render([
            'item' => $i
        ]);
    }

    public function actionAccept($id)
    {
        return self::doAction($id, function (\app\models\Chenneling $item) {
            $item->accept();
            \cs\Application::mail($item->getUser()->getEmail(), 'Ваше послание прошло модерацию', 'moderator/channeling_accept', [
                    'item' => $item,
                ]);
        });
    }

    /**
     * Отклоняет объединение
     *
     * REQUEST:
     * - reason - str - причина отклонения
     *
     * @param int $id идентификатор объединения
     *
     * @return \yii\web\Response
     */
    public function actionReject($id)
    {
        return self::doAction($id, function (\app\models\Chenneling $item) {
            $item->reject();
            \cs\Application::mail($item->getUser()->getEmail(), 'Ваше послание отклонено модератором', 'moderator/channeling_reject', [
                    'item'   => $item,
                    'reason' => self::getParam('reason'),
                ]);
        });
    }
}
