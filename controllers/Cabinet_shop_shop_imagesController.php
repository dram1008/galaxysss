<?php

namespace app\controllers;

use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\modules\Shop\services\Basket;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;


/**
 * обслуживает действия магазина с картинками
 *
 * Class Cabinet_shop_shopController
 * @package app\controllers
 */
class Cabinet_shop_shop_imagesController extends CabinetBaseController
{
    /**
     * Список картинок
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionIndex($id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            throw new Exception('Не найдено такой товар');
        }
        $union = $product->getUnion();
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Этот товар относится не к вашему объединению');
        }

        return $this->render([
            'query'   => $product->getImages(),
            'union'   => $union,
            'product' => $product,
        ]);
    }

    /**
     * Добавление картинки
     *
     * @param int $id идентификатор товара gs_unions_shop_product.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionAdd($id)
    {
        $model = new \app\models\Form\Shop\ProductImage([
            'product_id' => $id,
        ]);

        $product = Product::find($id);
        if (is_null($product)) {
            throw new Exception('Не найдено такой товар');
        }
        $union = $product->getUnion();
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Этот товар относится не к вашему объединению');
        }

        if ($model->load(Yii::$app->request->post()) && $model->add($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
                'union'   => $union,
                'product' => $product,
            ]);
        }
    }

    /**
     * редактирование картинки
     *
     * @param int $id идентификатор картинки gs_unions_shop_product_images.id
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionEdit($id)
    {
        $model = \app\models\Form\Shop\ProductImage::find($id);
        $product = Product::find($model->product_id);
        if (is_null($product)) {
            throw new Exception('Не найдено такой товар');
        }
        $union = $product->getUnion();
        if ($union->getField('user_id') != Yii::$app->user->id) {
            throw new Exception('Этот товар относится не к вашему объединению');
        }

        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model'   => $model,
                'union'   => $union,
                'product' => $product,
            ]);
        }
    }

    /**
     * AJAX
     * удаление картинки
     *
     * @param int $id идентификатор товара gs_unions_shop_product_images.id
     *
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = \app\models\Form\Shop\ProductImage::find($id);
        $product = Product::find($model->product_id);
        if (is_null($product)) {
            return self::jsonErrorId(101, 'Не найдено такой товар');
        }
        $union = $product->getUnion();
        if ($union->getField('user_id') != Yii::$app->user->id) {
            return self::jsonErrorId(102, 'Этот товар относится не к вашему объединению');
        }
        $model->delete();

        return self::jsonSuccess();
    }
}
