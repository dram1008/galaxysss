<?php

namespace app\controllers;

use app\models\Chenneling;
use app\models\Form\Event;
use app\models\HD;
use app\models\HdGenKeys;
use app\models\HDtown;
use app\models\Log;
use app\models\SiteUpdate;
use app\models\Union;
use app\models\User;
use app\models\UserRod;
use app\services\GetArticle\YouTube;
use app\services\GraphExporter;
use app\services\HumanDesign2;
use app\services\investigator\MidWay;
use app\services\LogReader;
use cs\Application;
use cs\base\BaseController;
use cs\helpers\Html;
use cs\services\SitePath;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\services\RegistrationDispatcher;

class Human_designController extends BaseController
{
    public $layout = 'menu';

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionCross()
    {
        return $this->render();
    }



    /**
     * AJAX
     * Расчет дизайна
     *
     * REQUEST:
     * - date - string - 'dd.mm.yyyy'
     * - time - string - 'hh:mm'
     * - londonOffset - int - смещение при летнем времени в секундах на лондоне
     * - placeOffset - int - смещение в секундах для места назначения
     * - location - string - lat,lng
     * - verifyCode - string - строка генерируемая страницей отображения формы и записываемая в переменную сессии под ключом 'route:human_design/index/code'
     *
     * @return array
     */
    public function actionAjax()
    {
        $date = self::getParam('date');
        $time = self::getParam('time');
        $verifyCode = self::getParam('verifyCode');
        $session = Yii::$app->getSession();
        $sessionVerifyCode = $session->get('route:human_design/index/code', '');
        if ($sessionVerifyCode == '') {
            return self::jsonErrorId(101, 'Проверочный код не соответсвует указанному в форме');
        } else {
            if ($sessionVerifyCode != $verifyCode) {
                return self::jsonErrorId(102, 'Проверочный код не соответсвует указанному в форме');
            }
        }

        $date = explode('.', $date);
        $placeOffset = (int)self::getParam('placeOffset');
        $londonOffset = (int)self::getParam('londonOffset');
        $location = self::getParam('location');
        $location = explode(',', $location);
        // получаю время и дату в лондоне
        {
            $t2 = new \DateTime($date[2].'-'.$date[1].'-'.$date[0].' '.$time.':00', new \DateTimeZone('UTC'));
            $timestamp = (int)$t2->format('U');
            $timestamp = $timestamp - $placeOffset + $londonOffset;
            $londonTime = new \DateTime('@'.$timestamp, new \DateTimeZone('UTC'));
            $dateLondon = $londonTime->format('Y-m-d H:i:s');
            $t = new \DateTime($dateLondon, new \DateTimeZone('Europe/London'));
        }

        // получаю данные Дизайна Человека
        $extractor = new \app\modules\HumanDesign\calculate\HumanDesignAmericaCom();
        $data = $extractor->calc($t->format('U').'000', $t->format('d/m/Y'), $t->format('H:i'));

        $html = Yii::$app->view->renderFile('@app/views/human_design/ajax.php', [
            'human_design'  => $data,
            'birth_date'    => $date[2].'-'.$date[1].'-'.$date[0],
            'birth_time'    => $time.':00',
            'birth_lat'     => $location[0],
            'birth_lng'     => $location[1],
        ]);

        return self::jsonSuccess($html);
    }

    /**
     * @param int $id номер генного ключа
     * @return string
     * @throws Exception
     */
    public function actionGen_keys_item($id)
    {
        $genKey = HdGenKeys::find(['num' => $id]);
        if (is_null($genKey)) {
            throw new Exception('Не найден такой ключ');
        }

        return $this->render([
            'item' => $genKey,
        ]);
    }

}
