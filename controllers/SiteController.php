<?php

namespace app\controllers;

use app\models\Chenneling;
use app\models\ChennelingAuthor;
use app\models\ChennelingTree;
use app\models\Form\Event;
use app\models\HD;
use app\models\HDtown;
use app\models\Log;
use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\SiteUpdate;
use app\models\Union;
use app\models\User;
use app\models\User2;
use app\models\UserRod;
use app\services\GetArticle\YouTube;
use app\services\GraphExporter;
use app\services\HumanDesign2;
use app\services\investigator\MidWay;
use app\services\LogReader;
use app\services\Subscribe;
use cs\Application;
use cs\base\BaseController;
use cs\helpers\Html;
use cs\services\BitMask;
use cs\services\SitePath;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\services\RegistrationDispatcher;

class SiteController extends BaseController
{
    public $layout = 'menu';

    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@app/views/site/error.php'
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionTest_ajax()
    {
        $v = 1/0;
    }

    /**
     * AJAX
     *
     * REQUEST:
     * - status: int,
     * - url: string,
     * - type: string 'get'|'post'
     * - data: mixed
     *
     * @return Response
     */
    public function actionError_ajax_report()
    {
        $mailList = Yii::$app->params['adminEmail'];
        if (!is_array($mailList)) {
            $mailList = [$mailList];
        }
        foreach($mailList as $mail) {
            Application::mail($mail, 'ОШИБКА AJAX GALAXYSSS.RU', 'error_ajax_report', [
                'status' => self::getParam('status'),
                'url'    => self::getParam('url'),
                'type'   => self::getParam('type'),
                'data'   => self::getParam('data'),
            ]);
        }


        return self::jsonSuccess();
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'events' => \app\models\Event::query()
                ->limit(3)
                ->where(['>=', 'end_date', gmdate('Ymd')])
                ->orderBy([
                    'date_insert' => SORT_DESC,
                ])
                ->all(),
        ]);
    }

    public function actionThank()
    {
        return $this->render();
    }

    public function actionMinistration()
    {
        return $this->render();
    }

    public function actionHuman_design()
    {
        return $this->render();
    }

    /**
     * AJAX
     * Расчет дизайна
     *
     * REQUEST:
     * - date - string - 'dd.mm.yyyy'
     * - time - string - 'hh:mm'
     * - londonOffset - int - смещение при летнем времени в секундах на лондоне
     * - placeOffset - int - смещение в секундах для места назначения
     * - location - string - lat,lng
     *
     * @return array
     */
    public function actionHuman_design_ajax()
    {
        $date = self::getParam('date');
        $time = self::getParam('time');
        $date = explode('.', $date);
        $placeOffset = (int)self::getParam('placeOffset');
        $londonOffset = (int)self::getParam('londonOffset');
        $location = self::getParam('location');
        $location = explode(',', $location);
        // получаю время и дату в лондоне
        {
            $t2 = new \DateTime($date[2].'-'.$date[1].'-'.$date[0].' '.$time.':00', new \DateTimeZone('UTC'));
            $timestamp = (int)$t2->format('U');
            $timestamp = $timestamp - $placeOffset + $londonOffset;
            $londonTime = new \DateTime('@'.$timestamp, new \DateTimeZone('UTC'));
            $dateLondon = $londonTime->format('Y-m-d H:i:s');
            $t = new \DateTime($dateLondon, new \DateTimeZone('Europe/London'));
        }

        // получаю данные Дизайна Человека
        $extractor = new \app\modules\HumanDesign\calculate\HumanDesignAmericaCom();
        $data = $extractor->calc($t->format('U').'000', $t->format('d/m/Y'), $t->format('H:i'));

        $html = Yii::$app->view->renderFile('@app/views/site/human_design_ajax.php', [
            'human_design'  => $data,
            'birth_date'    => $date[2].'-'.$date[1].'-'.$date[0],
            'birth_time'    => $time.':00',
            'birth_lat'     => $location[0],
            'birth_lng'     => $location[1],
        ]);

        return self::jsonSuccess($html);
    }

    public function actionTeslagen()
    {
        return $this->redirect('/blog/2015/12/07/mavzoley__okkultnoe_stroenie');
    }

    public function actionConditions()
    {
        return $this->render();
    }

    public function actionHologram()
    {
        return $this->render();
    }

    /**
     * Выводит карточку профиля
     *
     * @param $id
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionUser($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            throw new \cs\web\Exception('Пользователь не найден');
        }

        return $this->render([
            'user' => $user,
        ]);
    }

    /**
     *  Прием уведомлений о платежах
     */
    public function actionMoney()
    {
        Yii::info(\yii\helpers\VarDumper::dumpAsString(Yii::$app->request->post()), 'gs\\money');

        return self::jsonSuccess();
    }

    /**
     *  Прием уведомлений о платежах
     */
    public function actionThankyou()
    {
        return $this->render();
    }

    public function actionService()
    {
        return $this->render();
    }

    public function actionTest()
    {
        VarDumper::dump(Json::decode('{"image":"/upload/HumanDesign/00005783.png","type":{"text":"Манифестирующий генератор","href":"/types/manifesting-generator"},"profile":{"text":"3/5","href":"/profiles/profile-3-5"},"definition":{"text":"Одинарное"},"inner":{"text":"Сакрал"},"strategy":{"text":"Откликаться"},"theme":{"text":"Фрустрация"},"cross":{"text":"Правоугольныйкрест Инфицирования (30/29 | 14/8)"},"signature":{"text":"Удовлетворение"},"extended":"{\"id\":\"88663657109702a16ed\",\"name\":\"Your Chart From Jovian\",\"birthCountry\":\"United Kingdom\",\"birthPlace\":\"London\",\"localBirthYear\":\"1976\",\"localBirthMonth\":\"2\",\"localBirthDay\":\"16\",\"localBirthHour\":\"6\",\"localBirthMinute\":\"0\",\"localBirthEpochTicks\":\"193298400000\",\"utcBirthEpochTicks\":\"193298400000\",\"chartData\":\"{\\\"design\\\":{\\\"sun\\\":[14,5,3,6,1,1,0,74,47,83,2,14],\\\"earth\\\":[8,5,3,6,1,0,0,74,47,83,2,14],\\\"nnode\\\":[43,4,1,4,5,0,0,51,10,63,81,7],\\\"snode\\\":[23,4,1,4,5,0,0,51,10,63,81,7],\\\"moon\\\":[15,3,3,2,1,0,0,39,36,19,18,93],\\\"mercury\\\":[14,1,1,3,2,0,1,1,6,38,32,63],\\\"venus\\\":[48,4,3,6,1,0,0,57,47,86,18,94],\\\"mars\\\":[15,3,6,4,5,0,0,49,94,66,96,83],\\\"jupiter\\\":[51,1,2,4,1,0,0,4,25,51,10,53],\\\"saturn\\\":[31,1,6,6,5,0,0,16,99,96,80,2],\\\"uranus\\\":[28,3,4,3,3,0,0,42,56,40,41,8],\\\"neptune\\\":[9,6,4,6,1,0,0,94,64,84,7,36],\\\"pluto\\\":[48,2,3,6,3,1,1,24,48,91,47,35]},\\\"personality\\\":{\\\"sun\\\":[30,3,3,1,2,0,0,38,33,3,22,14],\\\"earth\\\":[29,3,3,1,2,0,0,38,33,3,22,14],\\\"nnode\\\":[1,3,6,6,1,0,1,49,97,83,2,13],\\\"snode\\\":[2,3,6,6,1,0,0,49,97,83,2,13],\\\"moon\\\":[59,5,4,1,2,0,0,75,50,3,22,12],\\\"mercury\\\":[60,5,3,1,2,0,0,72,34,6,36,83],\\\"venus\\\":[61,6,3,1,1,0,0,88,33,2,12,61],\\\"mars\\\":[45,2,5,2,3,0,1,28,70,24,46,34],\\\"jupiter\\\":[42,2,1,3,2,0,0,17,6,36,20,2],\\\"saturn\\\":[56,2,1,6,3,0,0,19,15,92,57,86],\\\"uranus\\\":[28,6,3,6,1,0,0,91,47,83,1,8],\\\"neptune\\\":[5,3,4,1,5,1,1,42,52,14,89,46],\\\"pluto\\\":[48,3,1,2,2,0,0,33,3,22,33,68]},\\\"summary\\\":{\\\"birthDateTicks\\\":193298400000,\\\"designDateTicks\\\":185815672000,\\\"chartType\\\":1,\\\"chartProfile\\\":35,\\\"chartDefinition\\\":1,\\\"chartAuthority\\\":1,\\\"chartCross\\\":132},\\\"centers\\\":[{\\\"ID\\\":3,\\\"defined\\\":false,\\\"ActivatingGates\\\":[30]},{\\\"ID\\\":1,\\\"defined\\\":true,\\\"ActivatingGates\\\":[29,59,14,42,5,9]},{\\\"ID\\\":5,\\\"defined\\\":true,\\\"ActivatingGates\\\":[1,2,15]},{\\\"ID\\\":6,\\\"defined\\\":true,\\\"ActivatingGates\\\":[8,45,56,23,31]},{\\\"ID\\\":0,\\\"defined\\\":false,\\\"ActivatingGates\\\":[60]},{\\\"ID\\\":8,\\\"defined\\\":false,\\\"ActivatingGates\\\":[61]},{\\\"ID\\\":2,\\\"defined\\\":false,\\\"ActivatingGates\\\":[28,48]},{\\\"ID\\\":7,\\\"defined\\\":true,\\\"ActivatingGates\\\":[43]},{\\\"ID\\\":4,\\\"defined\\\":false,\\\"ActivatingGates\\\":[51]}],\\\"channels\\\":[\\\"12\\\",\\\"11\\\",\\\"20\\\",\\\"5\\\"]}\",\"chartType\":\"1\",\"chartProfile\":\"35\",\"chartDefinition\":\"1\",\"chartAuthority\":\"1\",\"chartCross\":\"132\",\"dateCreated\":\"1460705027\",\"dateModified\":\"1460705027\",\"type\":\"standard\",\"comingFromJovian\":true}"}'));
        $d = microtime(true);
        $id = 48;
        /** @var \app\models\User $me */
        $me = Yii::$app->user->identity;
        $wallet1 = $me->getWallet();
        $wallet1->parent_id = $id;
        $wallet1->save();
        $wallet1->in(10000, new \app\models\Piramida\WalletSource\God(), 'Приход с яндекс денег');
        $wallet2 = Wallet::findOne($id);
        $comment = 'Перевод по регистрации $id=' . $id;
        $wallet1->move($wallet2, 2000, $comment);
        if ($wallet2->parent_id) {
            $wallet3 = Wallet::findOne($wallet2->parent_id);
            $wallet1->move($wallet3, 2000, $comment);
            if ($wallet3->parent_id) {
                $wallet4 = Wallet::findOne($wallet3->parent_id);
                $wallet1->move($wallet4, 2000, $comment);
            }
        }
        $wallet1->move(Piramida::getGlobalWallet(), 4000, $comment);
        VarDumper::dump(microtime(true) - $d);
    }

    /**
     * Возвращает элементы списка
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public function getRows($parentId = null)
    {
        $rows = (new Query())
            ->select('id, header as name')
            ->from('gs_unions_tree')
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC])
            ->all();
        for($i = 0; $i < count($rows); $i++ ) {
            $item = &$rows[$i];
            $rows2 = $this->getRows($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
            $item['unions'] = $unions = Union::query(['tree_node_id' => $item['id']])->all();
        }

        return $rows;
    }


    public function actionStatistic()
    {
        $data = User::query()
            ->select([
                'datetime_reg',
            ])
            ->column();
        $data2 = \app\services\Statistic::getIncrementDataAllGraphic($data, 'Y-m-d');

        $new = [];
        for ($i = 0; $i < count($data2['x']); $i++) {
            if ($data2['x'][ $i ] > '2015-07-18') {
                $new['x'][] = (new \DateTime($data2['x'][ $i ]))->format('d.m');
                $new['y'][] = $data2['y'][ $i ];
            }
        }
        $data2 = $new;
        $data3 = GraphExporter::convert([
            'rows'             => [User::query()
                ->select([
                    'COUNT(id) as `kurs`',
                    'DATE(datetime_reg) as `date`',
                ])
                ->groupBy('DATE(datetime_reg)')
                ->all()],
            'formatX'          => 'd.m',
            'start'            => new \DateTime('2015-07-05'),
            'isExcludeWeekend' => false,
        ]);

        return $this->render([
            'lineArray'  => $data3,
            'lineArray2' => [
                'x' => $data2['x'],
                'y' => [$data2['y']],
            ],
        ]);
    }

    public function actionSite_update()
    {
        \app\services\SiteUpdateItemsCounter::clear();
        \app\services\UserLastActive::updateNow();

        return $this->render([
            'list' => SiteUpdate::query()
                ->orderBy(['date_insert' => SORT_DESC])
                ->limit(50)
                ->all()
        ]);
    }

    public function actionSite_update_ajax()
    {
        $typeId = self::getParam('id');

        return self::jsonSuccess($this->renderFile('@app/views/site/site_update_ajax.php', [
            'list' => SiteUpdate::query(['type' => $typeId])->orderBy(['date_insert' => SORT_DESC])->limit(50)->all()
        ]));
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['mailList']['contact'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionUser_rod($user_id, $rod_id)
    {
        $user = UserRod::find(['user_id' => $user_id, 'rod_id' => $rod_id]);
        if (is_null($user)) {
            $user = new UserRod([
                'user_id' => $user_id,
                'rod_id'  => $rod_id,
            ]);
        }
        $path = $user->getRodPath();
        $breadcrumbs = [];
        foreach ($path as $i) {
            $breadcrumbs[] = [
                'label' => (is_null($i['name'])) ? '?' : $i['name'],
                'url'   => [
                    'site/user_rod',
                    'user_id' => $user_id,
                    'rod_id'  => $i['id'],
                ],
            ];
        }

        return $this->render([
            'userRod'     => $user,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Выводит весь род человека
     *
     * @param int $id идентификатор пользователя
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionUser_rod_list($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            throw new \cs\web\Exception('Не найден пользователь');
        }
        $rod = UserRod::query(['user_id' => $id])->all();
        $rows = [];
        for($i = 1; $i < 127; $i++) {
            $new = null;
            foreach($rod as $item) {
                if ($item['rod_id'] == $i) {
                    $new = $item;
                }
            }
            if (is_null($new)) {
                $new = [
                    'rod_id' => $i
                ];
            }
            $rows[$i] = $new;
        }

        return $this->render([
            'items' => $rows,
            'user'  => $user,
        ]);
    }

    public function actionUser_rod_edit($user_id, $rod_id)
    {
        if (Yii::$app->user->isGuest) {
            throw new \cs\web\Exception('Вы не можете редактировать данные');
        }
        if (Yii::$app->user->id != $user_id) {
            throw new \cs\web\Exception('Вы не можете редактировать чужие данные');
        }

        $user = UserRod::find(['user_id' => $user_id, 'rod_id' => $rod_id]);
        if (is_null($user)) {
            $user = UserRod::insert([
                'user_id' => $user_id,
                'rod_id'  => $rod_id,
            ]);
        }
        $path = $user->getRodPath();
        $breadcrumbs = [];
        foreach ($path as $i) {
            $breadcrumbs[] = [
                'label' => (is_null($i['name'])) ? '?' : $i['name'],
                'url'   => [
                    'site/user_rod_edit',
                    'user_id' => $user_id,
                    'rod_id'  => $i['id'],
                ],
            ];
        }
        $model = new \app\models\Form\UserRod($user->getFields());
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model'       => $model,
                'breadcrumbs' => $breadcrumbs,
            ]);
        }
    }

    public function actionLogo()
    {
        return $this->render();
    }

    public function actionLog()
    {
        $first = self::getParam('first');

        return $this->render([
            'log' => LogReader::file('@runtime/logs/app.log')->readLast(
                ArrayHelper::merge(
                    ['maxStrings' => 10],
                    Yii::$app->request->get()
                )
            ),
            'first' => ($first)? $first : 0,
        ]);
    }

    public function actionLog_db()
    {
        $query = Log::query()->orderBy(['log_time' => SORT_DESC]);
        $category = self::getParam('category', '');
        if ($category) {
            $query->where(['like', 'category', $category . '%', false]);
        }
        $type = self::getParam('type', '');
        if ($type) {
            switch ($type) {
                case 'INFO':
                    $type = \yii\log\Logger::LEVEL_INFO;
                    break;
                case 'ERROR':
                    $type = \yii\log\Logger::LEVEL_ERROR;
                    break;
                case 'WARNING':
                    $type = \yii\log\Logger::LEVEL_WARNING;
                    break;
                case 'PROFILE':
                    $type = \yii\log\Logger::LEVEL_PROFILE;
                    break;
                default:
                    $type = null;
                    break;
            }
            if ($type) {
                $query->where(['type' => $type]);
            }
        }

        return $this->render([
            'dataProvider' => new ActiveDataProvider([
                'query'      => $query,

                'pagination' => [
                    'pageSize' => 50,
                ],
            ])
        ]);
    }
}
