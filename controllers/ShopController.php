<?php

namespace app\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Form\Shop\Order;
use app\models\Piramida\InRequest;
use app\models\Piramida\Wallet;
use app\models\Praktice;
use app\models\Service;
use app\models\Shop;
use app\models\Shop\Payments;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\TreeNodeGeneral;
use app\models\Union;
use app\models\UnionCategory;
use app\models\User;
use app\modules\Shop\services\Basket;
use app\services\HumanDesign2;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\HttpException;


class ShopController extends BaseController
{
    public $layout = 'menu';

    public function actionIndex()
    {
        return $this->render([
            'tree' => \app\modules\Shop\services\Shop::getTree(1),
        ]);
    }

    public function actionCategory($id)
    {
        $treeNode = TreeNodeGeneral::find($id);
        if (is_null($treeNode)) {
            throw new Exception('Не найдена категория');
        }
        $items = Product::query()
            ->select([
                'gs_unions_shop_product.*',
            ])
            ->where(['gs_shop_tree_products_link.tree_node_id' => $treeNode->getId()])
            ->innerJoin('gs_shop_tree_products_link', 'gs_shop_tree_products_link.product_id = gs_unions_shop_product.id')
            ->all();

        return $this->render([
            'treeNode' => $treeNode,
            'items'    => $items,
        ]);
    }

    /**
     * Показывает продукт
     *
     * @param int $id идентификатор продукта
     *
     * @return string
     *
     * @throws Exception
     */
    public function actionProduct($id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            throw new Exception('Не найден продукт');
        }

        return $this->render([
            'product' => $product,
        ]);
    }

    /**
     * Выводит результаты посика товаров
     *
     * REQUEST:
     * - text - string - строка поиска
     *
     * @return string
     *
     * @throws Exception
     */
    public function actionSearch()
    {
        $text = self::getParam('text');

        $items = Product::query()
            ->select([
                'gs_unions_shop_product.*',
            ])
            ->where(['like', 'gs_unions_shop_product.name', $text])
            ->all();

        return $this->render([
            'text'  => $text,
        ]);
    }

    /**
     * AJAX
     * Добавление товара в корзину
     * REQUEST
     * - id - int - идентификатор товара gs_unions_shop_product.id
     *
     * @return string
     */
    public function actionBasket_add()
    {
        $id = self::getParam('id');
        $product = Product::find($id);
        if (is_null($product)) {
            return self::jsonErrorId(101, 'Не найден товар');
        }
        $count = Basket::add($product);
        Yii::$app->session->set('shop.help_window_already_showed', 1);

        return self::jsonSuccess($count);
    }

    /**
     * AJAX
     * Добавление товара в корзину
     *
     * REQUEST:
     * - product - int - идентификатор товара gs_unions_shop_product.id
     * - count - int - кол-во товаров
     *
     * @return string
     */
    public function actionBasket_update()
    {
        Basket::setCount(self::getParam('product'), self::getParam('count'));

        return self::jsonSuccess();
    }

    public function actionSet_no_help_window()
    {
        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(101, 'Пользователь не авторизован');
        }
        Yii::$app->user->identity->update(['is_show_shop_help_window' => 1]);

        return self::jsonSuccess();
    }

    public function actionBasket()
    {
        return $this->render([
            'items' => Basket::get()
        ]);
    }

    /**
     * Удаляет товар из корзины
     *
     * AJAX
     *
     * REQUEST:
     * - id - integer - идентификатор товара
     *
     * @return string
     */
    public function actionBasket_delete()
    {
        $id = self::getParam('id');
        Basket::delete($id);

        return self::jsonSuccess();
    }

    public function actionOrder()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        if (count(Basket::get()) == 0) return $this->redirect('/');
        $model = new Order();
        if ($model->load(Yii::$app->request->post()) && $model->add()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * Выдает форму для оплаты заказа
     *
     * @param int $id bog_shop_requests.id
     *
     * @return string|Response
     * @throws \cs\web\Exception
     */
    public function actionOrder_buy($id)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        $request = \app\models\Shop\Request::find($id);
        if (is_null($request)) {
            throw new Exception('Нет такого заказа');
        }
        if ($request->getField('is_paid', 0) == 1) return $this->redirect(['site_cabinet/request', 'id' => $request->getId()]);

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     * AJAX
     *
     * RESPONSE:
     * - login - string
     * - name - string
     *
     * @return array|string|Response
     */
    public function actionRegistration_ajax()
    {
        $login = strtolower(self::getParam('login'));
        $name = self::getParam('name');
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            return self::jsonErrorId(106, 'Не верный формат email');
        }
        if (User::query(['email' => $login])->exists()) {
            return self::jsonErrorId(101, 'Пользователь уже существует');
        }
        $user = User::registration_password($login, substr(str_shuffle("01234567890123456789"), 0, 4), [
            'name_first' => $name,
        ]);
        Yii::$app->user->login($user);

        return self::jsonSuccess();
    }

    /**
     * AJAX
     *
     * RESPONSE:
     * - login - string
     * - password - string
     *
     * @return array|string|Response
     */
    public function actionLogin_ajax()
    {
        $login = strtolower(self::getParam('login'));
        $password = self::getParam('password');
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            return self::jsonErrorId(106, 'Не верный формат email');
        }
        $user = User::find(['email' => $login]);
        if (is_null($user)) {
            return self::jsonErrorId(101, 'Пользователь не найден');
        }
        if (!$user->validatePassword($password)) {
            return self::jsonErrorId(102, 'Не верный пароль');
        }
        Yii::$app->user->login($user);

        return self::jsonSuccess();
    }


    /**
     * AJAX
     *
     * REQUEST
     * - id - int - идентификатор продукта
     * - comment - string
     * - dostavka - int -
     * - address - string -
     * - price - int - полная цена заказа с учетом доставки
     * - phone - string - телефон
     * - paymentType - string - способ оплаты AC/PC/DI
     *                          AC - Банковской картой
     *                          PC - Яндекс Деньги
     *                          DI - Другие способы оплаты
     *
     * @return string|Response
     */
    public function actionOrder_ajax()
    {
        $fields['union_id'] = self::getParam('union_id');
        $fields['comment'] = self::getParam('comment');
        $fields['address'] = self::getParam('address');
        $fields['dostavka'] = self::getParam('dostavka');
        $fields['price'] = self::getParam('price');
        $fields['phone'] = self::getParam('phone');
        $paymentType = self::getParam('paymentType');

        $request = \app\models\Shop\Request::add($fields, Basket::get($fields['union_id']));
        $union = $request->getUnion();
        $shop = $union->getShop();
        $dostavkaList = $shop->getDostavkaRows();
        $dostavkaText = '';
        $isAddress = 0;
        foreach ($dostavkaList as $i) {
            if ($i['id'] == $fields['dostavka']) {
                $dostavkaText = $i['name'];
                $isAddress = $i['is_need_address'];
                break;
            }
        }
        $message = [
            'Стоимость с учетом доставки: ' . $fields['price'],
            'Доставка: ' . $dostavkaText,
            ($isAddress) ? 'Адрес: ' . $fields['address'] : '',
        ];
        if (($fields['price'] > 15000) or ($paymentType == 'DI')) {
            $message = ArrayHelper::merge(
                $message,
                [
                    'Реквизиты для оплаты вашего заказа:',
                    $shop->getField('rekvizit', '')
                ]
            );
        }
        $request->addStatusToClient([
            'status'  => \app\models\Shop\Request::STATUS_ORDER_DOSTAVKA,
            'message' => join("\n", $message),
        ]);
        // письмо магазину
        Shop::mail($shop->getAdminEmail(), 'Заказ #' . $request->getId(), 'shop/request_to_shop', [
            'request'    => $request,
            'shop'       => $shop,
        ]);
        // письмо клиенту
        Shop::mail($request->getUser()->getEmail(), 'Заказ #' . $request->getId(), 'shop/request_to_client', [
            'request'    => $request,
            'shop'       => $shop,
        ]);
        Basket::clear($fields['union_id']);

        return self::jsonSuccess($request->getId());
    }

    /**
     * Прием денег из яндекса
     *
     * @return string
     */
    public function actionOrder_success()
    {
        $secretCode = Yii::$app->params['shop']['secretCode'];

        // https://money.yandex.ru/doc.xml?id=526991
        // Удостоверение подлинности и целостности уведомления

        // живой платеж
//        'notification_type' => 'card-incoming'
//        'amount' => '19.60'
//        'datetime' => '2015-12-28T15:38:38Z'
//        'codepro' => 'false'
//        'withdraw_amount' => '20.00'
//        'sender' => ''
//        'sha1_hash' => '4bf0ac57f8a86653914d68ba28a3a02c99780eed'
//        'unaccepted' => 'false'
//        'operation_label' => '1e136b24-0002-5000-8033-8fa644e381dd'
//        'operation_id' => '504632318089016012'
//        'currency' => '643'
//        'label' => '1231'


//        пример
//        'notification_type' => 'p2p-incoming'
//        'amount' => '138.29'
//        'datetime' => '2015-12-27T20:28:51Z'
//        'codepro' => 'false'
//        'sender' => '41001000040'
//        'sha1_hash' => '163ca7ebf6685d84db11985cd06df592301a1b20'
//        'test_notification' => 'true'
//        'operation_label' => ''
//        'operation_id' => 'test-notification'
//        'currency' => '643'
//        'label' => ''
        $c = [
            'bogdan' => function($id, $transaction){
                $mail = 'avia@galaxysss.ru';
                $request_id = $id;
                $request = \app\models\Shop\BogDan\Request::find($request_id);
                if (is_null($request)) {
                    if ($mail) {
                        Application::mail($mail, 'Ошибка. не найден заказ', 'shop/bogdan/payments/no_request', [
                            'fields' => $transaction,
                        ]);
                    }
                }
                if ($request->getField('price') != $transaction->withdraw_amount) {
                    if ($mail) {
                        Application::mail($mail, 'Ошибка. Сумма не соотвтствует заказу', 'shop/bogdan/payments/wrong_sum', [
                            'fields'  => $transaction,
                            'request' => $request,
                        ]);
                    }
                }
                $request->paid();
            },
            'inRequest' => function($id, $transaction){
                $requestId = $id;
                $request = InRequest::findOne($requestId);
                if (is_null($request)) {
                    self::jsonSuccess();
                }
                $request->is_paid = 1;
                $wallet = Wallet::findOne($request->wallet_id);
                $t = $wallet->in($request->price, new \app\models\Piramida\WalletSource\Yandex(['transaction' => $transaction]));
                $request->transaction_id = $t->id;
                $request->save();
            },
            'gsss' => function($id, $transaction){
                $label = explode('.', $id);
                $union_id = $label[0];
                $union = Union::find($union_id);
                $shop = $union->getShop();
                $mail = $shop->getAdminEmail();
                $request_id = $label[1];
                $request = \app\models\Shop\Request::find($request_id);
                if (is_null($request)) {
                    if ($mail) {
                        Application::mail($mail, 'Ошибка. не найден заказ', 'no_request', [
                            'fields' => $transaction,
                            'shop'   => $shop,
                        ]);
                    }
                }
                if ($request->getField('price') != $transaction->withdraw_amount) {
                    if ($mail) {
                        Application::mail($mail, 'Ошибка. Сумма не соотвтствует заказу', 'wrong_sum', [
                            'fields'  => $transaction,
                            'request' => $request,
                            'shop'    => $shop,
                        ]);
                    }
                }
                $request->paid();
            },
        ];

        // добавляю в БД
        $fields = Yii::$app->request->post();
        Yii::info(\yii\helpers\VarDumper::dumpAsString($fields), 'gs\pay');
        if (ArrayHelper::getValue($fields, 'test_notification', '') == 'true') {
            return self::jsonSuccess();
        }

        $label = ArrayHelper::getValue($fields, 'label', '');
        if ($label != '') {
            if (StringHelper::startsWith($label, 'bogdan.')) {
                $secretCode = Yii::$app->params['shop']['secretCode'];
            } else if (StringHelper::startsWith($label, 'gsss.')) {
                $label = explode('.', $label);
                $union_id = $label[1];
                $union = Union::find($union_id);
                $shop = $union->getShop();
                $secretCode = $shop->get('secret');
                if ($secretCode == '') {
                    return self::jsonSuccess();
                }
            }
        }

        $fields['is_valid'] = ($this->isValidSha1($fields, $secretCode)) ? 1 : 0;
        $fields['date_insert'] = time();
        if (Payments::query([
            'operation_id'    => $fields['operation_id'],
            'operation_label' => $fields['operation_label'],
            'sha1_hash'       => $fields['sha1_hash'],
        ])->exists()) {
            Yii::info('payments operation_id = '.$fields['operation_id'] . ' already exists', 'gs\pay');
            return self::jsonSuccess();
        }
        $dbFields = [
            'id',
            'notification_type',
            'amount',
            'datetime',
            'date_insert',
            'codepro',
            'withdraw_amount',
            'sender',
            'sha1_hash',
            'unaccepted',
            'operation_label',
            'operation_id',
            'currency',
            'label',
            'is_valid',
            'zip',
            'firstname',
            'city',
            'building',
            'lastname',
            'suite',
            'phone',
            'street',
            'flat',
            'email',
            'fathersname',
        ];
        $new = [];
        foreach($fields as $n =>  $v) {
            if (in_array($n, $dbFields)) {
                $new [$n] = $v;
            }
        }
        $transaction = new Payments($new);
        $transaction->save();

        // проверка на верность
        $label = ArrayHelper::getValue($fields, 'label', '');
        if ($label != '') {
            $pos = strpos($label, '.');
            if ($pos === false) return self::jsonSuccess();
            $prefix = substr($label, 0, $pos);
            $id = substr($label, $pos);
            if (array_key_exists($prefix, $c)) {
                $function = $c[$prefix];
                $function($id, $transaction);
            }
        }

        return self::jsonSuccess();
    }

    /**
     * Проверка целостности полей и подлинности
     *
     * @param array $fields поля
     * @param string $notification_secret секретный код от Яндекса
     *
     * @return bool
     */
    private function isValidSha1($fields, $notification_secret)
    {
        // notification_type&operation_id&amount&currency&datetime&sender&codepro&notification_secret&label
        $arr = [
            'notification_type',
            'operation_id',
            'amount',
            'currency',
            'datetime',
            'sender',
            'codepro',
            'notification_secret',
            'label',
        ];
        $str = [];
        foreach ($arr as $i) {
            if ($i == 'notification_secret') {
                $str[] = $notification_secret;
            } else {
                $str[] = ArrayHelper::getValue($fields, $i, '');
            }
        }
        $str = join('&', $str);
        $sha1 = sha1($str);

        $sha1Fields = ArrayHelper::getValue($fields, 'sha1_hash', '');
        if ($sha1Fields == '') return false;

        return $sha1Fields == $sha1;
    }

}
