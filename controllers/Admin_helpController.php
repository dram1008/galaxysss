<?php

namespace app\controllers;

use app\models\Article;
use app\models\Help;
use app\models\SiteUpdate;
use app\models\Smeta;
use app\services\Subscribe;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class Admin_helpController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render([
        ]);
    }

    public function actionAdd()
    {
        $model = new \app\models\Form\Help();
        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = \app\models\Form\Help::find($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \app\models\Form\Help::find($id)->delete();

        return self::jsonSuccess();
    }

}
