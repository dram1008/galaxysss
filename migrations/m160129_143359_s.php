<?php

use yii\db\Schema;
use yii\db\Migration;

class m160129_143359_s extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users_shop_requests ADD last_message_time int NULL;');
    }

    public function down()
    {
        echo "m160129_143359_s cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
