<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_125309_shop extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_shop_tree_products_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tree_node_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m151211_125309_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
