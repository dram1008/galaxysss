<?php

use yii\db\Schema;
use yii\db\Migration;

class m160312_231023_yii extends Migration
{
    public function up()
    {
        $this->update('gs_cheneling_list',['moderation_status' => 1]);
    }

    public function down()
    {
        echo "m160312_231023_yii cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
