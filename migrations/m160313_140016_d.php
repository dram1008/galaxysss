<?php

use yii\db\Schema;
use yii\db\Migration;

class m160313_140016_d extends Migration
{

    /**
     * Удаляет тег. чувствительна к регистру
     * @param $v
     * @param $tag
     * @return bool|string
     */
    private function delete_tag($v, $tag)
    {
        $pos = strpos($v, '<'.$tag);
        if ($pos === false) return false;
        $end = strpos($v, '>', $pos);
        $v = substr($v, 0, $pos) . substr($v,$end+1);
        $pos = strpos($v, '</'.$tag.'>');
        if ($pos === false) return false;
        $v = substr($v, 0, $pos) . substr($v, $pos+strlen('</'.$tag.'>'));

        return $v;
    }

    /**
     * Удаляет тег. не чувствительна к регистру
     * @param $v
     * @param $tag
     * @return bool|string
     */
    private function deleteTag($v, $tag)
    {
        $v = $this->delete_tag($v, $tag);
        if ($v === false) {
            return $this->delete_tag($v, strtoupper($tag));
        }

        return $v;
    }

    /**
     * Удаляет все теги. не чувствительна к регистру
     * @param $v
     * @param $tag
     * @return bool|string
     */
    private function deleteTag2($v, $tag)
    {
        $n = $v;
        do {
            $v = $n;
            $n = $this->deleteTag($v, $tag);
        } while($n !== false);

        return $v;
    }

    private function cc($v)
    {
        $arr = [
            '<br style="line-height: 20.8px;" />',
            '<br>',
            '<br/>',
            '<br />',
        ];
        foreach($arr as $i) {
            $v = str_replace($i, '</p><p>', $v);
        }
        $v = $this->deleteTag2($v, 'a');

        return $v;
    }

    public function up()
    {
        foreach(\app\models\HdGenKeys::query()->all() as $row) {
            $fields = [
                'content',
                'content_ten',
                'content_dar',
                'content_siddhi',
            ];
            $update = [];
            foreach($fields as $field) {
                $v = \yii\helpers\ArrayHelper::getValue($row, $field, '');
                if ($v != '') {
                    $update[ $field ]= $this->cc($v);
                }
            }
            $this->update('gs_hd_gen_keys',$update, ['id' => $row['id']]);
        }
    }

    public function down()
    {
        echo "m160313_140016_d cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
