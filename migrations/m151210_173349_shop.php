<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_173349_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_unions ADD is_shop TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m151210_173349_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
