<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_101243_wallet extends Migration
{
    public function up()
    {
        // пакеты акций
        $this->execute('CREATE TABLE `nw_packets` (
    `id` int(11) NOT NULL,
  `price_in` DOUBLE DEFAULT NULL,
  `price_out` DOUBLE DEFAULT NULL,
  `name` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        // Заказы акций
        $this->execute('CREATE TABLE `nw_packets_requests` (
    `id` int(11) NOT NULL,
  `packet_id` int(11) DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160410_101243_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
