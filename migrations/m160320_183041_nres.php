<?php

use yii\db\Schema;
use yii\db\Migration;

class m160320_183041_nres extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_news CHANGE content content longtext;');
    }

    public function down()
    {
        echo "m160320_183041_nres cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
