<?php

use yii\db\Schema;
use yii\db\Migration;

class m160229_084006_users extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users ADD mode_chenneling TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m160229_084006_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
