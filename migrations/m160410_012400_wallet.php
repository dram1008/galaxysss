<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_012400_wallet extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `nw_wallets_sources_link` (
  `id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `data` VARCHAR(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        /**
         * id - идентификатор транзакции
         * destination_id - идентификатор типа Назначителя (Выводчика денег)
         * data - данные для класса Выводчика денег как сообщение о танзакции в Выводчике, например
         * Если Выводчик это Яндекс, то здесь будет храниться информация о номере кошелька куда были выведены средства фио получателя
         */
        $this->execute('CREATE TABLE `nw_wallets_destinations_link` (
    `id` int(11) NOT NULL,
    `destination_id` int(11) DEFAULT NULL,
    `data` VARCHAR(4000) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160409_212755_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
