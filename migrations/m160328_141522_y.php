<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\Chenneling;

class m160328_141522_y extends Migration
{
    public function up()
    {
        $this->update(Chenneling::TABLE, ['author_id' => 11], ['id' => Chenneling::query(['author_id' => 25])->select('id')->column()]);
        $this->delete(\app\models\ChennelingAuthor::TABLE, ['id' => 25]);
    }

    public function down()
    {
        echo "m160328_141522_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
