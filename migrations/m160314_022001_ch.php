<?php

use yii\db\Schema;
use yii\db\Migration;

class m160314_022001_ch extends Migration
{
    public function up()
    {
        $this->update('gs_cheneling_list', ['user_id' => null]);
    }

    public function down()
    {
        echo "m160314_022001_ch cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
