<?php

use yii\db\Schema;
use yii\db\Migration;

class m160302_235555_u extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_cheneling_list ADD user_id int NULL;');
    }

    public function down()
    {
        echo "m160302_235555_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
