<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_234813_y extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `tree_node_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

    }

    public function down()
    {
        echo "m160329_234813_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
