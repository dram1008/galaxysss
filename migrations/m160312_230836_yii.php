<?php

use yii\db\Schema;
use yii\db\Migration;

class m160312_230836_yii extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list ADD moderation_status tinyint(1) NULL;');
    }

    public function down()
    {
        echo "m160312_230836_yii cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
