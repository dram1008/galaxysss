<?php

use yii\db\Schema;
use yii\db\Migration;

class m160328_121904_u extends Migration
{
    public function up()
    {
        $arr = \app\models\ChennelingAuthor::query()->select('old_id')->column();
        $new = [];
        foreach(\app\models\Chenneling::query()->select('tree_node_id_mask,id')->orderBy('id')->all() as $row){
            $b = new \cs\services\BitMask($row['tree_node_id_mask']);
            $c = 0;
            foreach($b->getArray() as $r) {
                if (in_array($r, $arr)) {
                    $new[$row['id']] = $r;
                    $c++;
                }
            }
        }
        foreach($new as $channelingId => $authorId) {
            $this->update('gs_cheneling_list', ['author_id' => $authorId], ['id' => $channelingId]);
        }
    }

    public function down()
    {
        echo "m160328_121904_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
