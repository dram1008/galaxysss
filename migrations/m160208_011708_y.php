<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_011708_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_hd_gen_keys  RENAME TO gs_hd;');
        $this->execute('ALTER TABLE gs_human_desing  RENAME TO gs_hd_gen_keys;');

    }

    public function down()
    {
        echo "m160208_011708_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
