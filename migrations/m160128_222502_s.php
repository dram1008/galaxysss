<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_222502_s extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_vozdayanie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `sort_index` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin');
    }

    public function down()
    {
        echo "m160128_222502_s cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
