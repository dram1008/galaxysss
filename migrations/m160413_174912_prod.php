<?php

use yii\db\Schema;
use yii\db\Migration;

class m160413_174912_prod extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_unions_shop_product ADD is_electron TINYINT(1) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.gs_unions_shop_product ADD electron_text text NULL;');
    }

    public function down()
    {
        echo "m160413_174912_prod cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
