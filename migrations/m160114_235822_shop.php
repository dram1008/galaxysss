<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_235822_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_unions_shop_product ADD price int NULL;');
    }

    public function down()
    {
        echo "m160114_235822_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
