<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_230350_cha extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list ADD power_id int NULL;');
        $this->execute('CREATE TABLE `gs_cheneling_list_power_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $arr = [];
        $d = \app\models\ChennelingTree::query(['parent_id' => 6])->select('name,id as old_id')->all();
        foreach($d as $r) {
            $arr[] = [$r['name'], $r['old_id']];
        }
        $this->batchInsert('gs_cheneling_list_power_id',['name','old_id'], $arr);

        $arr = \app\models\ChennelingPower::query()->select('old_id')->column();
        $new = [];
        foreach(\app\models\Chenneling::query()->select('tree_node_id_mask,id')->orderBy('id')->all() as $row){
            $b = new \cs\services\BitMask($row['tree_node_id_mask']);
            $c = 0;
            foreach($b->getArray() as $r) {
                if (in_array($r, $arr)) {
                    $new[$row['id']] = $r;
                    $c++;
                }
            }
        }

        $a = \app\models\ChennelingPower::query()->select('old_id,id')->all();
        $b = \yii\helpers\ArrayHelper::map($a,'old_id','id');
        foreach($new as $channelingId => $authorId) {
            $this->update('gs_cheneling_list', ['power_id' => $b[$authorId]], ['id' => $channelingId]);
        }

        $a = \app\models\ChennelingPower::query()->select('old_id')->column();
        $this->delete('gs_cheneling_tree', ['in','id', $a]);
    }

    public function down()
    {
        echo "m160329_230350_cha cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
