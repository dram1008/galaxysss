<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_100905_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_operations MODIFY COLUMN `id` bigint NOT NULL AUTO_INCREMENT;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions MODIFY COLUMN `id` bigint NOT NULL AUTO_INCREMENT;');

    }

    public function down()
    {
        echo "m160410_100905_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
