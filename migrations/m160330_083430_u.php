<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_083430_u extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_video ADD datetime_insert int NULL;');
    }

    public function down()
    {
        echo "m160330_083430_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
