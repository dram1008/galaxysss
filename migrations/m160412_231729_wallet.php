<?php

use yii\db\Schema;
use yii\db\Migration;

class m160412_231729_wallet extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `nw_in_requests` (
    `id` int(11) NOT NULL,
  `wallet_id` DOUBLE DEFAULT NULL,
  `price` DOUBLE DEFAULT NULL,
  `datetime` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160412_231729_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
