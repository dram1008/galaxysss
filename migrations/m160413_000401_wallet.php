<?php

use yii\db\Schema;
use yii\db\Migration;

class m160413_000401_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_in_requests MODIFY COLUMN `id` int NOT NULL AUTO_INCREMENT;');

    }

    public function down()
    {
        echo "m160413_000401_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
