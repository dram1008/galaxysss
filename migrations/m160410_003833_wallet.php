<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_003833_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions ADD comment VARCHAR(1000) NULL;');
    }

    public function down()
    {
        echo "m160410_003833_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
