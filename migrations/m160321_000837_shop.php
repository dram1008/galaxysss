<?php

use yii\db\Schema;
use yii\db\Migration;

class m160321_000837_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_unions_shop ADD secret VARCHAR(100) NULL;');
    }

    public function down()
    {
        echo "m160321_000837_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
