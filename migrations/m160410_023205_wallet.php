<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_023205_wallet extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `nw_operations` (
    `id` int(11) NOT NULL,
  `wallet_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  `before` DOUBLE DEFAULT NULL,
  `after` DOUBLE DEFAULT NULL,
  `summa` DOUBLE DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160410_023205_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
