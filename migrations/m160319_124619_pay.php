<?php

use yii\db\Schema;
use yii\db\Migration;

class m160319_124619_pay extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD zip VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD firstname VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD city VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD building VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD lastname VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD suite VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD phone VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD street VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD flat VARCHAR(100) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD email VARCHAR(100) NULL;');
    }

    public function down()
    {
        echo "m160319_124619_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
