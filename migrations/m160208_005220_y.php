<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_005220_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_hd RENAME TO gs_hd_gen_keys;');
    }

    public function down()
    {
        echo "m160208_005220_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
