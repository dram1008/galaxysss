<?php

use yii\db\Schema;
use yii\db\Migration;

class m160401_094257_u extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_video ADD torrent_file VARCHAR(255) NULL;');
    }

    public function down()
    {
        echo "m160401_094257_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
