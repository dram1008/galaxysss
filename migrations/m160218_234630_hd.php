<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_234630_hd extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_hd_gen_keys CHANGE content content text;');

    }

    public function down()
    {
        echo "m160218_234630_hd cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
