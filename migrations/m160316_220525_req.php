<?php

use yii\db\Schema;
use yii\db\Migration;

class m160316_220525_req extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_users_shop_requests ADD is_hide TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m160316_220525_req cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
