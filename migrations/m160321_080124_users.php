<?php

use yii\db\Schema;
use yii\db\Migration;

class m160321_080124_users extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_unions_shop_product MODIFY COLUMN name varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;');
    }

    public function down()
    {
        echo "m160321_080124_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
