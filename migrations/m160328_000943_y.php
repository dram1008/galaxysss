<?php

use yii\db\Schema;
use yii\db\Migration;

class m160328_000943_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list_author_id ADD old_id int NULL;');
        $arr = [];
        $d = \app\models\ChennelingTree::query(['parent_id' => 5])->select('name,id as old_id')->all();
        foreach($d as $r) {
            $arr[] = [$r['name'], $r['old_id']];
        }
        $this->batchInsert('gs_cheneling_list_author_id',['name','old_id'], $arr);
    }

    public function down()
    {
        echo "m160328_000943_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
