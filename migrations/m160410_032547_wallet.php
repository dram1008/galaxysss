<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_032547_wallet extends Migration
{
    public function up()
    {
        $this->execute('CREATE INDEX wallet_id_index ON galaxysss_2.nw_operations (wallet_id);');
        $this->execute('CREATE INDEX datetime_index ON galaxysss_2.nw_operations (datetime);');
        $this->execute('CREATE INDEX from_index ON galaxysss_2.nw_transactions (`from`);');
        $this->execute('CREATE INDEX to_index ON galaxysss_2.nw_transactions (`to`);');
        $this->execute('CREATE INDEX datetime_index ON galaxysss_2.nw_transactions (datetime);');
    }

    public function down()
    {
        echo "m160410_032547_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
