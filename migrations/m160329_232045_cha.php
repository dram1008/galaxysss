<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_232045_cha extends Migration
{
    public function up()
    {
        $this->delete('gs_cheneling_tree',['in', 'id', [5,6]]);
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list_power_id DROP old_id;');
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list_author_id DROP old_id;');
    }

    public function down()
    {
        echo "m160329_232045_cha cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
