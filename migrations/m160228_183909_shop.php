<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_183909_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_unions_shop ADD moderation_mode TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m160228_183909_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
