<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_135952_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_unions_shop_product_images ADD moderation_status TINYINT(1) NULL;');
        $this->execute('ALTER TABLE gs_unions_shop_product_images ADD sort_index INT(11) NULL;');
    }

    public function down()
    {
        echo "m160202_135952_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
