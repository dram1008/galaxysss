<?php

use yii\db\Schema;
use yii\db\Migration;

class m160117_172659_ss extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_unions_shop_dostavka ADD is_need_address TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m160117_172659_ss cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
