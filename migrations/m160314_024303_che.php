<?php

use yii\db\Schema;
use yii\db\Migration;

class m160314_024303_che extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list ADD date_created int NULL;');
        foreach(\app\models\Chenneling::query()->all() as $item) {
            $d = $item['date_insert'];
            $d = new DateTime($d, new DateTimeZone('UTC'));
            $this->update('gs_cheneling_list', ['date_created' => $d->format('U')], ['id' => $item['id']]);
        }
    }

    public function down()
    {
        echo "m160314_024303_che cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
