<?php

use yii\db\Schema;
use yii\db\Migration;

class m160412_232257_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_in_requests ADD transaction_id int NULL;');
        $this->execute('ALTER TABLE galaxysss_2.nw_in_requests ADD is_paid tinyint(1) NULL;');
    }

    public function down()
    {
        echo "m160412_232257_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
