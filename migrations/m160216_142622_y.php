<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_142622_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users CHANGE human_design human_design varchar(4000);');
    }

    public function down()
    {
        echo "m160216_142622_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
