<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_025221_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions CHANGE summa summa DOUBLE;');
        $this->execute('ALTER TABLE galaxysss_2.nw_wallets CHANGE value value DOUBLE;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions DROP from_before;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions DROP from_after;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions DROP to_before;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions DROP to_after;');
    }

    public function down()
    {
        echo "m160410_025221_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
