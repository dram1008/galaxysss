<?php

use yii\db\Schema;
use yii\db\Migration;

class m160409_234332_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions ADD from_before DECIMAL(19,2) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions ADD from_after DECIMAL(19,2) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions ADD to_before DECIMAL(19,2) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions ADD to_after DECIMAL(19,2) NULL;');
    }

    public function down()
    {
        echo "m160409_234332_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
