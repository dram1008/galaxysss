<?php

use yii\db\Schema;
use yii\db\Migration;

class m160317_194737_n extends Migration
{
    public function up()
    {
        $this->update('gs_users', ['is_show_shop_help_window' => 1]);
        $this->execute('ALTER TABLE galaxysss_2.gs_users MODIFY is_show_shop_help_window tinyint DEFAULT 1;');
        $this->execute('ALTER TABLE galaxysss_2.gs_users MODIFY COLUMN is_show_shop_help_window tinyint NOT NULL;');
    }

    public function down()
    {
        echo "m160317_194737_n cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
