<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_215350_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_help ADD image VARCHAR(255) NULL;');
    }

    public function down()
    {
        echo "m160329_215350_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
