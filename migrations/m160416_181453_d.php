<?php

use yii\db\Schema;
use yii\db\Migration;

class m160416_181453_d extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_wallets ADD member_of_transaction int NULL;');
    }

    public function down()
    {
        echo "m160416_181453_d cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
