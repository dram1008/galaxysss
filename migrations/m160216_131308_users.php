<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_131308_users extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users ADD birth_lat DOUBLE NULL;');
        $this->execute('ALTER TABLE gs_users ADD birth_lng DOUBLE NULL;');
    }

    public function down()
    {
        echo "m160216_131308_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
