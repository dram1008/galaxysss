<?php

use yii\db\Schema;
use yii\db\Migration;

class m160321_081550_users extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.widget_uploader_many ADD datetime DATETIME NULL;');
    }

    public function down()
    {
        echo "m160321_081550_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
