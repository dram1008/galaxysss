<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_003902_y extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_human_desing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) DEFAULT NULL,
  `ten` varchar(100) DEFAULT NULL,
  `dar` varchar(100) DEFAULT NULL,
  `siddhi` varchar(100) DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `image` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160208_003902_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
