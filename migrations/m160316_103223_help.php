<?php

use yii\db\Schema;
use yii\db\Migration;

class m160316_103223_help extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_help` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` longtext,
  `date_insert` int DEFAULT NULL,
  `tree_node_id` int DEFAULT NULL,
  `sort_index` int DEFAULT NULL,
  `id_string` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Помощь\'');
        $this->execute('CREATE TABLE `gs_help_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `sort_index` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT=\'Дерево для помощи\'');
    }

    public function down()
    {
        echo "m160316_103223_help cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
