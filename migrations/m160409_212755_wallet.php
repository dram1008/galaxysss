<?php

use yii\db\Schema;
use yii\db\Migration;

class m160409_212755_wallet extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `nw_wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `nw_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(20) DEFAULT NULL,
  `to` int(20) DEFAULT NULL,
  `summa` decimal(19,2) DEFAULT NULL,
  `datetime` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160409_212755_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
