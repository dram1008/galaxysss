<?php

use yii\db\Schema;
use yii\db\Migration;

class m160313_225714_r extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_users MODIFY mode_chenneling tinyint DEFAULT 1;');
        $this->execute('ALTER TABLE galaxysss_2.gs_users ADD mode_chenneling_is_subscribe TINYINT(1) NULL;');
        $this->update('gs_users', ['mode_chenneling' => 1]);
    }

    public function down()
    {
        echo "m160313_225714_r cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
