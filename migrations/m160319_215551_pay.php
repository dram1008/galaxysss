<?php

use yii\db\Schema;
use yii\db\Migration;

class m160319_215551_pay extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_users MODIFY COLUMN is_show_shop_help_window tinyint NULL;');
    }

    public function down()
    {
        echo "m160319_215551_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
