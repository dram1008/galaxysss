<?php

use yii\db\Schema;
use yii\db\Migration;

class m160319_130540_pay extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.bog_shop_payments ADD fathersname VARCHAR(100) NULL;');

    }

    public function down()
    {
        echo "m160319_130540_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
