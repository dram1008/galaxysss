<?php

use yii\db\Schema;
use yii\db\Migration;

class m160410_100734_wallet extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.nw_operations CHANGE id id bigint;');
        $this->execute('ALTER TABLE galaxysss_2.nw_transactions CHANGE id id bigint;');
    }

    public function down()
    {
        echo "m160410_100734_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
