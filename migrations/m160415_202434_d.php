<?php

use yii\db\Schema;
use yii\db\Migration;

class m160415_202434_d extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_unions_shop_product ADD attached_files int NULL;');
    }

    public function down()
    {
        echo "m160415_202434_d cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
