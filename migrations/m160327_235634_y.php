<?php

use yii\db\Schema;
use yii\db\Migration;

class m160327_235634_y extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_cheneling_list_author_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160327_235634_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
