<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_144450_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD content_ten TEXT NULL;');
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD content_dar TEXT NULL;');
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD content_siddhi TEXT NULL;');
    }

    public function down()
    {
        echo "m160216_144450_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
