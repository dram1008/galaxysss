<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_001809_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users_shop_requests ADD price INT NULL;');
    }

    public function down()
    {
        echo "m160118_001809_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
