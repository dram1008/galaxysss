<?php

use yii\db\Schema;
use yii\db\Migration;

class m160307_140726_u extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users ADD is_accept_nmp tinyint(1) NULL;');

    }

    public function down()
    {
        echo "m160307_140726_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
