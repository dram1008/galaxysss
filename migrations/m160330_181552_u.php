<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_181552_u extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `gs_sert_red` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identy` varchar(255) DEFAULT NULL,
  `reason` varchar(2000) DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  `content` text,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m160330_181552_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
