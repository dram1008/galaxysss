<?php

use yii\db\Schema;
use yii\db\Migration;

class m160321_074910_files extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `widget_uploader_many_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB');
        $this->execute('CREATE TABLE `widget_uploader_many` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int DEFAULT NULL,
  `field_id` int DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB');
    }

    public function down()
    {
        echo "m160321_074910_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
