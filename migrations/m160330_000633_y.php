<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_000633_y extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_video ADD content TEXT NULL;');
        $this->execute('ALTER TABLE galaxysss_2.gs_video ADD content_video VARCHAR(2000) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.gs_video ADD description VARCHAR(2000) NULL;');
        $this->execute('ALTER TABLE galaxysss_2.gs_video ADD image VARCHAR(255) NULL;');
    }

    public function down()
    {
        echo "m160330_000633_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
