<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_142837_shop extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_unions_shop CHANGE rtkvizir rekvizit VARCHAR(2000);');
    }

    public function down()
    {
        echo "m160127_142837_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
