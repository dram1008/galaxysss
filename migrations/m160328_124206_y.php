<?php

use yii\db\Schema;
use yii\db\Migration;

class m160328_124206_y extends Migration
{
    public function up()
    {
        $a = \app\models\ChennelingAuthor::query()->select('old_id')->column();
        $this->delete('gs_cheneling_tree', ['in','id', $a]);
    }

    public function down()
    {
        echo "m160328_124206_y cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
