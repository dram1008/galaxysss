<?php

use yii\db\Schema;
use yii\db\Migration;

class m160317_194525_n extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_users ADD is_show_shop_help_window TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m160317_194525_n cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
