<?php

use yii\db\Schema;
use yii\db\Migration;

class m160219_002934_hd extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD amin VARCHAR(255) NULL;');
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD patern VARCHAR(255) NULL;');
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD fiz VARCHAR(255) NULL;');

    }

    public function down()
    {
        echo "m160219_002934_hd cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
