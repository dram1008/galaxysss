<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_094748_request extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_users_shop_requests ADD phone VARCHAR(20) NULL;');
    }

    public function down()
    {
        echo "m160113_094748_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
