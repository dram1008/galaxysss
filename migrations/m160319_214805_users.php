<?php

use yii\db\Schema;
use yii\db\Migration;

class m160319_214805_users extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_users ADD subscribe_is_god TINYINT(1) NULL;');
    }

    public function down()
    {
        echo "m160319_214805_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
