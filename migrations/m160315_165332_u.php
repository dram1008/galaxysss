<?php

use yii\db\Schema;
use yii\db\Migration;

class m160315_165332_u extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_cheneling_list MODIFY COLUMN img varchar(255) NULL;');
    }

    public function down()
    {
        echo "m160315_165332_u cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
