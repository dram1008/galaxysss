<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_235229_hd extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE gs_hd_gen_keys ADD codon VARCHAR(255) NULL;');
        $this->execute('ALTER TABLE gs_hd_gen_keys CHANGE content content longtext;');
    }

    public function down()
    {
        echo "m160218_235229_hd cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
