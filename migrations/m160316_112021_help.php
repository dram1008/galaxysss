<?php

use yii\db\Schema;
use yii\db\Migration;

class m160316_112021_help extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE galaxysss_2.gs_help DROP id_string;');
    }

    public function down()
    {
        echo "m160316_112021_help cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
