<?php

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;


?>
<?php if (Yii::$app->request->cookies->getValue('isCloseFbLike', 0) == 0) { ?>
    <?php
    $this->registerJs(<<<JS
    $('#buttonFbLikeClose').click(function() {
        function setCookie2 (name, value) {
            var date = new Date();
            var now = new Date();
            var time = now.getTime();
            var expireTime = time + 1000 * 36000;
            now.setTime(expireTime);
            document.cookie = name + "=" + value +
            "; expires=" + now.toGMTString() +
            "; path=/";
        }
        setCookie2('isCloseFbLike', 1);
        $(this).parent().parent().remove();
    });
JS
    );
    ?>
    <div class="panel panel-default" style="margin-top: 20px;">
        <div class="panel-heading">Если вы хотите получать новости на Facebook, нажмите «нравится»<button type="button" class="close" aria-label="Close" id="buttonFbLikeClose"><span aria-hidden="true">×</span></button></div>
        <div class="panel-body">
            <div class="fb-like" data-href="https://www.facebook.com/gsss.merkaba/" data-layout="standard" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
    </div>
<?php } ?>