<?php


use yii\helpers\Url;


function liMenu($route, $name = null, $options = [])
{
    if (is_array($route)) {
        $arr = [];
        foreach ($route as $route1 => $name1) {
            $arr[] = liMenu($route1, $name1);
        }
        return join("\n", $arr);
    }
    if (Yii::$app->requestedRoute == $route) {
        $options['class'] = 'active';
    }

    return \yii\helpers\Html::tag('li', \cs\helpers\Html::a($name, [$route]), $options);
}

?>

<li><a href="/mission">О нас</a></li>
<li class="dropdown<?php if (Yii::$app->controller->id == 'direction' or in_array(Yii::$app->requestedRoute, [
        'page/language',
        'page/energy',
        'page/time',
        'page/house',
        'page/study',
        'page/forgive',
        'page/money',
        'page/medical',
        'page/food',
        'page/idea',
        'page/tv',
        'page/clothes',
        'page/arts',
        'page/music',
    ])) {
    echo ' active';
} ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">Сферы жизни <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">

        <?= liMenu('direction/index', 'Все'); ?>

        <li class="divider"></li>

        <?= liMenu([
            'page/language' => 'Язык',
            'page/energy'   => 'Энергия',
            'page/time'     => 'Время',
            'page/house'    => 'Пространство',
            'page/study'    => 'Обучение',
            'page/forgive'  => 'Прощающая система',
            'page/money'    => 'Деньги',
            'page/medical'  => 'Здоровье',
            'page/food'     => 'Питание',
        ]);
        ?>
        <li class="divider"></li>

        <?= liMenu([
            'page/idea'    => 'Идеологические группы',
            'page/tv'      => 'ТВ',
            'page/clothes' => 'Одежда',
            'page/arts'    => 'Художники',
            'page/music'   => 'Музыка',
        ]);
        ?>
        <li class="divider"></li>

        <?= liMenu('direction/semya', 'Семья'); ?>

    </ul>
</li>
<li class="dropdown<?php if (Yii::$app->controller->id == 'new_earth' or Yii::$app->requestedRoute == 'human_design/index') {
    echo ' active';
} ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">Земля 5D <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">

        <?= liMenu('new_earth/index', 'Введение'); ?>
        <li class="divider"></li>
        <?= liMenu([
            'new_earth/nmp'                => 'Новый Порядок Мира',
            'new_earth/noosfera'           => 'Ноосферный Контакт',
            'human_design/index'           => 'Дизайн Человека',
            'new_earth/declaration'        => 'Декларация',
            'new_earth/manifest'           => 'Манифест',
            'new_earth/codex'              => 'Кодекс',
            'new_earth/flag'               => 'Флаг',
            'new_earth/conditions'         => 'Соглашение о наблюдении',
            'new_earth/residence'          => 'Резиденция',
            'new_earth/chakri'             => 'Карта чакр',
            'new_earth/hymn'               => 'Гимн',
            'new_earth/history'            => 'История Человечества',
            'new_earth/kon'                => 'Каноны',
            'new_earth/service_cert'       => 'Агентство Сертификации',
            'new_earth/service_vozdayanie' => 'Агентство Воздаяния',
            'new_earth/price'              => 'Смета'
        ]);
        ?>

    </ul>
</li>

<li<?php if ((Url::to(['page/chenneling']) == Url::current()) or (Yii::$app->controller->action->id == 'chenneling_item')) {
    echo ' class="active"';
} ?>><a href="<?= Url::to(['page/chenneling']) ?>">Послания</a></li>
<li<?php if (Yii::$app->controller->id == 'news') {
    echo ' class="active"';
} ?>><a href="<?= Url::to(['news/index']) ?>">Новости</a></li>

<?php if (!\Yii::$app->user->isGuest) { ?>
    <?php
    $c = \app\services\SiteUpdateItemsCounter::getValue();
    $this->registerJs("$('#linkUpdates').tooltip({placement:'right'})");
    if ($c > 0) {
        $class = 'danger';
    } else {
        $class = 'default';
    }
    ?>
    <li>
        <a href="<?= Url::to(['site/site_update']) ?>">
            <span title="Обновления" id="linkUpdates" class="label label-<?= $class ?>">
                <?= $c ?>
            </span>
        </a>
    </li>
<?php } ?>


<li<?php if ((Url::to(['page/services']) == Url::current()) or (Yii::$app->controller->action->id == 'services_item')) {
    echo ' class="active"';
} ?>><a href="<?= Url::to(['page/services']) ?>">Услуги</a></li>
<li<?php if ((Url::to(['calendar/events']) == Url::current()) or (Yii::$app->controller->action->id == 'events_item')) {
    echo ' class="active"';
} ?>><a href="<?= Url::to(['calendar/events']) ?>">События</a></li>
<li<?php if (Yii::$app->controller->id == 'blog') {
    echo ' class="active"';
} ?>><a href="<?= Url::to(['blog/index']) ?>">Блог</a></li>

<?php if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isShop', 0) == 1) { ?>
    <li><a href="<?= Url::to(['shop/index']) ?>">Магазин</a></li>
<?php } ?>
