<?php

/** @var $url */
/** @var $image */
/** @var $title */
/** @var $summary */
/** @var $description */
/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;

?>
<?= $this->render('shareMeta', [
    'image'       => $image,
    'url'         => $url,
    'title'       => $title,
    'description' => $description,
]) ?>
<?= $this->render('shareButton', [
    'image'       => $image,
    'url'         => $url,
    'title'       => $title,
    'description' => $description,
]) ?>