<?php

/** @var $item array article */
/** @var $category string */

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;

$item = \app\models\Service::find(11);
?>

<div class="thumbnail">
    <a href="/category/medical/344/shop/product/1">
        <img alt="Получить Акселератор Мозга в подарок"
             src="<?= $item->getImage() ?>"
             style="width: 100%; display: block;">
    </a>

    <div class="caption">
        <h3><?= $item->getName() ?></h3>

        <p><?= $item->getDescription() ?></p>
    </div>
</div>
