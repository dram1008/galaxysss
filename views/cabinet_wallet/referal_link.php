<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Кошелек';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>
        <div class="col-lg-8">
            <p>Ваша реферальная ссылка:</p>
            <?php
            $id = Yii::$app->user->id;
            $idLength = 16;
            $name = $id;
            if (is_numeric($id)) {
                $name = str_repeat('0', $idLength - strlen($id)) . $id;
            }
            $s = [];
            $s[] = substr($name, 0, 4);
            $s[] = substr($name, 0+4, 4);
            $s[] = substr($name, 0+4+4, 4);
            $s[] = substr($name, 0+4+4+4, 4);

            ?>
            <div class="form-group">
                <input class="form-control" value="<?= 'https://www.galaxysss.com/reg/'.join('-',$s) ?>">
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <?= $form->field($model, 'mails')->textarea(['rows' => 10])->label('Список адресов')->hint('Укажите адреса по одному на одной строке, или через запятую или через точку с запятой') ?>

            <hr>
            <div class="form-group">
                <?= Html::submitButton('Разослать', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>


            <div class="form-group">
<!--                <button class="btn btn-success btn-lg" style="width: 100%">Пригласить друзей</button>-->
            </div>

        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>
</div>
