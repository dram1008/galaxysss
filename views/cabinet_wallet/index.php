<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Кошелек';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>
        <div class="col-lg-8">
            <?php
            /** @var \app\models\Piramida\Wallet $wallet */
            $wallet = Yii::$app->user->identity->getWallet();
            $sort = new \yii\data\Sort([
                'attributes' => [
                    'datetime' => [
                        'label'   => 'Время',
                        'default' => SORT_DESC,
                    ],
                    'before'             => [
                        'label' => 'До',
                    ],
                    'after'             => [
                        'label' => 'После',
                    ],
                    'summa'             => [
                        'label' => 'Сумма',
                    ],
                ]
            ]);
            ?>
            <p>В вашем кошельке:</p>
            <p class="alert alert-success" style="font-weight: bold;font-size: 300%; font-family: Impact;">
                <?= Yii::$app->formatter->asDecimal($wallet->value) ?> $
                </p>
            <p><a href="/cabinet/profile/wallet/add" class="btn btn-info">Купить</a></p>
            <?php \yii\widgets\Pjax::begin(); ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \app\models\Piramida\Operation::find()
                        ->select([
                            'nw_operations.*',
                            'nw_transactions.comment',
                        ])
                        ->where(['nw_operations.wallet_id' => $wallet->id])
                        ->innerJoin('nw_transactions', 'nw_transactions.id = nw_operations.transaction_id')
                        ->orderBy(array_merge($sort->orders, ['datetime' => SORT_DESC]))->asArray()
                    ,
                    'pagination' => [
                        'pageSize' => 50,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $class = ['rowTable'];
                    if (\yii\helpers\ArrayHelper::getValue($item, 'type', 0) == 1) $class[] = 'success';

                    return [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => join(' ', $class),
                    ];
                },
                'columns' => [
                    [
                        'header'  => $sort->link('datetime'),
                        'content' => function ($item) {
                            $v = (int)\yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);

                            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                        }
                    ],
                    [
                        'header'  => $sort->link('before'),
                        'headerOptions'  => [
                            'style' => 'text-align:right;'
                        ],
                        'contentOptions' => [
                            'style' => 'text-align:right;'
                        ],
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'before', 0);

                            return Yii::$app->formatter->asDecimal($v);
                        }
                    ],
                    [
                        'header'  => $sort->link('after'),
                        'headerOptions'  => [
                            'style' => 'text-align:right;'
                        ],
                        'contentOptions' => [
                            'style' => 'text-align:right;'
                        ],
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'after', 0);

                            return Yii::$app->formatter->asDecimal($v);
                        }
                    ],
                    [
                        'header'  => $sort->link('summa'),
                        'headerOptions'  => [
                            'style' => 'text-align:right;'
                        ],
                        'contentOptions' => [
                            'style' => 'text-align:right;'
                        ],
                        'content' => function ($item) {
                            $type = \yii\helpers\ArrayHelper::getValue($item, 'type', 0);
                            $type = ($type > 0)? '+' : '-';
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'summa', 0);
                            $string = $type . ' ' . Yii::$app->formatter->asDecimal($v);
                            if ($type == '+') {
                                $string = Html::tag('abbr', $string, []);
                            }

                            return $string;
                        }
                    ],
                    'comment:text:Комментарий'
                ]
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>
</div>
