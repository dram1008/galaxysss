<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 16.04.2016
 * Time: 20:21
 */
/** $rows = [
 *      [
 *          'id' =>
 *          'value' =>
 *          'parent_id' =>
 *      ],
 * ] */
?>


<?php foreach ($rows as $row) { ?>
    <?php $user = \app\services\UsersInCache::find($row['id']) ?>
    <div class="media">
        <div class="media-left">
            <a href="/user/<?= $user['id'] ?>">
                <img
                    class="media-object"
                    src="<?= $user['avatar'] ?>"
                    style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
                    >
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= (trim($user['name']))? $user['name'] : $user['email'] ?></h4>
            <?php
            // пришло ему как процент с системы
            $all = \app\models\Piramida\Transaction::find()->where([
                'and',
                ['to' => $row['id']],
                ['not', ['from' => null]],
            ])->select('sum(summa)')->scalar();
            if (is_null($all)) $all = 0;
            // пришло мне от него
            $in = \app\models\Piramida\Transaction::find()->where([
                'to'   => Yii::$app->user->id,
                'from' => $row['id'],
            ])->select('sum(summa)')->scalar();
            if (is_null($in)) $in = 0;
            ?>
            <span class="label label-success">+ <?= Yii::$app->formatter->asDecimal($in, 0) ?> р.</span> |
            <span class="label label-info">+ <?= Yii::$app->formatter->asDecimal($all, 0) ?> р.</span>

            <?= $this->render('_tree', ['rows' => $row['childs']]); ?>
        </div>
    </div>
<?php } ?>
