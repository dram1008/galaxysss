<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Кошелек';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>

        <div class="col-lg-8">
            <div id="step1">
                <?php
                /** @var \app\models\Piramida\Wallet $wallet */
                $wallet = Yii::$app->user->identity->getWallet();
                ?>
                <p>Сколько:</p>
                <div class="form-group">
                    <input class="form-control" value="" id="inputValue"/>
                    <p class="hide errorComment error"></p>
                </div>
                <?php
                $this->registerJs(<<<JS
                $('.buttonAdd').click(function() {
                    if ($('#inputValue').val() == '') {
                        $('#inputValue').parent().addClass('has-error');
                        $('.errorComment').removeClass('hide').html('Это поле обязательно надо заполнить!').show();
                        return;
                    }
                    ajaxJson({
                        url: '/cabinet/profile/wallet/addAjax',
                        data: {
                            value: $('#inputValue').val()
                        },
                        success: function(ret) {
                            $('#formPayLabel').val('inRequest.' + ret);
                            $('#productPriceForm').val($('#inputValue').val());
                            $('#formPay').submit();
                        }
                    });
                });
                $('#inputValue').on('focus', function() {
                    $('#inputValue').parent().removeClass('has-error');
                    $('.errorComment').hide();
                });
JS
                );
                ?>
                <button class="btn btn-success buttonAdd">Пополнить</button>
            </div>
            <div id="step2" style="display: none;">
                <h2 class="text-center">Идет перенаправление на оплату...</h2>

                <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" name="form2" id="formPay">
                    <input type="hidden" name="receiver" value="410011473018906">
                    <input type="hidden" name="formcomment" value="<?= Yii::$app->user->identity->getName2() ?>">
                    <input type="hidden" name="short-dest" value="Пополнение кошелька #<?= $wallet->id ?>">
                    <input type="hidden" name="label" value="inRequest." id="formPayLabel">
                    <input type="hidden" name="quickpay-form" value="donate">
                    <input type="hidden" name="targets" value="Пополнение кошелька #<?= $wallet->id ?>">
                    <input type="hidden" name="sum" value="" data-type="number"
                           id="productPriceForm">
                    <input type="hidden" name="comment" value="">
                    <input type="hidden" name="need-fio" value="false">
                    <input type="hidden" name="need-email" value="false">
                    <input type="hidden" name="need-phone" value="false">
                    <input type="hidden" name="need-address" value="false">
                    <input type="hidden" name="paymentType" value="<?= Yii::$app->request->get('type', 'AC') ?>" id="paymentType">

                    <div class="row">
                        <div class="col-lg-12" style="margin-top: 30px;">
                            <div class="btn-group" role="group" aria-label="...">
                                <input type="button" value="Перейти к оплате"
                                       class="btn btn-success btn-lg buttonSubmit" style="width: 400px; border: 2px solid white; border-radius: 20px;">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-4">
            <?= $this->render('../cabinet/profile_menu/profile_menu') ?>
        </div>
    </div>

</div>
