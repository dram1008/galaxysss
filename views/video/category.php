<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $item \app\models\VideoTreeNode */

$this->title = $item->get('name');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <?php foreach(\app\models\Video::query(['tree_node_id' => $item->getId()])->orderBy(['datetime_insert' => SORT_DESC])->all() as $video) { ?>
        <?php $i = new \app\models\Video($video); ?>
        <div class="col-lg-4">
            <p style="height: 60px;"><a href="<?= $i->getLink() ?>"><?= $i->get('name', '') ?></a></p>
            <p style="font-size: 70%; color: #808080;"><?= \app\services\GsssHtml::dateString($i->get('datetime_insert')) ?></p>
            <a href="<?= $i->getLink() ?>"><img src="<?= $i->getImage() ?>" width="100%" class="thumbnail"/></a>
        </div>
    <?php }?>
</div>
