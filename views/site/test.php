<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Отблагодарить';
?>
<div class="container">
    <?php
        $this->registerJs(<<<JS
    $('.buttonTest').click(function(){
        console.log(ajaxJson({
            url: '/test/ajax',
            data: {
                 maya: '{}',
                 stamp: 1,
                 datetime: '2016-12-01 01:01:01',
                 str: '4 апр 2015 г.'
            },
            success: function(ret) {
                console.log(ret);
            }
        }));
    });
JS
);
    ?>
    <button class="btn btn-success buttonTest">test</button>
</div>
