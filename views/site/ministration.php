<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Приглашаем к служению';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">


        <p>
            Приглашаем к служению всех, кто имеет время и желание помощь воплощению <a href="/newEarth/order">Нового Мирового Порядка</a> на планете
            Земля по следующим направлениям:
        </p>

        <p>
            1. <b>Редактор Инструмента Вознесения</b><br>
            Требования: примерно 1 час в день<br>
            Желательно знание языка разметки HTML на базовом уровне
        </p>

        <p>
            2. <b>Разработчик Инструмента Вознесения</b><br>
            Требования: Знания PHP, Mysql, Git, фреймворк Yii<br>
            Желательно знание фреймворка Bootstrap 3, JavaScript
        </p>

        <p>
            3. <b>Верстальщик</b><br>
            Требования: Знания Html, Bootstrap
        </p>

        <p>
            4. <b>Куратор Служителей</b><br>
            Требования: вести список задач и проверять выполнение задач служителями.
        </p>

        <p>
            После двух месяцев служения высылается <a href="/category/medical/344/shop/product/1">Акселератор Мозга</a>, Печать Творца (Богородицы).
        </p>

        <p>
            Для того чтобы начать служение, нужно написать заявку на странице <a href="/contact">http://www.galaxysss.ru/contact</a>.
        </p>


        <hr>

        <?= $this->render('../blocks/share', [
            'image'       => \yii\helpers\Url::to('/images/site/logo/ico.jpg', true),
            'url'         => \yii\helpers\Url::current([], true),
            'title'       => $this->title,
            'description' => 'Приглашаем к служению всех, кто имеет время и желание помощь воплощению Нового Мирового Порядка на планете Земля по следующим направлениям',
        ]) ?>
    </div>
</div>