<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Записи об экспертизах';

$this->registerJs(<<<JS
$('.buttonDelete').click(function (e) {
        e.preventDefault();
        if (confirm('Подтвердите удаление')) {
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/sertRed/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        $('#newsItem-' + id).remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/admin/sertRed/' + $(this).data('id') + '/edit';
    });

JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <a href="/admin/sertRed/add" class="btn btn-default" style="margin-bottom: 20px;">Добавить</a>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\AgencySertRed::query()->orderBy(['datetime' => SORT_DESC]),
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Автор',
                    'content' => function ($item) {
                        $u = \app\services\UsersInCache::find($item['user_id']);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px; margin-right: 10px;float: left;']);
                        $arr[] = Html::a($u['name'], '/user/'.$u['id']);
                        $arr[] = '<br>';
                        $arr[] = '<small style="color: #888;">'.$u['email'].'</small>';

                        return join('', $arr);
                    }
                ],
                'identy:text:Кто',
                [
                    'header'  => 'Причина',
                    'content' => function ($item) {
                        return GsssHtml::getMiniText($item['reason'], 100);
                    }
                ],
                [
                    'header'  => 'Создан',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
    </div>
</div>