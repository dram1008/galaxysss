<?php
$this->title = 'Агентство Сертификации «Золотой Век Творца»';
?>
<div class="container">
    <h1 class="page-header text-center">Агентство Сертификации «Золотой Век Творца»</h1>
    <p class="lead text-center">Сервис установения превышения качества над стандартом Золотого Века Творца</p>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <p>
                <img src="/images/new_earth/service_cert/11053053_732881036844086_4624601218787680766_o.jpg"  width="100%;" class="thumbnail">
            </p>
            <p>
                Это агентство предоставляет организациям мира 3D-4D планеты Земля сервис установения превышения качества
                над стандартом Золотого Века Творца. Также это агентство может прийти в любую организацию планеты Земля
                для проведения экспертизы на предмет соответствия стандарту. В случае не прохождения экспертизы
                служители агентства вправе запустить программу ускоренного воздаяния и синхронихации деятельности до
                стандарта.
            </p>
            <p>
                Основными признаками знака качества стардарта Золотого Века Творца таковы:<br>
                - продукт экологически чист<br>
                - продукт органического происхождения<br>
                - продукт природного происхождения<br>
                - сделан в ручную<br>
                - вегетарианский (сотворен с уважением ко всем формам жизни)<br>
                - продукт сделан с любовью.
            </p>
            <p>
                Агентсво имеет открытую и доступную всем базу данных проведения экспертиз и списка организаций,
                которые прошли или не прошли экспертизу.
            </p>
            <p>
                Если у вас возникают жалобы или предложения на любую организацию планеты Земля или у вас есть материалы и доказательства не соответствия <a href="/codex" target="_blank">стандртов «Золотого Века»</a>  вы можете обращаться на почту god@galaxysss.ru.
            </p>
            <p>
                Агентство сертификации действует при поддержке «<a href="/newEarth/service/vozdayanie" target="_blank">Агентства Воздаяния Творца</a>».
            </p>

            <p>
                <img src="/images/new_earth/service_cert/vert.jpg" width="100%;">
            </p>

            <span class="label label-danger">Красный список</span>
            <table class="table table-hover" style="margin-top: 20px;">
                <tr>
                    <th>
                        Кто
                    </th>
                    <th>
                        Причина не соответствия
                    </th>
                    <th>
                        Дата проведения экспертизы
                    </th>
                </tr>
                <?php foreach(\app\models\AgencySertRed::query()->orderBy(['datetime' => SORT_DESC])->all() as $item) { ?>
                    <tr>
                        <td>
                            <?= $item['identy'] ?>
                        </td>
                        <td>
                            <?= $item['reason'] ?>
                        </td>
                        <td>
                            <?= Yii::$app->formatter->asDate($item['datetime']) ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <span class="label label-success">Зеленый список</span>
            <table class="table table-hover" style="margin-top: 20px;">
                <tr>
                    <th>
                        Наименование
                    </th>
                    <th>
                        Сайт
                    </th>
                    <th>
                        дата проведения экспертизы
                    </th>
                </tr>
                <?php
                foreach(\app\models\Union::query([ 'moderation_status' => 1])->orderBy(['date_insert' => SORT_ASC])->all() as $union) {
                $u = new \app\models\Union($union);
                    ?>
                <tr>
                    <td>
                        <a href="<?= $u->getLink()?>"><?= $union['name'] ?></a>
                    </td>
                    <td>
                        <a href="<?= $union['link'] ?>" target="_blank">Сайт</a>
                    </td>
                    <td nowrap="nowrap">
                        <?= Yii::$app->formatter->asDate($union['date_insert']) ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
            <hr>


            <?= $this->render('../blocks/share', [
                'image'       => \yii\helpers\Url::to('/images/new_earth/service_cert/11053053_732881036844086_4624601218787680766_o.jpg', true) ,
                'url'         => \yii\helpers\Url::current([], true),
                'title'       => $this->title,
                'description' => 'Это агентство предоставляет организациям мира 3D-4D планеты Земля сервис установения превышения качества над стандартом Золотого Века Творца. Также это агентство может прийти в любую организацию планеты Земля для проведения экспертизы на предмет соответствия стандарту. В случае не прохождения экспертизы служители агентства вправе запустить программу ускоренного воздаяния и синхронихации деятельности до стандарта. Агентство сертификации сотрудничает с агентством Воздаяния.',
            ]) ?>
        </div>
    </div>
</div>