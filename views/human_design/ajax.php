<?php

/** @var $human_design \app\models\HumanDesign */

/**
'human_design'  => $data,
'birth_date'    => $date[2].'-'.$date[1].'-'.$date[0],
'birth_time'    => $time.':00',
'birth_lat'     => $location[0],
'birth_lng'     => $location[1],
 */
$humanDesign = $human_design;
?>


<div class="panel panel-default">
    <div class="panel-heading">Дизайн Человека

        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default btn-xs buttonDelete" >
                Удалить и пересчитать
            </button>
        </div>
    </div>
    <div class="panel-body">
        <img src="data:image/png;base64,<?= base64_encode ($humanDesign->image) ?>" style="width: 100%;">
        <table class="table table-striped table-hover" style="width: auto;" align="center">
            <tr>
                <td>Тип</td>
                <td>
                    <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['type'][$humanDesign->type['href']] ?>">
                        <?= $humanDesign->type['text'] ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Профиль</td>
                <td>
                    <a href="<?= \app\modules\HumanDesign\calculate\YourHumanDesignRu::$links['profile'][$humanDesign->profile['href']] ?>">
                        <?= $humanDesign->profile['text'] ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Определение</td>
                <td><?= $humanDesign->definition['text'] ?></td>
            </tr>
            <tr>
                <td>Внутренний Авторитет</td>
                <td><?= $humanDesign->inner['text'] ?></td>
            </tr>
            <tr>
                <td>Стратегия</td>
                <td><?= $humanDesign->strategy['text'] ?></td>
            </tr>
            <tr>
                <td>Тема ложного Я</td>
                <td><?= $humanDesign->theme['text'] ?></td>
            </tr>
            <tr>
                <td>Подпись</td>
                <td><?= (is_null($humanDesign->signature))? '' : $humanDesign->signature['text'] ?></td>
            </tr>
            <tr>
                <td>Инкарнационный крест</td>
                <?php
                $text = $humanDesign->cross['text'];
                $arr = explode('(', $text);
                $text = trim($arr[0]);
                $arr = explode(')', $arr[1]);
                $arr = explode('|', $arr[0]);
                $new = [];
                foreach($arr as $i) {
                    $a = explode('/', $i);
                    $new[] = trim($a[0]);
                    $new[] = trim($a[1]);
                }
                ?>
                <td><?= $text ?> (<a href="<?= \yii\helpers\Url::to(['cabinet/profile_human_design_gen_keys_item', 'id' => $new[0]])?>"><?= $new[0] ?></a>/<a href="<?= \yii\helpers\Url::to(['cabinet/profile_human_design_gen_keys_item', 'id' => $new[1]])?>"><?= $new[1] ?></a> | <a href="<?= \yii\helpers\Url::to(['cabinet/profile_human_design_gen_keys_item', 'id' => $new[2]])?>"><?= $new[2] ?></a>/<a href="<?= \yii\helpers\Url::to(['cabinet/profile_human_design_gen_keys_item', 'id' => $new[3]])?>"><?= $new[3] ?></a>)</td>
            </tr>
        </table>
        <h3>Генные ключи</h3>
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    #
                </td>
                <td>
                    Название
                </td>
                <td>
                    Тень
                </td>
                <td>
                    Дар
                </td>
                <td>
                    Сиддхи
                </td>
            </tr>

            <?php foreach($new as $i) {
                $genKey = \app\models\HdGenKeys::find(['num' => $i])
                ?>
                <tr class="rowTable" data-num="<?= $genKey->getField('num') ?>" role="button">
                    <td>
                        <?= $genKey->getField('num') ?>
                    </td>
                    <td>
                        <?= $genKey->getField('name') ?>
                    </td>
                    <td>
                        <?= $genKey->getField('ten') ?>
                    </td>
                    <td>
                        <?= $genKey->getField('dar') ?>
                    </td>
                    <td>
                        <?= $genKey->getField('siddhi') ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>