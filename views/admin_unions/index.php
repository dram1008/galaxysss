<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Все объединения';

$this->registerJs(<<<JS
        $('.buttonDelete').click(function (e) {
        if (confirm('Подтвердите удаление')) {
            e.preventDefault();
            e.stopPropagation();
            var id = $(this).data('id');
            var a = $(this).parent().parent();
            ajaxJson({
                url: '/admin/unions/' + id + '/delete',
                success: function (ret) {
                    a.remove();
                    infoWindow('Успешно', function() {

                    });
                }
            });
        }
    });

    // Отправить на модерацию
    $('.buttonSendModeration').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите свой выбор')) {
            var button = $(this);
            var id = $(this).data('id');
            var a = $(this).parent().parent();
            ajaxJson({
                url: '/admin/unions/' + id + '/sendModeration',
                success: function (ret) {
                    a.remove();
                    infoWindow('Успешно', function() {
                    });
                }
            });
        }
    });

    // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/unions/' + id + '/subscribe',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/admin/unions/' + $(this).data('id') + '/edit';
    });
JS
);

$searchModel = new \app\models\Form\UnionSearch();
$dataProvider = $searchModel->search(Yii::$app->request->get(), null);


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">Все объединения</h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
        ]) ?>
        <hr>

        <a href="/objects/add" class="btn btn-default" style="margin-bottom: 20px;">Добавить</a>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
                if ($item['moderation_status'] != 1) {
                    $data['style'] = 'opacity: 0.5';
                }
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => Html::cssStyleFromArray(['margin-bottom' => '0px']),
                        ]);
                    }
                ],
                'name:text:Название',
                [
                    'header'  => 'Автор',
                    'content' => function ($item) {
                        $u = \app\services\UsersInCache::find($item['user_id']);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px; margin-right: 10px;float: left;']);
                        $arr[] = Html::a($u['name'], '/user/'.$u['id']);
                        $arr[] = '<br>';
                        $arr[] = '<small style="color: #888;">'.$u['email'].'</small>';

                        return join('', $arr);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return \yii\helpers\Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Статус',
                    'content' => function ($item) {
                        switch ($item['moderation_status']) {
                            case 0: // отклонено
                                $class = 'glyphicon glyphicon-remove';
                                break;
                            case 1: // одобрено
                                $class = 'glyphicon glyphicon-ok';
                                break;
                            case 2: // оправлено на утверждение модератором
                                $class = 'glyphicon glyphicon-eye-open';
                                break;
                            default: // на стадии редактрования
                                $class = 'glyphicon glyphicon-time';
                                break;
                        }
                        return Html::tag('span', Html::tag('span',null,['class' => $class]), ['label label-default']);
                    }
                ],
                [
                    'header'  => 'Магазин',
                    'content' => function ($item) {
                        if (ArrayHelper::getValue($item, 'is_shop', 0) != 1) {
                            return '';
                        }
                        $arr = [];
                        $arr[] = Html::a('Магазин', ['admin_unions_shop/index', 'id' => $item['id']], ['class' => 'btn btn-default btn-xs', 'style' => Html::cssStyleFromArray(['margin-bottom' => '5px'])]);
//                        $arr[] = Html::a('Заказы', ['cabinet_shop_shop/request_list', 'id' => $item['id']], ['class' => 'btn btn-default btn-xs', 'style' => Html::cssStyleFromArray(['margin-bottom' => '5px'])]);
//                        $arr[] = Html::a('Товары', ['cabinet_shop_shop/product_list', 'id' => $item['id']], ['class' => 'btn btn-default btn-xs', 'style' => Html::cssStyleFromArray(['margin-bottom' => '5px'])]);

                        return join("<br>",$arr);
                    }
                ],
                [
                    'header'  => 'Рассылка',
                    'content' => function ($item) {
                        if (ArrayHelper::getValue($item, 'is_added_site_update', 0) == 1) {
                            return '';
                        }

                        return Html::button('Рассылка', [
                            'class' => 'btn btn-success btn-xs buttonAddSiteUpdate',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ]
            ]
        ]) ?>
    </div>
</div>