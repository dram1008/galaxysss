<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;
?>
<div class="list-group">
    <a href="<?= Url::to(['cabinet/profile']) ?>"
       class="list-group-item<?php if ('cabinet/profile' == $requestedRoute) {
           echo ' active';
       } ?>"> Профиль </a>
    <a href="<?= Url::to(['cabinet/profile_subscribe']) ?>"
       class="list-group-item<?php if ('cabinet/profile_subscribe' == $requestedRoute) {
           echo ' active';
       } ?>"> Рассылки </a>
    <a href="<?= Url::to(['cabinet/profile_human_design']) ?>"
       class="list-group-item<?php if ('cabinet/profile_human_design' == $requestedRoute) {
           echo ' active';
       } ?>"> Дизайн Человека </a>
    <a href="<?= Url::to(['cabinet/profile_zvezdnoe']) ?>"
       class="list-group-item<?php if ('cabinet/profile_zvezdnoe' == $requestedRoute) {
           echo ' active';
       } ?>"> Звездное происхождение </a>
    <a href="<?= Url::to(['cabinet/profile_time']) ?>"
       class="list-group-item<?php if ('cabinet/profile_time' == $requestedRoute) {
           echo ' active';
       } ?>"> Персональная Галактическая Печать</a>
    <a href="<?= Url::to(['cabinet/profile_possibilities']) ?>"
       class="list-group-item<?php if ('cabinet/profile_possibilities' == $requestedRoute) {
           echo ' active';
       } ?>"> Возможности профиля</a>
    <a href="<?= Url::to(['cabinet_wallet/index']) ?>"
       class="list-group-item<?php if ('cabinet_wallet/index' == $requestedRoute) {
           echo ' active';
       } ?>"> Кошелек</a>
    <a href="<?= Url::to(['cabinet_wallet/referal_link']) ?>"
       class="list-group-item<?php if ('cabinet_wallet/referal_link' == $requestedRoute) {
           echo ' active';
       } ?>"> Реферальная ссылка</a>
</div>
