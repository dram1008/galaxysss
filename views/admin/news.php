<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Все новости';

$this->registerJs(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        if (confirm('Подтвердите удаление')) {
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/news/' + id + '/delete',
                success: function (ret) {
                    showInfo('Успешно', function() {
                        $('#newsItem-' + id).remove();
                    });
                }
            });
        }
    });

    // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        if (confirm('Подтвердите')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/news/' + id + '/subscribe',
                success: function (ret) {
                    showInfo('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });
JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">Все Новости</h1>

        <div class="btn-group" style="margin-bottom: 20px;">
            <a href="<?= Url::to(['admin/news_add']) ?>" class="btn btn-default">Добавить</a>
        </div>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \app\models\NewsItem::query()->orderBy(['date_insert' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions' => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns' => [
                'id',
                [
                    'header' => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'header:text:Название',
                [
                    'header' => 'Дата',
                    'content' => function ($item) {
                        return Html::tag('span', GsssHtml::dateString($item['date_insert']), ['style' => 'font-size: 80%; margin-bottom:10px; color: #c0c0c0;']);
                    }
                ],
                [
                    'header' => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data' => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header' => 'Рассылка',
                    'content' => function ($item) {
                        if (ArrayHelper::getValue($item, 'is_added_site_update', 0) == 1) {
                            return '';
                        }
                        return Html::button('Рассылка', [
                            'class' => 'btn btn-success btn-xs buttonAddSiteUpdate',
                            'data' => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ]
            ],
        ]) ?>
    </div>
</div>