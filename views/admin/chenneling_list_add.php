<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Добавить ченнелинг';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?php if (!is_null($id = Yii::$app->session->getFlash('contactFormSubmitted'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

        <?php
        $this->registerJs(<<<JS
        // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтведите свое намерение')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/chennelingList/' + id + '/subscribe',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });

JS
        );
        ?>
        <button class="btn btn-info buttonAddSiteUpdate" data-id="<?= $id ?>">Сделать рассылку</button>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'header') ?>
                <?= $model->field($form, 'source') ?>
                <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                <?= $model->field($form, 'content') ?>
                <?= $model->field($form, 'img') ?>
                <?= $model->field($form, 'is_add_image') ?>
                <?= $model->field($form, 'tree_node_id_mask') ?>
                <?= $model->field($form, 'author_id')->dropDownList(
                    ArrayHelper::merge(
                        [null => 'Ничего не выбрано'],
                        \yii\helpers\ArrayHelper::map(\app\models\ChennelingAuthor::query()->select('id,name')->orderBy(['name' => SORT_ASC])->all(),'id', 'name')
                    )
                ) ?>
                <?= $model->field($form, 'power_id')->dropDownList(
                    ArrayHelper::merge(
                        [null => 'Ничего не выбрано'],
                        \yii\helpers\ArrayHelper::map(\app\models\ChennelingPower::query()->select('id,name')->orderBy(['name' => SORT_ASC])->all(),'id', 'name')
                    )
                ) ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
