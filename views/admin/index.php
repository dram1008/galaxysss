<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Модерация';
?>
<div class="container">

    <h1 class="page-header">Административная часть</h1>

    <p>
        <a href="/admin/help">
            Помощь
        </a>
    </p>
    <p>
        <a href="/admin/smeta">
            Смета на Новую Землю
        </a>
    </p>
    <p>
        <a href="/admin/serviceList">
            Услуги
        </a>
    </p>
    <p>
        <a href="/admin/pictures">
            Картинки
        </a>
    </p>
    <p>
        <a href="/admin/unions">
            Объединения
        </a>
    </p>
    <p>
        <a href="/admin/events">
            События
        </a>
    </p>
    <p>
        <a href="/admin/praktice">
            Практики
        </a>
    </p>
    <p>
        <a href="/admin/hdGenKeys">
            Генные ключи
        </a>
    </p>
    <p>
        <a href="/admin/vozdayanie">
            Агентство Воздаяния (картинки)
        </a>
    </p>
    <p>
        <a href="/admin/shop/moneyIncome">
            Магазин. Входящие платежи
        </a>
    </p>
    <p>
        <a href="/admin/channelingAuthor">
            Послания. Автор
        </a>
    </p>
    <p>
        <a href="/admin/channelingPower">
            Послания. Высшие силы
        </a>
    </p>
    <p>
        <a href="/admin/video">
            Видео
        </a>
    </p>
    <p>
        <a href="/admin/sertRed">
            Агентство Сертификации. Красный список
        </a>
    </p>
</div>