<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все послания';

$this->registerJS(<<<JS
    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/chennelingList/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтведите свое намерение')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/chennelingList/' + id + '/subscribe',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/admin/chennelingList/' + $(this).data('id') + '/edit';
    });
JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="btn-group" style="margin-bottom: 20px;" role="group" aria-label="...">
            <a href="<?= Url::to(['admin/chenneling_list_add']) ?>" class="btn btn-default">Добавить</a>
            <a href="<?= Url::to(['admin/chenneling_list_add_from_page']) ?>" type="button" class="btn btn-default">со страницы</a>
        </div>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Chenneling::query()->orderBy(['date_created' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
                if ($item['moderation_status'] != 1) {
                    $data['style'] = 'opacity: 0.5';
                }
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                            'style' => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'header:text:Название',
                [
                    'header'  => 'Автор',
                    'content' => function ($item) {
                        $u = \app\services\UsersInCache::find($item['user_id']);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom: 0px; margin-right: 10px;float: left;']);
                        $arr[] = Html::a($u['name'], '/user/'.$u['id']);
                        $arr[] = '<br>';
                        $arr[] = '<small style="color: #888;">'.$u['email'].'</small>';

                        return join('', $arr);
                    }
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_created', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Статус',
                    'content' => function ($item) {
                        switch ($item['moderation_status']) {
                            case 0: // отклонено
                                $class = 'glyphicon glyphicon-remove';
                                break;
                            case 1: // одобрено
                                $class = 'glyphicon glyphicon-ok';
                                break;
                            case 2: // оправлено на утверждение модератором
                                $class = 'glyphicon glyphicon-eye-open';
                                break;
                            default: // на стадии редактрования
                                $class = 'glyphicon glyphicon-time';
                                break;
                        }
                        return Html::tag('span', Html::tag('span',null,['class' => $class]), ['label label-default']);
                    }
                ],
                [
                    'header'  => 'Рассылка',
                    'content' => function ($item) {
                        if (ArrayHelper::getValue($item, 'is_added_site_update', 0) == 1) {
                            return '';
                        }
                        if ($item['moderation_status'] != 1) {
                            return '';
                        }
                        return Html::button('Рассылка', [
                            'class' => 'btn btn-success btn-xs buttonAddSiteUpdate',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ]
            ],
        ]) ?>

    </div>
</div>