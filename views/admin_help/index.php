<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;

$this->title = 'Помощь';

$this->registerJs(<<<JS
$('.buttonDelete').click(function (e) {
        e.preventDefault();
        if (confirm('Подтвердите удаление')) {
            var b = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/admin/help/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        b.parent().parent().remove();
                    });
                }
            });
        }
    });

    $('.rowTable').click(function() {
        window.location = '/admin/help/' + $(this).data('id') + '/edit';
    });

JS
);
?>

<div class="container">
    <h1 class="page-header">Помощь</h1>

    <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \app\models\Help::query(),
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns' => [
                'id',
                'name',
                [
                    'header' => 'Удалить',
                    'content' => function($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data-id' => $item['id'],
                        ]);
                    }
                ],
            ]
        ])?>



    <div class="col-lg-6">
        <div class="row">
            <!-- Split button -->
            <div class="btn-group">
                <a href="<?= Url::to(['admin_help/add'])?>" class="btn btn-default">Добавить</a>
            </div>
        </div>
    </div>
</div>