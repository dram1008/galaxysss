<?php
/** @var $this yii\web\View */
/** @var \app\models\Event $item
 */

use cs\models\Calendar\Maya;
use app\services\GsssHtml;
use yii\helpers\Url;

$this->title = $item->getField('name');

?>
<style>
    .download-section {
        width: 100%;
        padding: 50px 0;
        color: #fff;
        background: url('<?= \cs\Widget\FileUpload3\FileUpload::getOriginal($item->getField('image')) ?>') no-repeat center center scroll;
        background-color: #000;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
</style>
<section id="contact" class="content-section text-center" style="">
    <div class="download-section" style="
    border-bottom: 1px solid #87aad0;
    border-top: 1px solid #87aad0;
    height: 450px;
    ">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2" style="color: #000000">

            </div>
        </div>
    </div>
</section>
<div class="container" style="padding-top: 0px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>

    <div class="col-lg-12">
        <div class="col-lg-10 col-lg-offset-1">
            <?= $item->getField('content') ?>
            <hr>
            <?= $this->render('../blocks/share', [
                'image'       => ($item->getField('image') != '')? Url::to(\cs\Widget\FileUpload2\FileUpload::getOriginal($item->getField('image')), true) : '' ,
                'url'         => Url::current([], true),
                'title'       => $item->getField('name'),
                'description' => trim(\cs\services\Str::sub(strip_tags($item->getField('content')), 0, 200)),
            ]) ?>
        </div>

    </div>

</div>
