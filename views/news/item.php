<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Str;

/* @var $this yii\web\View */
/* @var $newsItem array */

$item = $newsItem;
$this->title = $newsItem['header'];
$this->registerJs("$('#share').popover()");
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
    </div>

    <div class="col-lg-8" style="padding-bottom: 50px;">
        <p style="color: #ccc; font-family: 'Courier New', Monospace; font-size: 80%"><span class="glyphicon glyphicon-calendar" style=" margin-right: 10px;"></span><?= \app\services\GsssHtml::dateString($item['date_insert']) ?> |
        <span class="glyphicon glyphicon-eye-open" style=" margin-right: 10px;"></span><?= Yii::$app->formatter->asDecimal($item['view_counter'], 0) ?></p>
        <hr>
        <?= $newsItem['content'] ?>
        <hr>
        <?php if (isset($newsItem['source'])): ?>
            <?php if ($newsItem['source'] != ''): ?>
                <?= Html::a('Ссылка на источник »', $newsItem['source'], [
                    'class'  => 'btn btn-primary',
                    'target' => '_blank',
                    'style' => 'margin-bottom: 10px;',
                ]) ?>
            <?php endif; ?>
        <?php endif; ?>
        <?= $this->render('../blocks/share', [
            'image'       => \cs\Widget\FileUpload2\FileUpload::getOriginal(Url::to($item['img'], true), false),
            'url'         => Url::current([], true),
            'title'       => $item['header'],
            'description' => trim(Str::sub(strip_tags($item['content']), 0, 200)),
        ]) ?>
        <?= $this->render('../blocks/yandexMoney2') ?>
        <!--                Комментарии -->
        <?//= \app\modules\Comment\Service::render(\app\modules\Comment\Model::TYPE_NEWS, $item['id']); ?>
        <?= $this->render('../blocks/fbLike') ?>

    </div>
    <div class="col-lg-4">
        <?php if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isMarketing', 0) == 1) { ?>
            <?= $this->render('../blocks/reklama') ?>
        <?php } ?>

        <?php
        foreach ($lastList as $item) {
            ?>
            <div class="thumbnail">
                <a href="<?= \app\services\GsssHtml::getNewsUrl($item) ?>"><img
                        src="<?= $item['img'] ?>"
                        style="width: 100%; display: block;"
                        > </a>

                <div class="caption">
                    <h3>
                        <?= $item['header'] ?>
                    </h3>

                    <p>
                        <?= \app\services\GsssHtml::getMiniText($item['content']); ?>
                    </p>

                </div>
            </div>

        <?php
        }
        ?>

        <?= $this->render('../blocks/reklama/block2') ?>
    </div>

</div>