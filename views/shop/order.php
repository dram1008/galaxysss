<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use app\modules\Shop\services\Basket;

/**
 * Форма заказа
 * предназначена для одного магазина
 *
 * REQUEST:
 * - unionId - int - идентификатор объединения по которому будет выборка товаров для заказа
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Оформление заказа';

$unionId = Yii::$app->request->get('unionId');
if (!$unionId) {
    $unionId = Basket::getUnionIds()[0];
}
$productPrice = Basket::getPrice($unionId);
$union = \app\models\Union::find($unionId);

$shop = $union->getShop();
$dostavkaRows = $shop->getDostavkaRows();

$this->registerJs(<<<JS
    $('.field-order-address').hide();
    function formatAsDecimal (val)
    {
        var pStr = '';
        if (val > 1000) {
            var t = parseInt(val/1000);
            var ost = (val - t * 1000);
            var ostStr = ost;
            if  (ost == 0) {
                ostStr = '000';
            } else {
                if (ost < 10) {
                    ostStr = '00' + ost;
                }
                if (ost < 100) {
                    ostStr = '0' + ost;
                }
            }
            pStr = t + ' ' + ostStr;
        } else {
            pStr = val;
        }

        return pStr;
    }

    $('input[name="Order[dostavka]"]').on('change', function() {
        var newPrice;
        var dostavka_price = $(this).data('price');
        var dostavka_is_need_address = $(this).data('is_need_address');
        newPrice = productPrice + dostavka_price;
        if (dostavka_is_need_address == 1) {
            $('.field-order-address').show();
        } else {
            $('.field-order-address').hide();
        }

        $('#requestPrice').html(formatAsDecimal(newPrice)).data('price', newPrice);
    });
    $('.paymentType').click(function () {
        $('#paymentType').attr('value', $(this).data('value'));
        if (!$(this).hasClass('active')) {
            $('.paymentType').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.buttonSubmit').click(function() {
        // проверка формы
        var dostavka = '';
        var dostavkaObject;
        $('input[name="Order[dostavka]"]').each(function() {
            if ($(this).is(':checked')) {
                dostavkaObject = $(this);
                dostavka = dostavkaObject.attr('value');
            }
        });
        if (dostavka == '') {
            infoWindow('Вам нужно выбрать способ Доставки');
            return false;
        }
        if (dostavkaObject.data('is_need_address') == 1) {
            if ($('#order-address').val() == '') {
                infoWindow('Нужно обязательно заполнить адрес доставки');
                return false;
            }
        }
        ajaxJson({
            url: '/shop/order/ajax',
            data: {
                union_id: {$union->getId()},
                comment: $('#order-comment').val(),
                dostavka: dostavka,
                price: $('#requestPrice').data('price'),
                phone: $('#order-phone').val(),
                address: $('#order-address').val(),
                paymentType: $('#paymentType').val()
            },
            success: function(ret) {
                // Другие способы оплаты
                if ($('#paymentType').val() == 'DI') {
                    window.location = '/cabinet/order/' + ret;
                } else {
                    if ($('#requestPrice').data('price') > 15000) {
                        window.location = '/cabinet/order/' + ret;
                    } else {
                        window.location = '/shop/order/buy/' + ret + '?type=' + $('#paymentType').val();
                    }
                }
            }
        });
    })
JS
);
$this->registerJs(<<<JS
    var productPrice = {$productPrice};
JS
    , \yii\web\View::POS_HEAD);

?>
<div class="container">
    <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


    <div class="row" id="blockLogin"<?php if (!Yii::$app->user->isGuest) echo(' style="display: none;"') ?>>
        <div class="col-lg-12" style="padding-bottom: 30px; font-size: 200%;">
            Вы не авторизованы. Если вы в первый раз на нашем сайте, то зарегистрируйтесь. Или войдите если у вас есть логин.
        </div>
        <div class="col-lg-6">
            Вход
            <!--                    <input class="form-control" type="text" width="100%" name="login" id="formLoginUserName">-->
            <!--                    <input class="form-control" type="password" width="100%" name="password" id="formLoginPassword">-->
            <?php
            $form = \yii\bootstrap\ActiveForm::begin([
                'id'                 => 'login-form',
                'enableClientScript' => false,
            ]);
            $model2 = new \app\models\Auth\Login();
            ?>

            <?= $form->field($model2, 'username', ['inputOptions' => ['placeholder' => 'Логин']])->label('', ['class' => 'hide']) ?>
            <?= $form->field($model2, 'password', ['inputOptions' => ['placeholder' => 'Пароль']])->label('', ['class' => 'hide'])->passwordInput() ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
            <button class="btn btn-default" id="buttonLogin2" style="width: 100%;">Войти</button>

            <?php
            $this->registerJs(<<<JS
                        $('#buttonLogin2').click(function() {
                            if ($('#login-username').val() == '') {
                                $('.field-login-username').addClass('has-error');
                                $('.field-login-username .help-block').show().html('Нужно заполнить логин');

                                return false;
                            }
                            if ($('#login-password').val() == '') {
                                $('.field-login-password').addClass('has-error');
                                $('.field-login-password .help-block').show().html('Нужно заполнить пароль');

                                return false;
                            }
                            ajaxJson({
                                url: '/shop/loginAjax',
                                data: {
                                    login: $('#login-username').val(),
                                    password: $('#login-password').val()
                                },
                                success: function(ret) {
                                    $('#blockLogin').hide();
                                    $('#blockOrder').show();
                                },
                                errorScript: function(ret) {
                                    if (ret.id == 101) {
                                        $('.field-login-username').addClass('has-error');
                                        $('.field-login-username .help-block').show().html(ret.data);
                                    }
                                    if (ret.id == 102) {
                                        $('.field-login-password').addClass('has-error');
                                        $('.field-login-password .help-block').show().html(ret.data);
                                    }
                                }
                            });

                        });
                        $('#login-username').on('focus', function() {
                            $('.field-login-username').removeClass('has-error');
                            $('.field-login-username .help-block').hide();
                        });
                        $('#login-password').on('focus', function() {
                            $('.field-login-password').removeClass('has-error');
                            $('.field-login-password .help-block').hide();
                        });
JS
            );
            ?>
        </div>
        <div class="col-lg-6">
            Регистрация
            <?php
            $form = \yii\bootstrap\ActiveForm::begin([
                'id'                 => 'rigistration-form',
                'enableClientScript' => false,
            ]);
            $model2 = new \app\models\Auth\Regisration();
            ?>

            <?= $form->field($model2, 'name', ['inputOptions' => ['placeholder' => 'Имя (Фамилия)']])->label('', ['class' => 'hide']) ?>
            <?= $form->field($model2, 'username', ['inputOptions' => ['placeholder' => 'Логин/почта']])->label('', ['class' => 'hide']) ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
            <button class="btn btn-default" id="buttonRegister" style="width: 100%;">Войти</button>
            <?php
            $this->registerJs(<<<JS
                        $('#buttonRegister').click(function(){
                            if ($('#regisration-name').val() == '') {
                                $('.field-regisration-name').addClass('has-error');
                                $('.field-regisration-name .help-block').show().html('Нужно заполнить имя');

                                return false;
                            }
                            if ($('#regisration-username').val() == '') {
                                $('.field-regisration-username').addClass('has-error');
                                $('.field-regisration-username .help-block').show().html('Нужно заполнить логин');

                                return false;
                            }

                            ajaxJson({
                                url: '/shop/registrationAjax',
                                data: {
                                    login: $('#regisration-username').val(),
                                    name: $('#regisration-name').val()
                                },
                                success: function(ret) {
                                    $('#blockLogin').hide();
                                    $('#blockOrder').show();
                                },
                                errorScript: function(ret) {
                                    $('.field-regisration-username').addClass('has-error');
                                    $('.field-regisration-username .help-block').show().html(ret.data);
                                }
                            });

                        });
                        $('#regisration-username').on('focus', function() {
                            $('.field-regisration-username').removeClass('has-error');
                            $('.field-regisration-username .help-block').hide();
                        });
                        $('#regisration-name').on('focus', function() {
                            $('.field-regisration-name').removeClass('has-error');
                            $('.field-regisration-name .help-block').hide();
                        });
JS
            );
            ?>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Доставка</h4>
                </div>
                <div class="modal-body">
                    <?php foreach($dostavkaRows as $row) { ?>
                        <p><b><?= $row['name'] ?></b> - <?= $row['description'] ?>. Стоимость = <?= is_null($row['price'])? 0 : Yii::$app->formatter->asDecimal($row['price'],0) ?> руб.</p>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8 col-lg-offset-2" id="blockOrder"<?php if (Yii::$app->user->isGuest) echo(' style="display: none;"') ?>>
        <p>Сумма заказа: <span id="requestPrice" data-price="<?= $productPrice ?>"><?= Yii::$app->formatter->asDecimal($productPrice, 0) ?></span></p>
        <?php $form = ActiveForm::begin([
            'id'      => 'contact-form',
        ]);

        ?>
        <?php
        $this->registerJs(<<<JS
    $('.buttonDostavkaMore').click(function() {
        $('#myModal').modal('show');
    });
JS
);
        ?>
        <?= $form->field($model, 'dostavka', [
            'template' => "{label}\n{input}\n{hint}\n{error}<p><a href='javascript:void(0);' class='buttonDostavkaMore'>Подробнее</a></p>",
        ])->widget('app\modules\Shop\widget\RadioList', ['rows' => $dostavkaRows ])->label('Доставка') ?>
        <?= $form->field($model, 'address')->textarea(['rows' => 5])->label('Адрес доставки') ?>
        <?= $form->field($model, 'comment')->textarea(['rows' => 5])->label('Комментарий к заказу') ?>
        <?= $form->field($model, 'phone')->label('Телефон') ?>


        <?php ActiveForm::end(); ?>
        Оплата:
        <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" name="form2" id="formPay">
            <input type="hidden" name="receiver" value="410011473018906">
            <input type="hidden" name="formcomment" value="Авиалинии «БогДан»">
            <input type="hidden" name="short-dest" value="">
            <input type="hidden" name="label" value="" id="formPayLabel">
            <input type="hidden" name="quickpay-form" value="donate">
            <input type="hidden" name="targets" value="">
            <input
                   type = "hidden"
                   name = "sum"
                   value = ""
                   data-type = "number"
                   id = "productPriceForm"
                >
            <input type="hidden" name="comment" value="">
            <input type="hidden" name="need-fio" value="false">
            <input type="hidden" name="need-email" value="false">
            <input type="hidden" name="need-phone" value="false">
            <input type="hidden" name="shopSuccessURL" value="" id="formShopSuccessURL">
            <input type="hidden" name="need-address" value="false">
            <input type="hidden" name="paymentType" value="AC" id="paymentType">

            <?php if ($productPrice < 15000) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-default paymentType" data-value="PC">
                                Яндекс.Деньгами
                            </button>
                            <button type="button" class="btn btn-default paymentType active" data-value="AC">
                                Банковской картой
                            </button>
                            <button type="button" class="btn btn-default paymentType" data-value="DI">
                                Другие способы
                            </button>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Оплата осуществляется в личном кабинете после оформления заказа.</p>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-12" style="margin-top: 30px;">
                    <div class="btn-group" role="group" aria-label="...">

                        <input type="button" value="Перейти к оплате"
                               class="btn btn-success btn-lg buttonSubmit" style="width: 400px; border: 2px solid white; border-radius: 20px;">
                    </div>
                </div>
            </div>



        </form>
    </div>


</div>
