<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use app\modules\Shop\services\Basket;

/**
 * Форма оплаты
 */

/* @var $this yii\web\View */
/* @var $request \app\models\Shop\Request */

$this->title = 'Оформление заказа #' . $request->getId();

$union = $request->getUnion();
$shop = $union->getShop();

$this->registerJs(<<<JS
    $('#formPay').submit();
JS
);

?>
<div class="container">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2 class="text-center">Идет перенаправление на оплату...</h2>

            <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" name="form2" id="formPay">
                <input type="hidden" name="receiver" value="<?= $shop->getField('yandex_id') ?>">
                <input type="hidden" name="formcomment" value="<?= $shop->getField('name') ?>">
                <input type="hidden" name="short-dest" value="Заказ #<?= $request->getId() ?>">
                <input type="hidden" name="label" value="gsss.<?= $union->getId() ?>.<?= $request->getId() ?>" id="formPayLabel">
                <input type="hidden" name="quickpay-form" value="donate">
                <input type="hidden" name="targets" value="Заказ #<?= $request->getId() ?>">
                <input type="hidden" name="sum" value="<?= $request->getField('price') ?>" data-type="number"
                       id="productPriceForm">
                <input type="hidden" name="comment" value="">
                <input type="hidden" name="need-fio" value="false">
                <input type="hidden" name="need-email" value="false">
                <input type="hidden" name="need-phone" value="false">
                <input type="hidden" name="need-address" value="false">
                <input type="hidden" name="paymentType" value="<?= Yii::$app->request->get('type', 'AC') ?>" id="paymentType">

                <div class="row">
                    <div class="col-lg-12" style="margin-top: 30px;">
                        <div class="btn-group" role="group" aria-label="...">
                            <input type="button" value="Перейти к оплате"
                                   class="btn btn-success btn-lg buttonSubmit" style="width: 400px; border: 2px solid white; border-radius: 20px;">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
