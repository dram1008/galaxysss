<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $items array */

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = $this->title;

$url = \yii\helpers\Url::to(['shop/basket_delete']);
$this->registerJs(<<<JS
    $('.buttonDelete').click(function() {
        var b = $(this);
        var id = b.data('id');
        ajaxJson({
            url: '{$url}',
            data: {
                id: id
            },
            success: function(ret) {
                b.parent().parent().remove();
            }
        });
    });

    function formatAsDecimal (val)
    {
        var pStr = '';
        if (val > 1000) {
            var t = parseInt(val/1000);
            var ost = (val - t * 1000);
            var ostStr = ost;
            if  (ost == 0) {
                ostStr = '000';
            } else {
                if (ost < 10) {
                    ostStr = '00' + ost;
                }
                if (ost < 100) {
                    ostStr = '0' + ost;
                }
            }
            pStr = t + ' ' + ostStr;
        } else {
            pStr = val;
        }

        return pStr;
    }
JS
);


?>
<div class="container">
    <div class="col-lg-12">
        <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>



        <?php
        $ids = [];
        foreach(\app\modules\Shop\services\Basket::get() as $item) {
            $ids[] = $item['id'];
        }
        $unionRows = \app\models\Shop\Product::query(['in', 'gs_unions_shop_product.id', $ids])
        ->innerJoin('gs_unions', 'gs_unions.id = gs_unions_shop_product.union_id')
        ->select([
            'gs_unions.*'
        ])
            ->groupBy([
                'gs_unions_shop_product.union_id'
            ])
        ->all();

        for ($i = 0; $i < count($unionRows); $i++) {
            $unionRows[$i]['productList'] = \app\models\Shop\Product::query(['union_id' => $unionRows[$i]['id']])
                ->andWhere(['in', 'gs_unions_shop_product.id', $ids])
                ->all();
            for($j=0;$j<count($unionRows[$i]['productList']);$j++) {
                foreach (\app\modules\Shop\services\Basket::get() as $d) {
                    if ($unionRows[$i]['productList'][$j]['id'] == $d['id']) {
                        $unionRows[$i]['productList'][$j]['count'] = $d['count'];
                        continue;
                    }
                }
            }
        }
        ?>

        <?php
        $this->registerJs(<<<JS
    $('.buttonUp').click(function() {
        var id = $(this).data('id');
        var input = $(this).parent().find('input');
        var c = input.val();
        c++;

        var price = $('.price-' + id).data('value');
        var sum = price * c;
        var sumStr = formatAsDecimal(sum);

        ajaxJson({
            url: '/shop/basket/update',
            data: {
                product: id,
                count: c
            },
            success: function(ret) {
                $('.sum-' + id).html(sumStr).data('value', sum);
                input.val(c);
            }
        });

    });
    $('.buttonDown').click(function() {
        var id = $(this).data('id');
        var input = $(this).parent().find('input');
        var c = input.val();
        c--;
        if (c < 0) {
            return;
        }

        var price = $('.price-' + id).data('value');
        var sum = price * c;
        var sumStr = formatAsDecimal(sum);

        ajaxJson({
            url: '/shop/basket/update',
            data: {
                product: id,
                count: c
            },
            success: function(ret) {
                $('.sum-' + id).html(sumStr).data('value', sum);
                input.val(c);
            }
        });
    });
JS
);
        ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>
                    #
                </th>
                <th>
                    Наимениование
                </th>
                <th>
                    Кол-во
                </th>
                <th>
                    Стоимость единицы
                </th>
                <th>
                    Стоимость
                </th>
                <th>
                    Удалить
                </th>
            </tr>
            <?php $c = 1; ?>
            <?php foreach($unionRows as $union) { ?>
            <tr>
                <td>

                </td>
                <td colspan="5">
                    <b><?= $union['name'] ?></b>
                    <?php if (count($unionRows) > 1) { ?>
                        <a href="<?= \yii\helpers\Url::to(['shop/order', 'unionId' => $union['id']]) ?>" class="btn btn-primary btn-xs">Оформить заказ</a>
                    <?php } ?>
                </td>
            </tr>
                <?php
                foreach($union['productList'] as $product) {
                    $p = new \app\models\Shop\Product($product);
                    ?>
                    <tr>
                        <td>
                            <?= $c++; ?>
                        </td>
                        <td>
                            <a href="<?= $p->getLink() ?>"><?= $product['name'] ?></a>
                        </td>
                        <td>
                            <input type="text" class="form-control" value="<?= $product['count'] ?>" style="width: 50px; display: inline-block;">
                            <button type="button" class="btn btn-default btn-xs buttonUp" data-id="<?= $product['id'] ?>">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-xs buttonDown" data-id="<?= $product['id'] ?>">
                                <span class="glyphicon glyphicon-minus"></span>
                            </button>
                        </td>
                        <td class="price-<?= $product['id'] ?>" data-value="<?= $product['price'] ?>">
                            <?= Yii::$app->formatter->asDecimal($product['price'],0) ?>
                        </td>
                        <td class="sum-<?= $product['id'] ?>" data-value="<?= $product['count'] * $product['price'] ?>">
                            <?= Yii::$app->formatter->asDecimal($product['count'] * $product['price'], 0) ?>
                        </td>
                        <td>
                            <button class="btn btn-danger btn-xs buttonDelete" data-id="<?= $product['id'] ?>">Удалить</button>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
        <?php if (count($unionRows) == 1) { ?>
            <a href="<?= \yii\helpers\Url::to(['shop/order']) ?>" class="btn btn-primary">Оформить заказ</a>
        <?php } ?>
    </div>
</div>
