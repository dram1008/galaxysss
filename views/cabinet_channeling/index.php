<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Мои послания';

$this->registerJS(<<<JS

    $('.buttonDelete').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите удаление')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/channeling/' + id + '/delete',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().parent().remove();
                    });
                }
            });
        }
    });

    $('.buttonSendModeration').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтвердите отправку')) {
            var button = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/channeling/' + id + '/sendModeration',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        button.parent().html($('<span>', {
                            class: 'label label-info'
                        }).html('Отправлено'));
                    });
                }
            });
        }
    });
    // Сделать рассылку
    $('.buttonAddSiteUpdate').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm('Подтведите свое намерение')) {
            var buttonSubscribe = $(this);
            var id = $(this).data('id');
            ajaxJson({
                url: '/cabinet/channeling/' + id + '/subscribe',
                success: function (ret) {
                    infoWindow('Успешно', function() {
                        buttonSubscribe.remove();
                    });
                }
            });
        }
    });
    // Просмотр
    $('.buttonView').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        window.location = '/cabinet/channeling/' + $(this).data('id') + '/view';
    });

    $('.rowTable').click(function() {
        window.location = '/cabinet/channeling/' + $(this).data('id') + '/edit';
    });

JS
);

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <div class="btn-group" style="margin-bottom: 20px;">
            <a href="<?= Url::to(['cabinet_channeling/add']) ?>" class="btn btn-default">Добавить</a>
        </div>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Chenneling::query()
                    ->andWhere([
                        'user_id' => Yii::$app->user->id
                    ])
                    ->orderBy(['date_created' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data' => ['id' => $item['id']],
                    'role' => 'button',
                    'class' => 'rowTable'
                ];
            },
            'columns'      => ArrayHelper::merge([
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'img', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class' => "thumbnail",
                            'width' => 80,
                        ]);
                    }
                ],
                'header:text:Название',
                [
                    'header'  => 'Дата',
                    'content' => function ($item) {
                        return Html::tag('span', GsssHtml::dateString($item['date_created']), ['style' => 'font-size: 80%; margin-bottom:10px; color: #c0c0c0;']);
                    }
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Посмотреть',
                    'content' => function ($item) {
                        return Html::button('Посмотреть', [
                            'class' => 'btn btn-default btn-xs buttonView',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header' => 'Модерация',
                    'content' => function ($item) {
                        $moderation_status = \yii\helpers\ArrayHelper::getValue($item, 'moderation_status');
                        if (is_null($moderation_status)) {
                            $arr[] = Html::tag('span', 'На редактировании', ['class' => 'label label-default']);
                            $arr[] = Html::tag('br');
                            $arr[] = Html::tag('br');
                            $arr[] = Html::button('Отправить на модерацию', [
                                'class'   => "btn btn-default btn-xs buttonSendModeration",
                                'data-id' => $item['id'],
                            ]);
                            return join('', $arr);
                        } else if ($moderation_status == 2) {
                            return Html::tag('span', 'На рассмотрении', ['class' => 'label label-warning']);
                        } else if ($moderation_status == 0) {
                            $arr[] = Html::tag('span', 'Отклонено модератором', ['class' => 'label label-danger']);
                            $arr[] = Html::tag('br');
                            $arr[] = Html::tag('br');
                            $arr[] = Html::button('Отправить на модерацию', [
                                'class'   => "btn btn-default btn-xs buttonSendModeration",
                                'data-id' => $item['id'],
                            ]);
                            return join('', $arr);
                        } else {
                            return Html::tag('span', 'Одобрено', ['class' => 'label label-success']);
                        }
                    }
                ],

            ], (Yii::$app->user->identity->get('mode_chenneling_is_subscribe',0) == 1)? [
                [
                    'header'  => 'Рассылка',
                    'content' => function ($item) {
                        if (ArrayHelper::getValue($item, 'is_added_site_update', 0) == 1) {
                            return '';
                        }
                        if (ArrayHelper::getValue($item, 'moderation_status', 0) != 1) {
                            return '';
                        }

                        return Html::button('Рассылка', [
                            'class' => 'btn btn-success btn-xs buttonAddSiteUpdate',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ]
            ] : []),
        ]) ?>

    </div>
</div>