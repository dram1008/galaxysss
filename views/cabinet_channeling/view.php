<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;

/* @var $this yii\web\View */
/* @var $item \app\models\Chenneling поля послания */

$this->title = $item->get('header');

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8" style="padding-bottom: 50px;">
        <?= $item->get('content') ?>

<!--        Показ пользователя -->
        <?php $author = $item->getUser(); ?>
        <?php if (is_null($author)) { ?>
            <hr>
            <p>Автор: <img src="<?= $author->getAvatar() ?>" width="50" class="img-circle"> <a href="<?= $author->getLink() ?>"><?= $author->getName2() ?></a>
        <?php } ?>


    </div>
</div>