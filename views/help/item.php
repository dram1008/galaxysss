<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $item \app\models\Help */

$this->title = $item->get('name');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <?= $item->get('content') ?>
    </div>
</div>

<?php $this->render('../blocks/shareMeta', [
    'image' => $item->getImage(true),
    'url' => \yii\helpers\Url::current([], true),
    'title' => $this->title,
    'description' => \app\services\GsssHtml::getMiniText($item->get('content')),
]) ?>
