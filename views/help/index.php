<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $rows array */

$this->title = 'Помощь';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">Помощь</h1>


    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <?= $this->render('_tree', [
            'rows' => $rows
        ])?>
    </div>

</div>
