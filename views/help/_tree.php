<?php

/**
 * params:
 * [
 *      'rows'     => [
 *                       [
 *                            'id'                 => '524'
 *                            'name'               => '123456'
 *                            ...
 *                            'selected'           => bool
 *                        ], ...
 *                    ]
 *     'formName'          => $this->model->formName(), // string
 *     'model'             => $this->model,             // \yii\base\Model
 *     'attrId'            => $this->attrId,            // attribute id
 *     'attrName'          => $this->attrName,          // attribute name
 *     'templateVariables' => []
 * ]
 */

/** @var $rows array */

?>



<ul>
    <?php
    foreach ($rows as $item) {
        ?>
        <li>
            <?= $item['name'] ?>
        </li>
        <?php if (\yii\helpers\ArrayHelper::getValue($item, 'articleList')) { ?>
            <ul>
                <?php foreach (\yii\helpers\ArrayHelper::getValue($item, 'articleList') as $i) { ?>
                    <li>
                        <a href="/help/<?= $i['id'] ?>"><?= $i['name'] ?></a>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>

        <?php
        if (isset($item['nodes'])) {
            echo $this->render('_tree', [
                'rows' => $item['nodes'],
            ]);
        }
        ?>

    <?php } ?>
</ul>
