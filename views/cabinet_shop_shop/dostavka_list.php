<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $union_id int */

$union = \app\models\Union::find($union_id);

$this->title = 'Доставка';
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            [
                'label' => 'Магазин',
                'url'   => Url::to(['cabinet_shop_shop/index', 'id' => $union->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>

    <?= \yii\grid\GridView::widget([
        'tableOptions' => [
            'class' => 'table tableMy table-striped table-hover',
        ],
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \app\models\Shop\DostavkaItem::query(['union_id' => $union_id])
                ->select([
                    'id',
                    'name',
                    'description',
                    'price',
                    'is_need_address',
                ])
            ,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            'id',
            'name:text:Название',
            'description:text:Описание',
            'price:currency:Цена',
            [
                'header' => 'Нужен адрес?',
                'content' => function($item) {
                    if (\yii\helpers\ArrayHelper::getValue($item, 'is_need_address', 0) == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);

                    return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                }
            ],
            [
                'header' => 'Редактировать',
                'content' => function($item) {
                    return Html::a('Редактировать', ['cabinet_shop_shop/dostavka_list_edit', 'id' => $item['id']], ['class' => 'btn btn-primary']);
                }
            ],
            [
                'header' => 'Удалить',
                'content' => function($item) {
                    return Html::button('Удалить', ['class' => 'btn btn-danger btn-xs buttonDelete', 'data-id' => $item['id']]);
                }
            ],
        ]
    ]) ?>
    <hr>
    <a href="<?= \yii\helpers\Url::to(['cabinet_shop_shop/dostavka_list_add', 'id' => $union_id]) ?>" class="btn btn-default">Добавить</a>
</div>
