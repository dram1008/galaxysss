<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */
/* @var $union \app\models\Union */

$this->title = 'Магазин';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
        'items' => [
            [
                'label' => 'Объединения',
                'url'   => Url::to(['cabinet/objects']),
            ],
            [
                'label' => $union->getName(),
                'url'   => Url::to(['cabinet/objects_edit', 'id' => $union->getId()]),
            ],
            $this->title
        ],
        'home'  => [
            'name' => 'я',
            'url'  => Url::to(['site/user', 'id' => Yii::$app->user->id])
        ]
    ]) ?>
    <hr>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Успешно обновлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin([
                    'id'      => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'name') ?>
                <?= $model->field($form, 'admin_email') ?>
                <?= $model->field($form, 'rekvizit')->textarea(['rows' => 10]) ?>
                <?= $model->field($form, 'mail_image') ?>
                <?= $model->field($form, 'signature')->textarea(['rows' => 3]) ?>
                <?= $model->field($form, 'contact')->textarea(['rows' => 3]) ?>

                <?= $model->field($form, 'yandex_id') ?>
                <?= $model->field($form, 'secret') ?>

                <?php if ($model->union_id) { ?>
                    <hr>
                    <a href="<?= \yii\helpers\Url::to(['cabinet_shop_shop/product_list', 'id' => $model->union_id]) ?>" class="btn btn-default">Товары</a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet_shop_shop/request_list', 'id' => $model->union_id]) ?>" class="btn btn-default">Заказы</a>
                    <a href="<?= \yii\helpers\Url::to(['cabinet_shop_shop/dostavka_list', 'id' => $model->union_id]) ?>" class="btn btn-default">Доставка</a>
                <?php } ?>
                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Обновить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
