<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Мои заказы';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(<<<JS
    $('.rowTable').click(function() {
        window.location = '/cabinet/order/' + $(this).data('id');
    });
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => [
                $this->title
            ],
            'home'  => [
                'name' => 'я',
                'url'  => \yii\helpers\Url::to(['site/user', 'id' => Yii::$app->user->id])
            ]
        ]) ?>
        <hr>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \app\models\Shop\Request::query([
                    'user_id' => Yii::$app->user->id,
                ])
                    ->andWhere(['not in', 'status', [
                        \app\models\Shop\Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT,
                        \app\models\Shop\Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP,
                        \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT,
                        \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP,
                    ]])
                    ->orderBy([
//                    'is_answer_from_shop' => SORT_DESC,
'last_message_time' => SORT_DESC,
'date_create'       => SORT_DESC,
                    ])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Объединение',
                    'content' => function ($item) {
                        $c = \app\models\Union::find($item['union_id']);

                        return (is_null($c)) ? '' : $c->getField('name');
                    },
                ],
                [
                    'header'  => 'Цена',
                    'content' => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['price'], 0);
                    },
                ],
                [
                    'header'  => 'Статус',
                    'content' => function ($item) {
                        $s = \yii\helpers\ArrayHelper::getValue(\app\models\Shop\Request::$statusList, $item['status'], null);
                        if (is_null($s)) {
                            $title = '';
                        } else {
                            $title = \yii\helpers\ArrayHelper::getValue($s, 'client', '');
                        }
                        switch ($item['status']) {
                            case \app\models\Shop\Request::STATUS_DONE:
                                $progress = 100;
                                break;
                            case \app\models\Shop\Request::STATUS_SEND_TO_SHOP:
                                $progress = 10;
                                break;
                            case \app\models\Shop\Request::STATUS_ORDER_DOSTAVKA:
                                $progress = 20;
                                break;
                            case \app\models\Shop\Request::STATUS_PAID_CLIENT:
                                $progress = 30;
                                break;
                            case \app\models\Shop\Request::STATUS_PAID_SHOP:
                                $progress = 40;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_PREPARE:
                                $progress = 50;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_PREPARE_DONE:
                                $progress = 60;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_SEND_TO_USER:
                                $progress = 70;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_FINISH_CLIENT:
                                $progress = 90;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_ADDRESS_FINISH_SHOP:
                                $progress = 100;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_SAMOVIVOZ_WAIT:
                                $progress = 50;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_CLIENT:
                                $progress = 90;
                                break;
                            case \app\models\Shop\Request::STATUS_DOSTAVKA_SAMOVIVOZ_FINISH_SHOP:
                                $progress = 100;
                                break;
                            default:
                                $progress = 0;
                                break;
                        }

                        return Html::tag('div',
                            Html::tag('div',
                                Html::tag('span', '40% Complete (success)', ['class' => 'sr-only'])
                                , ['class' => 'progress-bar progress-bar-success', 'role' => "progressbar", 'aria-valuenow' => $progress, 'aria-valuemin' => "0", 'aria-valuemax' => "100", 'style' => "width: " . $progress . "%"])
                            , [
                                'class' => 'progress gsssTooltip',
                                'title' => $title,
                            ]);
                    },
                ],
                [
                    'header'  => 'Время создания',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'date_create', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                'comment:text:Комментарий',
                [
                    'header'  => 'Последний ответ',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['class' => 'gsssTooltip', 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Есть ответ?',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_answer_from_shop', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', Html::tag('span', null, ['class' => 'glyphicon glyphicon-envelope']), ['class' => 'label label-success']);
                    }
                ],
            ]
        ]) ?>
    </div>
</div>
