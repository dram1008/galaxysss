<?php

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;

$this->title = 'Входящие платежи';

?>

<div class="container">
    <h1 class="page-header"><?= $this->title ?></h1>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \app\models\Shop\Payments::find()->orderBy(['date_insert' => SORT_DESC]),
            'sort' => new \yii\data\Sort([
                'attributes' => [
                    'withdraw_amount',
                    'amount',
                    'date_insert',
                    'unaccepted',
                    'datetime',
                ]
            ]),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            return [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable'

            ];
        },
    ]) ?>
</div>