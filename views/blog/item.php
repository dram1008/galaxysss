<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Str;

/* @var $this yii\web\View */
/* @var $item \app\models\Blog */

$this->title = $item->get('header');
?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>
    </div>

    <div class="col-lg-8" style="padding-bottom: 50px;">
        <?= $item->get('content') ?>
        <hr>
        <?php if ($item->get('source', '') != ''): ?>
            <?= Html::a('Ссылка на источник »', $item->get('source'), [
                'class'  => 'btn btn-primary',
                'target' => '_blank',
                'style'  => 'margin-bottom:10px;'
            ]) ?>
        <?php endif; ?>
        <?= $this->render('../blocks/share', [
            'image'       => \cs\Widget\FileUpload2\FileUpload::getOriginal(Url::to($item->get('image'), true), false),
            'url'         => Url::current([], true),
            'title'       => $item->get('header'),
            'description' => $item->get('description'),
        ]) ?>
        <?= $this->render('../blocks/yandexMoney2') ?>
        <!--                Комментарии -->
        <?//= \app\modules\Comment\Service::render(\app\modules\Comment\Model::TYPE_NEWS, $item['id']); ?>
        <?= $this->render('../blocks/fbLike') ?>

    </div>
    <div class="col-lg-4">
        <?php if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isMarketing', 0) == 1) { ?>
            <?= $this->render('../blocks/reklama') ?>
        <?php } ?>

        <?php
        foreach ($nearList as $item) {
            $b = new \app\models\Blog($item);
            ?>
            <div class="thumbnail">
                <a href="<?= $b->getLink() ?>"><img
                        src="<?= $item['image'] ?>"
                        style="width: 100%; display: block;"
                        > </a>

                <div class="caption">
                    <h3>
                        <?= $item['header'] ?>
                    </h3>

                    <p>
                        <?= $b->get('description'); ?>
                    </p>

                    <!--                                <p>-->
                    <!--                                    <a href="#"-->
                    <!--                                       class="btn btn-default"-->
                    <!--                                       role="button"-->
                    <!--                                        style="width: 100%"-->
                    <!--                                        >Button</a>-->
                    <!--                                </p>-->
                </div>
            </div>

        <?php
        }
        ?>

        <?= $this->render('../blocks/reklama/block2') ?>
    </div>

</div>