<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $html string код html */

$this->title = 'Блог';

$this->registerJs(<<<JS
    var page = 1;
    var isStartLoad = false;
    var isFinished = false;
    $(window).on('scroll', function() {

        var sizeBottom = $('#layoutMenuFooter').height() + $('#layoutMenuFooter').find('.container').height() + 420;
        if ($(window).scrollTop() > $(document).height() - sizeBottom) {

            if (isFinished == false) {
                if (isStartLoad === false) {
                    isStartLoad = true;
                    page++;
                    $('#chennelingPages').remove();
                    var d = {
                            page: page
                        };
                    ajaxJson({
                        url: '/blog/ajax',
                        data: d,
                        beforeSend: function() {
                            $('#channelingList').append(
                                $('<div>', {
                                    id: 'channelingListLoading',
                                    class: 'col-lg-12'
                                }).append(
                                    $('<img>', {src: pathLayoutMenu + '/images/ajax-loader.gif'})
                                )
                            );
                        },
                        success: function(ret) {
                            if (ret == '') isFinished = true;
                            $('#channelingListLoading').remove();
                            $('#channelingList').append(ret);
                            isStartLoad = false;
                        }
                    });
                }
            }
        }
    });
JS
);
?>

<?= $html ?>