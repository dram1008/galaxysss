<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use cs\services\Url as csUrl;
use cs\services\Str;
use yii\widgets\Breadcrumbs;

/** @var $this yii\web\View */
/** @var $union       \app\models\Union */
/** @var $shop        \app\models\Shop */
/** @var $breadcrumbs array */
/** @var $category    \app\models\UnionCategory */
/** @var $items       array gs_unions_shop_product.* */

$this->title = 'Магазин';

$isShowWindow = 1;
if (Yii::$app->user->isGuest) {
    if (Yii::$app->session->get('shop.help_window_already_showed', 0) == 1) {
        $isShowWindow = 0;
    }
} else {
    if (Yii::$app->user->identity->get('is_show_shop_help_window', 0) == 0) {
        if (Yii::$app->session->get('shop.help_window_already_showed', 0) == 1) {
            $isShowWindow = 0;
        }
    } else {
        $isShowWindow = 0;
    }
}

$url = Url::to(['shop/basket']);
$urlAdd = Url::to(['shop/basket_add']);
$this->registerJs(<<<JS
    var isShowedWindow = false;
    $('.buttonAdd').click(function() {
        var id = $(this).data('id');
        ajaxJson({
            url: '{$urlAdd}',
            data: {
                id: id
            },
            success: function(ret) {
                var basket = $('#basketCount');
                var span;
                if (basket.length > 0) {
                    var count = basket.html();
                    basket.html(ret);
                    span = $('#basketCount').popover({
                        content: 'Товар добавлен',
                        placement: 'bottom'
                    });
                    span.popover('show');
                    window.setTimeout(function() {
                        span.popover('hide');
                        if ({$isShowWindow} == 1) {
                            GSSS.modal('#modalHelpShop2', {
                                callbacks: {
                                    close: function() {
                                        if (!$('#isShowModalHelp').is(':checked')) {
                                            ajaxJson({
                                                url: '/shop/setNoHelpWindow'
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }, 2000);
                } else {
                    var userMenu = $('#userBlockLi');
                    span = $('<span>', {
                                id: 'basketCount',
                                class: 'label label-success',
                                title: 'Корзина'
                            }).tooltip({placement:"left"}).html(ret).popover({
                        content: 'Товар добавлен',
                        placement: 'bottom'
                    });
                    var liBasket = $('<li>').append(
                        $('<a>', {
                            href: '{$url}',
                            style: 'padding-right: 0px;  padding-bottom: 0px;'
                        }). append(
                            span
                        )
                    );
                    liBasket.insertBefore(userMenu);
                    span.popover('show');
                    window.setTimeout(function() {
                        span.popover('hide');
                        if ({$isShowWindow} == 1) {
                            GSSS.modal('#modalHelpShop2', {
                                callbacks: {
                                    close: function() {
                                        if (!$('#isShowModalHelp').is(':checked')) {
                                            ajaxJson({
                                                url: '/shop/setNoHelpWindow'
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }, 2000);
                }
            }
        });
    });
JS
);

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <?= \cs\Widget\BreadCrumbs\BreadCrumbs::widget([
            'items' => $breadcrumbs
        ]) ?>
        <hr>
    </div>

    <div class="col-lg-4">
        <img class="img-thumbnail" src="<?= $union->getImage() ?>">
    </div>
    <div class="col-lg-8">

        <p>Товары</p>

        <?php foreach($items as $item) { ?>
            <div class="row">
                <div class="col-lg-4">
                    <a href="<?= Url::to(['union_shop/product', 'id' => $item['id'], 'union_id' => $union->getId(), 'category' => $category->getField('id_string')]) ?>">
                        <img src="<?= $item['image'] ?>"  width="100%" class="thumbnail">
                    </a>
                </div>
                <div class="col-lg-8">
                    <p><?= $item['description'] ?></p>
                    <hr>
                    <p><?= Yii::$app->formatter->asDecimal($item['price']) ?></p>
                    <button class="btn btn-default btn-lg buttonAdd" style="100%" data-id="<?= $item['id'] ?>">Добавить</button>
                </div>
            </div>
        <?php } ?>

        <hr>
        <?= $this->render('../blocks/share', [
            'image'       => \cs\Widget\FileUpload2\FileUpload::getOriginal(Url::to($union->getImage(), true), false),
            'url'         => Url::current([], true),
            'title'       => $union->getName() . '. Магазин',
            'description' => 'ff',
        ]) ?>
    </div>


</div>

<?php if ($isShowWindow) { ?>
    <?php
    \app\assets\ModalBoxNew\Asset::register($this);

    ?>
    <div id="modalHelpShop2" class="zoom-anim-dialog mfp-hide mfp-dialog">
        <h2>Как совершить покупку</h2>

        <div style="margin-top: 50px;margin-bottom: 50px;">
            <p>Сейчас выбранный вами товар попал в корзину.</p>
            <img src="/images/union_shop/product/modal_basket.png" class="thumbnail" style="width: 100%;">
            <p>Вы можете продолжить выбирать товары или перейти к <a href="/shop/order">оформлениею заказа</a>.</p>
            <p>Просмотреть <a href="/shop/basket">содержимое корзины</a> вы можете кликнув по зеленому значку.</p>
        </div>
        <?php
        \cs\assets\CheckBox\Asset::register($this);
        ?>
        <table>
            <tr>
                <td style="vertical-align:  center; padding-right: 10px;">
                    Показывать это окно в дальнейшем?
                </td>
                <td>
                    <input type="checkbox" data-toggle="toggle" data-on="Да" data-off="Нет" id="isShowModalHelp"
                           checked="checked">
                </td>
            </tr>
        </table>
    </div>
<?php } ?>
